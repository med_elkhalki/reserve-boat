import { persist } from "mobx-persist";
import { action, observable } from "mobx";
import { asyncAction } from "mobx-utils/lib/mobx-utils";
import Screens from "./../navigation/screensName";
import Api from "./../libs/api";
import * as Entities from "./../modules/reservations";
import ErrorsHandler from "./../libs/errorHandler";

export default class ReservationsStore {
  @observable
  reservations = observable(Entities.ReservationsEntity());
  @observable
  mySell = observable(Entities.ReservationsEntity());
  @observable
  reservationsDetail = observable(Entities.ReservationEntity());
  @observable
  totalPrice = 0;

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  getReservationsAction = asyncAction(
    "getReservationsAction",
    function*() {
      try {
        let result = yield Api.get("myreservationss", {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.reservations = Entities.ReservationsEntity(result.data);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getReservationDetailAction = asyncAction(
    "getReservationDetailAction",
    function*(id) {
      try {
        let result = yield Api.get(`detailsdivereserve/${id}`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.reservationsDetail = Entities.ReservationEntity(result[0]);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getMyReservationsAction = asyncAction(
    "getMyReservationsAction",
    function*() {
      try {
        let result = yield Api.get("reserve/ship", {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.reservations = Entities.ReservationsEntity(result.data);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  reserveShipAction = asyncAction(
    "reserveShipAction",
    function*({ ship_id, day, start, crew_count, total_price }) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post(
          `reserve/ship`,
          { ship_id, day, start, crew_count, total_price },
          true
        );
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.rootStore.setLoader(false);
        return;
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  canceldivereserveAction = asyncAction(
    "canceldivereserveAction",
    function*(id) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.get(`canceldivereserve/${id}`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.rootStore.setLoader(false);
        return result;
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  confirmdivereserveAction = asyncAction(
    "confirmdivereserveAction",
    function*(id) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post(
          `confirmdive/reserve`,
          { id, status: "confirmed" },
          true
        );
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.rootStore.setLoader(false);
        return result;
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  gettotalpriceAction = asyncAction(
    "gettotalpriceAction",
    function*(id, crew_count) {
      try {
        let result = yield Api.get(`totalprice/${id}/${crew_count}`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.totalPrice = result;
        return result;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getMySellAction = asyncAction(
    "getMySellAction",
    function*() {
      try {
        let result = yield Api.get(`mysell`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.mySell = Entities.MySellEntity(result.data);
        return result;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getSellShipAction = asyncAction(
    "getSellShipAction",
    function*() {
      try {
        let result = yield Api.get(`sell/ship`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.mySell = Entities.MySellEntity(result.data);
        return result;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );
}
