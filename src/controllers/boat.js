import { persist } from "mobx-persist";
import { action, observable } from "mobx";
import {asyncAction} from "mobx-utils/lib/mobx-utils";
import Screens from "./../navigation/screensName";
import Api from "./../libs/api";
import * as Entities from "./../modules/boat";
import ErrorsHandler from "./../libs/errorHandler";
import config from "../../config";

export default class BoatStore {
  @observable
  boatsList = observable(Entities.BoatListEntity());

  @observable
  boatDetail = observable(Entities.BoatDetailEntity());

  @observable
  myShips = observable(Entities.BoatListEntity());

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  getBoatsByNamection = asyncAction('getBoatsByNamection',
    function*(name) {
      try {
        let result = yield Api.post("show/all", { name }, true);
        this.boatsList = Entities.BoatListEntity(result.data);
        return this.boatsList;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getBoatsDetailsAction = asyncAction('getBoatsDetailsAction',
    function*(id) {
      try {
        let result = yield Api.get(`ship/show/${id}`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.boatDetail = Entities.BoatDetailEntity(result);
        return this.boatDetail;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  getMyShipsAction = asyncAction('getMyShipsAction',
    function*() {
      try {
        let result = yield Api.get(`myships`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.myShips = Entities.BoatListEntity(result.data);
        return this.myShips;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  sendRating = asyncAction('sendRating',
    function*({ ship_id, rate, rate2, rate3 }) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post(
          "rate",
          { ship_id, rate, rate2, rate3 },
          true
        );
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.rootStore.setLoader(false);
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  searchInMyBoats = asyncAction('searchInMyBoats',
    function*(keyword) {
      try {
        let result = yield Api.get(`search/${keyword}`, {}, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.myShips = Entities.BoatListEntity(result.data);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  searchShip = asyncAction('searchShip',
    function*({ type, body }) {
      try {
        let result = yield Api.post(`searchship`, { type, body }, true);
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.boatsList = Entities.BoatListEntity(result.data);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );

  addShip = asyncAction('addShip',
    function*({
      type,
      crew_count,
      maintenance_start,
      maintenance_end,
      start,
      end,
      price,
      details,
      from_city,
      to_city,
      user_id,
      images,
      name,
      marque,
      carburant,
      year,
      width,
      Motorisation,
      height
    }) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post(
          `ship`,
          type && type === config.sell ?
          {
            type,
            crew_count,
            price,
            details,
            user_id,
            images,
            name,
            marque,
            carburant,
            year,
            width,
            Motorisation,
            height,
            start: new Date(),
            end: new Date(),
          }
          : {
            type,
            crew_count,
            maintenance_start,
            maintenance_end,
            start,
            end,
            price,
            details,
            from_city,
            to_city,
            user_id,
            images,
            name
          },
          true
        );
        if (Api.testStatus(result).error) throw Api.testStatus(result);
        this.rootStore.setLoader(false);
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors);
      }
    }.bind(this)
  );
}
