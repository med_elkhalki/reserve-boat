import {persist} from "mobx-persist";
import {action, observable} from "mobx";
import {asyncAction} from "mobx-utils/lib/mobx-utils";
import Screens from "./../navigation/screensName";
import Api from "./../libs/api";
import * as Entities from "./../modules/auth";
import ErrorsHandler from './../libs/errorHandler'

export default class SettingsStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    contactUsAction = asyncAction('contactUsAction',
        function* ({name, phone, email, msg}) {
            try {
                this.rootStore.setLoader(true);
                let result = yield Api.post("contact", {name, phone, email, msg}, true);
                if(Api.testStatus(result).error) throw Api.testStatus(result);
                this.rootStore.setLoader(false);
            } catch (errors) {
                this.rootStore.setLoader(false);
                throw yield ErrorsHandler.handleGenericErrors(errors)
            }
        }.bind(this)
    );

    editUserProfile = asyncAction('editUserProfile',
        function* ({name, phone, email, password, password_confirmation, group_id}) {
            try {
                this.rootStore.setLoader(true);
                let result = yield Api.post("setting", {name, phone, email, password,password_confirmation,group_id}, true);
                if(Api.testStatus(result).error) throw Api.testStatus(result)
                this.rootStore.setLoader(false);
                return result;
            } catch (errors) {
                this.rootStore.setLoader(false);
                throw yield ErrorsHandler.handleGenericErrors(errors)
            }
        }.bind(this)
    );


}
