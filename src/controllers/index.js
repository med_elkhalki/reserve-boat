// @flow
import { enableLogging } from 'mobx-logger';
import { action, observable, configure } from 'mobx';
import {asyncAction} from "mobx-utils/lib/mobx-utils";
import AuthStore from './authentication';
import BoatStore from './boat';
import NotificationsStore from './notifications';
import ReservationsStore from './reservations';
import SettingsStore from './settings';
import { AsyncStorage } from 'react-native';
import { create, persist } from 'mobx-persist';
import Screens from './../navigation/screensName';
import { Navigation } from 'react-native-navigation';
import vars from './../libs/vars';
import I18n from './../languages/';

// configure({
//   enforceActions: 'always'
// });

const hydrate = create({ storage: AsyncStorage });

export class RootStore {
  @observable
  appMoappModede = 'splash'; // values : splash | login | user | admin
  @persist
  @observable
  rootScreen = Screens.auth.signin;

  @persist
  @observable
  position = I18n.locale.indexOf('ar') > -1 ? 'right' : 'left';

  @persist
  @observable
  lang = I18n.locale;

  @observable
  isLoading = false;

  @observable
  currentScreen = Screens.boat.home;

  authStore: AuthStore;
  boatStore: BoatStore;
  notificationsStore: NotificationsStore;
  reservationsStore: ReservationsStore;
  settingsStore: SettingsStore;

  constructor() {
    this.authStore = new AuthStore(this);
    this.boatStore = new BoatStore(this);
    this.notificationsStore = new NotificationsStore(this);
    this.reservationsStore = new ReservationsStore(this);
    this.settingsStore = new SettingsStore(this);
    hydrate('root', this);
  }

  getStores() {
    return {
      rootStore: this,
      authStore: this.authStore,
      boatStore: this.boatStore,
      notificationsStore: this.notificationsStore,
      reservationsStore: this.reservationsStore,
      settingsStore: this.settingsStore,
    };
  }

  splashScreenActions = asyncAction('splashScreenActions',
    function*() {
      try {
        return true;
      } catch (err) {
        throw err;
      }
    }.bind(this)
  );

  @action
  setAppModeAndRootScreen(appMode, rootScreen) {
    this.appMode = appMode;
    this.rootScreen = rootScreen;
  }

  @action
  setCurrentScreen(screensName) {
    this.currentScreen = screensName;
  }

  @action
  changeLang(lang) {
    switch (lang) {
      case 'ar':
        this.position = 'right';
        this.lang = 'ar';
        I18n.locale = 'ar';
        break;
      case 'en':
        this.position = 'left';
        this.lang = 'en';
        I18n.locale = 'en';
        break;
      case 'fr':
        this.position = 'left';
        this.lang = 'fr';
        I18n.locale = 'fr';
        break;
      default:
        this.position = 'right';
        this.lang = 'ar';
        I18n.locale = 'ar';
        break;
    }
  }

  @action
  setLoader(isLoading) {
    this.isLoading = isLoading;
  }

  @action
  async navigate(componentId = null, options = {}, method = 'push') {
    const componentIdUnusedIn = ['showModal', 'showOverlay', 'setRoot'];
    const setCurrentScreenOn = ['push', 'setStackRoot']
    if(method === 'setStackRoot') {
      this.currentScreen = options.component.name;
    }
    if (componentIdUnusedIn.indexOf(method) > -1) {
      await Navigation[method](options);
      return;
    }
    await Navigation[method](componentId, options);
  }
}

const config = {
  predicate: () => __DEV__ && Boolean(window.navigator.userAgent),
  action: true,
  reaction: true,
  transaction: true,
  compute: true
};

enableLogging(config);

export default new RootStore();
