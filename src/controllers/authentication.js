import { create, persist } from "mobx-persist";
import { action, observable } from "mobx";
import Screens from "./../navigation/screensName";
import {asyncAction} from "mobx-utils/lib/mobx-utils";
import Api from "./../libs/api";
import * as Entities from "./../modules/auth";
import { userStack } from "./../navigation/stacks";
import { AsyncStorage } from "react-native";
import ErrorsHandler from './../libs/errorHandler'

const hydrate = create({ storage: AsyncStorage });

export default class AuthStore {
  @persist
  @observable
  token = null;

  @persist("object")
  @observable
  user = observable(Entities.UserEntity());

  @observable
  policy = observable(Entities.PolicyEntity());

  constructor(rootStore) {
    hydrate("auth", this);
    this.rootStore = rootStore;
  }

  loginAction = asyncAction('loginAction',
    function*({ phone, password }) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post("user/login", { phone, password }, false);
        if(Api.testStatus(result).error) throw Api.testStatus(result)
        this.user = Entities.UserEntity(result.user);
        this.token = result.token ? result.token : result.user.token;
        Api.token = result.token ? result.token : result.user.token;
        this.rootStore.setLoader(false);
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors)
      }
    }.bind(this)
  );

  signupAction = asyncAction('signupAction',
    function*({ name, phone, email, password, group_id, password_confirmation }) {
      try {
        this.rootStore.setLoader(true);
        let result = yield Api.post("user", { name, phone, email, password, group_id, password_confirmation }, false);
        if(Api.testStatus(result).error) throw Api.testStatus(result)
        this.user = Entities.UserEntity(result.result.data);
        this.token = result.result && result.result.data && result.result.data.token ? result.result.data.token : null;
        Api.token = result.result && result.result.data && result.result.data.token ? result.result.data.token : null;
        this.rootStore.setLoader(false);
      } catch (errors) {
        this.rootStore.setLoader(false);
        throw yield ErrorsHandler.handleGenericErrors(errors)
      }
    }.bind(this)
  );

    resetPassword = asyncAction('resetPassword',
        function*({ phone}) {
            try {
                this.rootStore.setLoader(true);
                let result = yield Api.post("user/active", { phone }, false);
                if(Api.testStatus(result).error) throw Api.testStatus(result);
                this.rootStore.setLoader(false);
                return result;
            } catch (errors) {
                this.rootStore.setLoader(false);
                throw yield ErrorsHandler.handleGenericErrors(errors)
            }
        }.bind(this)
    );

    confirmActiveCode = asyncAction('confirmActiveCode',
        function*({ active_code }) {
            try {
                this.rootStore.setLoader(true);
                let result = yield Api.post("user/confirmactive", { active_code }, false);
                if(Api.testStatus(result).error) throw Api.testStatus(result);
                this.rootStore.setLoader(false);
                return result;
            } catch (errors) {
                this.rootStore.setLoader(false);
                throw yield ErrorsHandler.handleGenericErrors(errors)
            }
        }.bind(this)
    );

    changePassword = asyncAction('changePassword',
        function*({ active_code, password }) {
            try {
                this.rootStore.setLoader(true);
                let result = yield Api.post("user/changpassword", { active_code, password }, false);
                if(Api.testStatus(result).error) throw Api.testStatus(result);
                this.rootStore.setLoader(false);
                return result;
            } catch (errors) {
                this.rootStore.setLoader(false);
                throw yield ErrorsHandler.handleGenericErrors(errors)
            }
        }.bind(this)
    );

  logoutAction = asyncAction('logoutAction',
    function*() {
      try {
        this.user = Entities.UserEntity();
        this.token = null;
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors)
      }
    }.bind(this)
  );

  getPlicyAction = asyncAction('getPlicyAction',
    function*() {
      try {
        let result = yield Api.get("policy", {}, false);
        if(Api.testStatus(result).error) throw Api.testStatus(result)
        this.policy = Entities.PolicyEntity(result);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors)
      }
    }.bind(this)
  );
}
