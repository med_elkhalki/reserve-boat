import { persist } from "mobx-persist";
import { action, observable} from "mobx";
import Screens from "./../navigation/screensName";
import {asyncAction} from "mobx-utils/lib/mobx-utils";
import Api from "./../libs/api";
import * as Entities from "./../modules/notification";
import ErrorsHandler from './../libs/errorHandler';

export default class NotificationStore {
  @observable
  notifications = observable(Entities.NotificationsEntity());

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  getNotificationAction = asyncAction('getNotificationAction',
    function*() {
      try {
        let result = yield Api.get("notifications", {}, true);
        if(Api.testStatus(result).error) throw Api.testStatus(result)
        this.notifications = Entities.NotificationsEntity(result);
      } catch (errors) {
        throw yield ErrorsHandler.handleGenericErrors(errors)
      }
    }.bind(this)
  );
}
