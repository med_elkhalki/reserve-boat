import { Navigation } from "react-native-navigation";
import registerScreens from "./navigation/registerComponents";
import Screens from "./navigation/screensName";
import { reaction } from "mobx";
import Provider from "./libs/provider";
import rootStore from "./controllers/";
import { AsyncStorage } from "react-native";
import { create } from "mobx-persist";
import I18n from "./languages/";
import { LoginStack, userStack } from "./navigation/stacks";
import Api from './libs/api';
const hydrate = create({ storage: AsyncStorage });

registerScreens(rootStore.getStores(), Provider);

export default class Bootstrap {
  constructor() {
    hydrate("settings", rootStore)
      .then(async result => {
        console.log('result.lang', result.lang)
        I18n.locale = result.lang;
        Api.setLocaleLanguage(result.lang);
        if (result.authStore.token && result.authStore.user) {
          Api.token = result.authStore.token;
          Navigation.events().registerAppLaunchedListener(
            (componentId, componentName) => {
              Navigation.setRoot(userStack(result.lang));
            }
          );
        } else {
          Navigation.events().registerAppLaunchedListener(
            (componentId, componentName) => {
              Navigation.setRoot(LoginStack());
            }
          );
        }
      })
      .catch((err) => {
        Navigation.events().registerAppLaunchedListener(
          (componentId, componentName) => {
            Navigation.setRoot(LoginStack());
          }
        );
      });
  }
}
