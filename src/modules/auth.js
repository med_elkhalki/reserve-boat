export const UserEntity = data => {
  if (data) {
    return {
      IBAN: data.IBAN ? data.IBAN : null,
      active_code: data.active_code ? data.active_code : null,
      approved: data.approved ? data.approved : null,
      bank_account: data.bank_account ? data.bank_account : null,
      birth_date: data.birth_date ? data.birth_date : null,
      block: data.block ? data.block : null,
      created_at: data.created_at ? data.created_at : null,
      email: data.email ? data.email : null,
      group_id: data.group_id ? data.group_id : null,
      isAdmin: data.group_id === 4,
      id: data.id ? data.id : null,
      last_login: data.last_login ? data.last_login : null,
      mobile_token: data.mobile_token ? data.mobile_token : null,
      name: data.name ? data.name : null,
      national_id: data.national_id ? data.national_id : null,
      phone: data.phone ? data.phone : null,
      token: data.token ? data.token : null,
      updated_at: data.updated_at ? data.updated_at : null,
      verification_number: data.verification_number
        ? data.verification_number
        : null
    };
  }
  return {
    IBAN: null,
    active_code: null,
    approved: null,
    bank_account: null,
    birth_date: null,
    block: null,
    created_at: null,
    email: null,
    group_id: null,
    isAdmin: false,
    id: null,
    last_login: null,
    mobile_token: null,
    name: null,
    national_id: null,
    phone: null,
    token: null,
    updated_at: null,
    verification_number: null
  };
};

export const PolicyEntity = data => {
  let aboutUs = {};
  let humanIsAllforUs = {};
  let ourVision = {};
  let goals = {};
  let conditions = {};

  if (data && data[0]) {
    aboutUs = {
      title: data[0].title,
      body: data[0].body
    }
  }

  if (data && data[1]) {
    humanIsAllforUs = {
      title: data[1].title,
      body: data[1].body
    }
  }

  if (data && data[2]) {
    ourVision = {
      title: data[2].title,
      body: data[2].body
    }
  }

  if (data && data[3]) {
    goals = {
      title: data[3].title,
      body: data[3].body
    }
  }
  if (data && data[4]) {
    conditions = {
      title: data[4].title,
      body: data[4].body
    }
  }
  return {
     aboutUs,
     humanIsAllforUs,
     ourVision,
     goals,
     conditions
  }
};
