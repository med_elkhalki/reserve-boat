import Config from './../../config';
import {UserEntity} from './auth'

export const BoatListEntity = data => {
  let boats = [];
  if (data && data.length) {
    for (let boat of data) {
        boats.push(BoatEntity(boat));
    }
  }
  return boats;
};

export const BoatEntity = data => {
  if (data) {
      return {
        fromCity: data.from_city,
        id: data.id,
        name: data.name,
        toCity: data.to_city,
        images: ImagesEntity(data.images)
      }
  }
  return {
    fromCity: null,
    id: null,
    name: null,
    toCity: null,
    images: ImagesEntity()
  };
};

export const ImagesEntity = data => {
    let Images = []
    if(data && data.length) {
        for(let img of data) {
            Images.push(ImageEntity(img))
        }
    }
    return Images;
}
export const ImageEntity = data => {
  if (data) {
    console.log("Config.imagePath + data.img", Config.imagePath + data.img)
    return {
        boatId: data.boat_id,
        createdAt: data.created_at,
        id: data.id,
        img: Config.imagePath + data.img,
        shipId: data.ship_id,
        updatedAt: data.updated_at,
        userId: data.user_id
      };
  }
  return {
    boatId: null,
    createdAt: null,
    id: null,
    img: null,
    shipId: null,
    updatedAt: null,
    userId: null
  };
};

export const BoatDetailEntity = data => {
  if (data) {
    return {
      createdAt: data.created_at,
      crewCount: data.crew_count,
      details: data.details,
      end: data.end,
      fromCity: data.from_city,
      id: data.id,
      maintenanceEnd: data.maintenance_end,
      maintenanceStart: data.maintenance_start,
      name: data.name,
      price: data.price,
      start: data.start,
      status: data.status,
      suspended: data.suspended,
      toCity: data.to_city,
      type: data.type,
      updatedAt: data.updated_at,
      images: ImagesEntity(data.images),
      user: UserEntity(data.user),
      Annee: data.Annee,
      Carburant: data.Carburant,
      Description: data.Description,
      Largeur: data.Largeur,
      Longueur: data.Longueur,
      Marque: data.Marque,
      Motorisation: data.Motorisation
      };
  }
  return {
    createdAt: null,
    crewCount: null,
    details: null,
    end: null,
    fromCity: null,
    id: null,
    maintenanceEnd: null,
    maintenanceStart: null,
    name: null,
    price: null,
    start:null,
    status: null,
    suspended: null,
    toCity:null,
    type:null,
    updatedAt: null,
    images: ImagesEntity(),
    user: UserEntity(),
    Annee: null,
    Carburant: null,
    Description: null,
    Largeur: null,
    Longueur: null,
    Marque: null,
    Motorisation: null
  };
};