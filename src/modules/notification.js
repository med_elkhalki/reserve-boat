export const NotificationsEntity = data => {
  let notifications = [];
  if (data && data.length) {
    for (let notif of data) {
      notifications.push({
        IBAN: notif.IBAN,
        bankAccount: notif.bank_account,
        birthDate: notif.birth_date,
        crewCount: notif.crewCount,
        day: notif.day,
        email: notif.email,
        fromCity: notif.from_city,
        name: notif.name,
        nationalId: notif.national_id,
        notificationId: notif.notificationId,
        notiyBody: notif.notiyBody,
        notiyId: notif.notiyId,
        notiyTime: notif.notiyTime,
        notiyTitle: notif.notiyTitle,
        notiyType: notif.notiyType,
        phone: notif.phone,
        read: notif.read,
        shipId: notif.shipId,
        shipReserve_id: notif.shipReserve_id,
        start: notif.start,
        toCity: notif.to_city,
        totalPrice: notif.totalPrice
      });
    }
  }
  return notifications;
};
