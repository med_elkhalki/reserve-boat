import * as userEntites from "./auth";
import moment from "moment";

export const ReservationsEntity = data => {
  console.log(data);
  let reservations = [];
  if (data)
    if (data.length) {
      for (let reserv of data) {
        reservations.push({
          attendance: reserv.attendance,
          bankAccount: reserv.bank_account,
          createdAt: moment(reserv.created_at).format("MMM Do YY"),
          crewCount: reserv.crew_count,
          day: moment(reserv.day).format("MMM Do YY HH:mm"),
          id: reserv.id,
          return: reserv.return,
          ship: shipEntity(reserv.ship),
          shipId: reserv.ship_id,
          status: reserv.status,
          totalPrice: reserv.total_price,
          updatedAt: moment(reserv.updated_at).format("MMM Do YY"),
          userId: reserv.user_id
        });
      }
    } else {
      let keys = Object.keys(data);
      for (let index of keys) {
        reservations.push({
          attendance: data[index].attendance,
          bankAccount: data[index].bank_account,
          createdAt: moment(data[index].created_at).format("HH:mm"),
          crewCount: data[index].crew_count,
          day: moment(data[index].day).format("MMM Do YY"),
          id: data[index].id,
          return: data[index].return,
          ship: shipEntity(data[index].ship),
          shipId: data[index].ship_id,
          status: data[index].status,
          totalPrice: data[index].total_price,
          updatedAt: moment(data[index].updated_at).format("MMM Do YY"),
          userId: data[index].user_id
        });
      }
    }

  return reservations;
};

export const MySellEntity = data => {
  console.log(data);
  let sell = [];
  if (data)
    if (data.length) {
      for (let reserv of data) {
        sell.push({
          attendance: reserv.attendance,
          bankAccount: reserv.bank_account,
          createdAt: moment(reserv.created_at).format("HH:mm"),
          crewCount: reserv.crew_count,
          day: moment(reserv.day).format("MMM Do YY"),
          id: reserv.id,
          return: reserv.return,
          ship: shipEntity(reserv.ship),
          shipId: reserv.ship_id,
          status: reserv.status,
          totalPrice: reserv.total_price,
          updatedAt: moment(reserv.updated_at).format("MMM Do YY"),
          userId: reserv.user_id
        });
      }
    } else {
      let keys = Object.keys(data);
      for (let index of keys) {
        sell.push({
          attendance: data[index].attendance,
          bankAccount: data[index].bank_account,
          createdAt: moment(data[index].created_at).format("HH:mm"),
          crewCount: data[index].crew_count,
          day: moment(data[index].day).format("MMM Do YY"),
          id: data[index].id,
          return: data[index].return,
          ship: shipEntity(data[index].ship),
          shipId: data[index].ship_id,
          status: data[index].status,
          totalPrice: data[index].total_price,
          updatedAt: moment(data[index].updated_at).format("MMM Do YY"),
          userId: data[index].user_id
        });
      }
    }
  return sell;
};

export const shipEntity = data => {
  if (data) {
    return {
      end: data.end,
      id: data.id,
      //images:
      name: data.name,
      start: data.start
    };
  }
  return {
    end: null,
    id: null,
    //images:
    name: null,
    start: null
  };
};

export const ReservationEntity = data => {
  if (data) {
    return {
      attendance: data.attendance,
      bankAccount: data.bank_account,
      createdAt: data.created_at,
      crewCount: data.crew_count,
      day: data.day,
      id: data.id,
      return: data.return,
      ship: shipEntity(data.ship),
      shipId: data.ship_id,
      status: data.status,
      totalPrice: data.total_price,
      updatedAt: data.updated_at,
      userId: data.user_id,
      user: userEntites.UserEntity(data.user)
    };
  }
  return {
    attendance: null,
    bankAccount: null,
    createdAt: null,
    crewCount: null,
    day: null,
    id: null,
    return: null,
    ship: shipEntity(null),
    shipId: null,
    status: null,
    totalPrice: null,
    updatedAt: null,
    userId: null,
    user: userEntites.UserEntity(null)
  };
};
