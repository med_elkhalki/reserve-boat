import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  AlertIOS,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/splash.png";
import Logo from "./../../images/logo.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import { inject, observer } from "mobx-react/native";
import Screens from "./../../navigation/screensName";
import { Navigation } from "react-native-navigation";
import I18n from "./../../languages/";
import { userStack } from "./../../navigation/stacks";
import RNPickerSelect from "./../../libs/picker";
import countries from "./../../libs/countries";
import Phone from "./../../images/auth/phone.png";
import Password from "./../../images/auth/password.png";
import User from "./../../images/auth/user.png";

@inject(stores => ({
  authStore: stores.authStore,
  rootStore: stores.rootStore
}))
@observer
export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: null,
      password: null,
      code: null,
    };
  }

  setPhoneNum(phone) {
    this.setState({ phone });
  }
  setPassword(password) {
    this.setState({ password });
  }
  setCodeNumer(code) {
    this.setState({ code });
  }

  login() {
    if(this.props.rootStore.isLoading) return;
    if(!this.state.phone || !this.state.password || !this.state.code) {
      AlertIOS.alert(
        I18n.t("errors.login.requiredTitle"),
        I18n.t("errors.login.requiredMessage"),
        [
          {
            text: I18n.t("errors.login.cancel"),
            onPress: () => {},
            style: 'cancel',
          },
        ]
      );
      return
    }
    this.props.authStore
      .loginAction({ phone: this.state.code.concat(this.state.phone), password: this.state.password })
      //user
      //.loginAction({ phone: "+212615537398", password: "password" })
      //admin
      //.loginAction({ phone: "+212615259099", password: "password"})
      .then(() => {
        this.props.rootStore.setCurrentScreen(Screens.boat.home);
        this.props.rootStore.navigate(
          this.props.componentId,
          userStack(this.props.rootStore.lang),
          "setRoot"
        );
      })
      .catch(error => {
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 400) {
          let messageError = "";
          if (error && error.dataErrors.has("permission"))
            messageError += error.dataErrors.get("permission") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("phone"))
            messageError += error.dataErrors.get("phone") + "\n";
          if (error && error.dataErrors.has("password"))
            messageError += error.dataErrors.get("password") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        return;
      });
  }

  goSignup() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.auth.signup,
          options: {
            topBar: {
              visible: false
            }
          }
        }
      },
      "push"
    );
  }

  goToForgotPassword() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.auth.forgotPassword,
          options: {
            topBar: {
              visible: false
            }
          }
        }
      },
      "push"
    );
  }

  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <FastImage source={Background} style={styles.backgroundImage}>
          <FastImage source={Logo} style={styles.logo} />
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signin.phone")}
              keyboardType={"numeric"}
              placeholderTextColor={"white"}
              onChangeText={this.setPhoneNum.bind(this)}
              value={this.state.phone}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={Phone} style={styles.icon} />
            </View>
            <TouchableOpacity
              style={
                this.props.rootStore.position === "left"
                  ? styles.circlePhoneCountry
                  : styles.circleLeftPhoneCountry
              }
            >
              <RNPickerSelect
                hideIcon
                onValueChange={this.setCodeNumer.bind(this)}
                placeholder={{
                  label: I18n.t("signin.select"),
                  value: null
                }}
                doneText={I18n.t("signin.selectTitle")}
                items={countries}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signin.password")}
              secureTextEntry
              autoCapitalize={"none"}
              placeholderTextColor={"white"}
              onChangeText={this.setPassword.bind(this)}
              value={this.state.password}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={Password} style={styles.icon} />
            </View>
          </View>
          <TouchableOpacity
            style={
              this.props.rootStore.position === "right"
                ? styles.linkContainer
                : styles.linkContainerLeft
            }
            onPress={this.goToForgotPassword.bind(this)}
          >
            <Text style={styles.link}>{I18n.t("signin.forgotPassword")}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={this.login.bind(this)}
          >
            {this.props.rootStore.isLoading ?
            <ActivityIndicator color={Vars.mainColor} /> :
            <Text style={styles.buttonText}>{I18n.t("signin.go")}</Text> 
          }
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.linkCenterContainer}
            onPress={this.goSignup.bind(this)}
          >
            <Text style={styles.link}>{I18n.t("signin.signup")}</Text>
          </TouchableOpacity>
        </FastImage>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 80
  },
  inputContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20
  },
  input: {
    backgroundColor: "#ffffff3b",
    height: 45,
    borderRadius: 25,
    textAlign: "center",
    color: "white"
  },
  circle: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  circleLeft: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomLeftRadius: 25,
    position: "absolute",
    left: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  circlePhoneCountry: {
    width: 80,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  countryText: {
    color: Vars.mainColor,
    fontSize: 12
  },
  circleLeftPhoneCountry: {
    width: 80,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomLeftRadius: 25,
    position: "absolute",
    left: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  link: {
    color: "white",
    fontSize: 12,
    marginBottom: 20
  },
  button: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20,
    height: 45,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  buttonText: {
    color: Vars.mainColor
  },
  linkContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-end"
  },
  linkContainerLeft: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-start"
  },
  linkCenterContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "center"
  },
  icon: {
    width: 20,
    height: 20
  }
});
