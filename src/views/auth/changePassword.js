import {
    View,
    Text,
    ImageBackground,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator, AlertIOS
} from 'react-native';
import React, {Component} from 'react';
import Image from './../../images/settings/authorization.png';
import Vars from '../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from '../common/header';
import I18n from '../../languages';
import {inject, observer} from 'mobx-react/native';
import Screens from "./../../navigation/screensName";

@inject(stores => ({
    rootStore: stores.rootStore,
    authStore: stores.authStore,
}))
@observer
export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.isSamePassword = this.isSamePassword.bind(this);
        this.state = {
            active_code:this.props.active_code,
            password: null,
            confirmed_password: null
        };
    }

    setConfirmedPassword(confirmed_password) {
        this.setState({ confirmed_password });
    }

    setPassword(password) {
        this.setState({ password });
    }

    isSamePassword(){
        return this.state.password === this.state.confirmed_password
    }

    changePassword() {
        if(this.props.rootStore.isLoading) return;
        if (!this.state.active_code || !this.state.password || !this.state.confirmed_password) {
            AlertIOS.alert(
                I18n.t("errors.login.requiredTitle"),
                I18n.t("errors.login.requiredMessage"),
                [
                    {
                        text: I18n.t("errors.login.cancel"),
                        onPress: () => {},
                        style: "cancel"
                    }
                ]
            );
            return;
        }
        if (!this.isSamePassword()) {
            AlertIOS.alert(
                I18n.t("errors.changePassword.errorTitle"),
                I18n.t("errors.changePassword.message"),
                [
                    {
                        text: I18n.t("errors.changePassword.cancel"),
                        onPress: () => {},
                        style: "cancel"
                    }
                ]
            );
            return;
        }
        this.props.authStore
            .changePassword({
                active_code: this.state.active_code,
                password: this.state.password
            })
            .then((data) => {
                if(data && data.status === 1){
                    this.goToLogin();
                }
            })
            .catch(error => {
                if (error && error.dataErrors.has("server_error")) {
                    AlertIOS.alert(
                        I18n.t("errors.errorTitle"),
                        error.dataErrors.get("server_error"),
                        [
                            {
                                text: I18n.t("errors.cancel"),
                                onPress: () => {},
                                style: "cancel"
                            }
                        ]
                    );
                    return;
                }
                if (error && error.status === 400) {
                    let messageError = "";
                    if (error && error.dataErrors.has("permission"))
                        messageError += error.dataErrors.get("permission") + "\n";
                    AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
                        {
                            text: I18n.t("errors.cancel"),
                            onPress: () => {},
                            style: "cancel"
                        }
                    ]);
                }
                if (error && error.status === 422) {
                    let messageError = "";
                    if (error && error.dataErrors.has("active_code"))
                        messageError += error.dataErrors.get("active_code") + "\n";
                    if (error && error.dataErrors.has("password"))
                        messageError += error.dataErrors.get("password") + "\n";
                    AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
                        {
                            text: I18n.t("errors.cancel"),
                            onPress: () => {},
                            style: "cancel"
                        }
                    ]);
                }
                return;
            });
    }

    goToLogin(){
        this.props.rootStore.navigate(
            this.props.componentId,
            {
                component: {
                    name: Screens.auth.signin,
                    options: {
                        topBar: {
                            visible: false
                        }
                    }
                }
            },
            "push"
        );
    }

    render() {
        return <View style={styles.container}>
            <Header componentId={this.props.componentId} title={I18n.t('changePassword.title')} backButton/>
            <View style={styles.imageContainer}>
                <FastImage source={Image} style={styles.image}/>
            </View>
            <Text style={styles.title}>
                {I18n.t('changePassword.description')}
            </Text>
            <TextInput style={styles.input}
                       placeholder={I18n.t('changePassword.newPassword')}
                       secureTextEntry
                       onChangeText={this.setPassword.bind(this)}
                       value={this.state.password}/>
            <TextInput style={styles.input}
                       secureTextEntry
                       placeholder={I18n.t('changePassword.confirmPassword')}
                       onChangeText={this.setConfirmedPassword.bind(this)}
                       value={this.state.confirmed_password}/>
            <TouchableOpacity style={styles.button} onPress={this.changePassword.bind(this)}>
                {
                    this.props.rootStore.isLoading ?
                        <ActivityIndicator color={'white'} />
                        :
                        <Text style={styles.buttonText}>
                            {I18n.t('changePassword.confirmation')}
                        </Text>
                }
            </TouchableOpacity>
        </View>;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 150,
        alignItems: 'center',
    },
    imageContainer: {
        paddingHorizontal: 30
    },
    image: {
        width: Vars.width * .75,
        height: Vars.width * .4
    },
    button: {
        backgroundColor: Vars.mainColor,
        borderRadius: 20,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        marginTop: 10
    },
    input: {
        height: 45,
        width: Vars.width - 50,
        backgroundColor: '#fafafa',
        borderColor: '#eee',
        borderWidth: .5,
        borderRadius: 22,
        textAlign: 'center',
        marginVertical: 10
    },
    title: {
        marginBottom: 20,
        marginTop: 50,
    },
    subTitle: {
        color: '#ccc',
        fontSize: 11,
        marginBottom: 50,
        paddingHorizontal: 30,
        textAlign: 'center'
    },
    buttonText: {
        color: 'white',
    }
})