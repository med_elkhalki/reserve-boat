import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  AlertIOS,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/splash.png";
import Logo from "./../../images/logo.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import I18n from "./../../languages/";
import { inject, observer } from "mobx-react/native";
import { userStack } from "./../../navigation/stacks";
import RNPickerSelect from "./../../libs/picker";
import countries from "./../../libs/countries";
import Phone from "./../../images/auth/phone.png";
import Password from "./../../images/auth/password.png";
import User from "./../../images/auth/user.png";
import Line from "./../../images/auth/line.png";
import Screens from "./../../navigation/screensName";
import Header from "../common/header";

@inject(stores => ({
  authStore: stores.authStore,
  rootStore: stores.rootStore
}))
@observer
export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: null,
      password: null,
      code: null,
      name: null,
      confirm: null,
      role: 1,
      email: null
    };
  }

  signup() {
    if(this.props.rootStore.isLoading ) return
    if (
      !this.state.phone ||
      !this.state.password ||
      !this.state.code ||
      !this.state.email ||
      !this.state.confirm ||
      !this.state.name ||
      this.state.confirm !== this.state.password
    ) {
      AlertIOS.alert(
        I18n.t("errors.login.requiredTitle"),
        I18n.t("errors.login.requiredMessage"),
        [
          {
            text: I18n.t("errors.login.cancel"),
            onPress: () => {},
            style: "cancel"
          }
        ]
      );
      return;
    }
    this.props.authStore
      .signupAction({
        phone: this.state.code.concat(this.state.phone),
        password: this.state.password,
        password_confirmation: this.state.confirm,
        name: this.state.name,
        email: this.state.email,
        group_id: this.state.role
      })
      .then(() => {
        this.props.rootStore.setCurrentScreen(Screens.boat.home);
        this.props.rootStore.navigate(
          this.props.componentId,
          userStack(this.props.rootStore.lang),
          "setRoot"
        );
      })
      .catch(error => {
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 400) {
          let messageError = "";
          if (error && error.dataErrors.has("permission"))
            messageError += error.dataErrors.get("permission") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("phone"))
            messageError += error.dataErrors.get("phone") + "\n";
          if (error && error.dataErrors.has("password"))
            messageError += error.dataErrors.get("password") + "\n";
          if (error && error.dataErrors.has("confirm"))
            messageError += error.dataErrors.get("confirm") + "\n";
          if (error && error.dataErrors.has("name"))
            messageError += error.dataErrors.get("name") + "\n";
          if (error && error.dataErrors.has("email"))
            messageError += error.dataErrors.get("email") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        return;
      });
  }

  setPhoneNum(phone) {
    this.setState({ phone });
  }
  setPassword(password) {
    this.setState({ password });
  }
  setUsername(name) {
    this.setState({ name });
  }
  setEmail(email) {
    this.setState({ email });
  }
  setConfirm(confirm) {
    this.setState({ confirm });
  }
  setCodeNumer(code) {
    this.setState({ code });
  }
  setRole(role) {
    this.setState({ role });
  }

  goToForgotPassword() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.auth.forgotPassword,
          options: {
            topBar: {
              visible: false
            }
          }
        }
      },
      "push"
    );
  }

  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <FastImage source={Background} style={styles.backgroundImage}>
        <Header
          componentId={this.props.componentId}
          login
        />
          <FastImage source={Logo} style={styles.logo} />
          <View style={styles.checkboxContainer}>
            <FastImage source={Line} style={styles.line} />
            <View style={styles.buttonsContainer}>
              <TouchableOpacity onPress={this.setRole.bind(this, 1)}>
                <Text
                  style={
                    this.state.role === 1
                      ? styles.buttonText
                      : styles.disalebuttonText
                  }
                >
                  {I18n.t("signup.user")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.setRole.bind(this, 4)}>
                <Text
                  style={
                    this.state.role === 4
                      ? styles.buttonText
                      : styles.disalebuttonText
                  }
                >
                  {I18n.t("signup.admin")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signup.username")}
              placeholderTextColor={"white"}
              autoCapitalize={"none"}
              onChangeText={this.setUsername.bind(this)}
              value={this.state.name}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={User} style={styles.icon} />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signup.email")}
              autoCapitalize={"none"}
              placeholderTextColor={"white"}
              onChangeText={this.setEmail.bind(this)}
              value={this.state.email}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={User} style={styles.icon} />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signup.phone")}
              keyboardType={"numeric"}
              placeholderTextColor={"white"}
              onChangeText={this.setPhoneNum.bind(this)}
              value={this.state.phone}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={Phone} style={styles.icon} />
            </View>
            <TouchableOpacity
              style={
                this.props.rootStore.position === "left"
                  ? styles.circlePhoneCountry
                  : styles.circleLeftPhoneCountry
              }
            >
              <RNPickerSelect
                hideIcon
                onValueChange={this.setCodeNumer.bind(this)}
                placeholder={{
                  label: I18n.t("signin.select"),
                  value: null
                }}
                doneText={I18n.t("signin.selectTitle")}
                items={countries}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signup.password")}
              secureTextEntry
              autoCapitalize={"none"}
              placeholderTextColor={"white"}
              onChangeText={this.setPassword.bind(this)}
              value={this.state.password}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={Password} style={styles.icon} />
            </View>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder={I18n.t("signup.confirm")}
              secureTextEntry
              autoCapitalize={"none"}
              placeholderTextColor={"white"}
              onChangeText={this.setConfirm.bind(this)}
              value={this.state.confirm}
            />
            <View
              style={
                this.props.rootStore.position === "right"
                  ? styles.circle
                  : styles.circleLeft
              }
            >
              <FastImage source={Password} style={styles.icon} />
            </View>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={this.signup.bind(this)}
          >
          {this.props.rootStore.isLoading ?
            <ActivityIndicator color={Vars.mainColor} /> :
          <Text style={styles.buttonText}>{I18n.t("signup.save")}</Text> }
          </TouchableOpacity>


          <TouchableOpacity
            style={
              this.props.rootStore.position === "right"
                ? styles.linkContainer
                : styles.linkContainerLeft
            }
            onPress={this.goToForgotPassword.bind(this)}
          >
            <Text style={styles.link}>{I18n.t("signin.forgotPassword")}</Text>
          </TouchableOpacity>


        </FastImage>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 40
  },
  inputContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20
  },
  input: {
    backgroundColor: "#ffffff3b",
    height: 45,
    borderRadius: 25,
    textAlign: "center",
    color: "white"
  },
  circle: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  circleLeft: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomLeftRadius: 25,
    position: "absolute",
    left: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  link: {
    color: "white",
    fontSize: 12,
    marginBottom: 20
  },
  button: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20,
    height: 45,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  buttonText: {
    color: Vars.mainColor
  },
  linkContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-end"
  },
  circlePhoneCountry: {
    width: 80,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  countryText: {
    color: Vars.mainColor,
    fontSize: 12
  },
  circleLeftPhoneCountry: {
    width: 80,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomLeftRadius: 25,
    position: "absolute",
    left: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    width: 20,
    height: 20
  },
  checkboxContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  line: {
    height: 10,
    width: 200
  },
  linkContainerLeft: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-start"
  },
  buttonsContainer: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    width: 220,
    marginVertical: 10
  },
  disalebuttonText: {
    color: "white"
  }
});
