import {
    View,
    Text,
    ImageBackground,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    AlertIOS,
    ActivityIndicator
} from 'react-native';
import React, {Component} from 'react';
import Image from './../../images/settings/authorization.png';
import Vars from '../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from '../common/header';
import I18n from '../../languages';
import { inject, observer } from 'mobx-react/native';
import Screens from "./../../navigation/screensName";
import {userStack} from "../../navigation/stacks";


@inject(stores => ({
    rootStore: stores.rootStore,
    authStore: stores.authStore
}))
@observer
export default class RestPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: null
        };
    }

    goToResetPassword() {
        this.props.rootStore.navigate(
          this.props.componentId,
          {
            component: {
              name: Screens.auth.resetPassword,
              options: {
                topBar: {
                  visible: false
                }
              }
            }
          },
          "push"
        );
      }

      goToValidateCode() {
        this.props.rootStore.navigate(
            this.props.componentId,
            {
                component: {
                    name: Screens.auth.validateCode,
                    options: {
                        topBar: {
                            visible: false
                        }
                    }
                }
            },
            "push"
        );
    }

    resetPassword() {
        if(this.props.rootStore.isLoading) return;
        if (!this.state.phone) {
            AlertIOS.alert(
                I18n.t("errors.login.requiredTitle"),
                I18n.t("errors.login.requiredMessage"),
                [
                    {
                        text: I18n.t("errors.login.cancel"),
                        onPress: () => {},
                        style: "cancel"
                    }
                ]
            );
            return;
        }
        this.props.authStore
            .resetPassword({
                phone: this.state.phone
            })
            .then((data) => {
                if(data && data.status === 1){
                    this.goToValidateCode();
                }
            })
            .catch(error => {
                if (error && error.dataErrors.has("server_error")) {
                    AlertIOS.alert(
                        I18n.t("errors.errorTitle"),
                        error.dataErrors.get("server_error"),
                        [
                            {
                                text: I18n.t("errors.cancel"),
                                onPress: () => {},
                                style: "cancel"
                            }
                        ]
                    );
                    return;
                }
                if (error && error.status === 400) {
                    let messageError = "";
                    if (error && error.dataErrors.has("permission"))
                        messageError += error.dataErrors.get("permission") + "\n";
                    AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
                        {
                            text: I18n.t("errors.cancel"),
                            onPress: () => {},
                            style: "cancel"
                        }
                    ]);
                }
                if (error && error.status === 422) {
                    let messageError = "";
                    if (error && error.dataErrors.has("phone"))
                        messageError += error.dataErrors.get("phone") + "\n";
                    AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
                        {
                            text: I18n.t("errors.cancel"),
                            onPress: () => {},
                            style: "cancel"
                        }
                    ]);
                }
                return;
            });
    }

    setPhone(phone) {
        this.setState({ phone });
    }

    render() {
        return (
            <View style={styles.container}>
                <Header componentId={this.props.componentId} title={I18n.t('resetPassword.title')} backButton/>
                <View style={styles.imageContainer}>
                    <FastImage  source={Image} style={styles.image}/>
                </View>
                <Text style={styles.title}>{I18n.t('resetPassword.description')}</Text>
                {/*<TextInput style={styles.input} placeholder={I18n.t('resetPassword.phone')} />
                <Text style={styles.or}>{I18n.t('resetPassword.or')}</Text>*/}
                <TextInput style={styles.input}
                           placeholder={I18n.t('resetPassword.phone')}
                           onChangeText={this.setPhone.bind(this)}
                           value={this.state.phone}/>
                <TouchableOpacity style={styles.button} onPress={this.resetPassword.bind(this)}>

                    {this.props.rootStore.isLoading ?
                        <ActivityIndicator color={'white'} /> :
                        <Text style={styles.buttonText}>{I18n.t('resetPassword.send')}</Text>
                    }
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 150,
        alignItems: 'center',
    },
    imageContainer: {
        paddingHorizontal: 30
    },
    image: {
        width: Vars.width * .75,
        height: Vars.width * .4
    },
    button: {
        backgroundColor: Vars.mainColor,
        borderRadius: 20,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        marginTop: 10
    },
    input: {
        height: 45,
        width: Vars.width - 50,
        backgroundColor: '#fafafa',
        borderColor: '#eee',
        borderWidth: .5,
        borderRadius: 22,
        textAlign: 'center',
        marginVertical: 10
    },
    title: {
        marginBottom: 20,
        marginTop: 50,
    },
    subTitle: {
        color: '#ccc',
        fontSize: 11,
        marginBottom: 50,
        paddingHorizontal: 30,
        textAlign: 'center'
    },
    buttonText: {
        color: 'white',
    }
})