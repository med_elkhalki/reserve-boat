import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  AlertIOS,
  ActivityIndicator,
  KeyboardAvoidingView
} from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/reservation/banner2.png";
import Background from "./../../images/background.png";
import Line from "./../../images/reservation/line2.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "./../common/header";
import I18n from "./../../languages/";
import { inject, observer } from "mobx-react/native";
import InputValidation from "./../common/inputValidation";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";

@inject(stores => ({
  rootStore: stores.rootStore,
  reservationsStore: stores.reservationsStore
}))
@observer
export default class ResevBoat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      nameError: false,
      identity: "",
      identityError: false,
      birthDate: "",
      birthDateError: false,
      isbirthDate: false,
      phone: "",
      phoneError: false
    };
  }


  setBirthDate(date) {
    this.setState({
      birthDate: date ? moment(date).format("MMM Do YY") : "",
      isbirthDate: false,
      birthDateError: date ? true : I18n.t("errors.InvalidEntry")
    });
  }

  clear() {
    this.setState({
      name: "",
      nameError: false,
      identity: "",
      identityError: false,
      birthDate: "",
      birthDateError: false,
      isbirthDate: false,
      phone: "",
      phoneError: false
    });
  }

  reservShip() {
    if(this.props.rootStore.isLoading) return
    let TESTIFINPUTISEMPTY =
      !this.state.name.length ||
      !this.state.identity.length ||
      !this.state.phone.length ||
      !this.state.birthDate.length;
    let TESTIFERROR =
      this.state.nameError &&
      this.state.identityError &&
      this.state.phoneError &&
      this.state.birthDateError !== I18n.t("errors.InvalidEntry");
    if (TESTIFINPUTISEMPTY) {
      AlertIOS.alert(
        I18n.t("errors.reservBoat.requiredTitle"),
        I18n.t("errors.reservBoat.requiredMessage"),
        [
          {
            text: I18n.t("errors.cancel"),
            onPress: () => {},
            style: "cancel"
          }
        ]
      );
      return;
    }
    if (!TESTIFERROR) {
      return;
    }

    this.props.reservationsStore
      .reserveShipAction({
        ship_id: this.props.id,
        day: new Date(), 
        start: new Date(),
        crew_count: 1,
        total_price: 1
      })
      .then(() => {
        this.clear();
        AlertIOS.alert(
          I18n.t("success.reservBoat.messageTitle"),
          I18n.t("success.reservBoat.message"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
      })
      .catch(error => {
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("ship_id"))
            messageError += error.dataErrors.get("ship_id") + "\n";
          if (error && error.dataErrors.has("day"))
            messageError += error.dataErrors.get("day") + "\n";
          if (error && error.dataErrors.has("start"))
            messageError += error.dataErrors.get("start") + "\n";
          if (error && error.dataErrors.has("crew_count"))
            messageError += error.dataErrors.get("crew_count") + "\n";
          if (error && error.dataErrors.has("total_price"))
            messageError += error.dataErrors.get("total_price") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        return;
      });
  }

  render() {
    return (
      <FastImage source={Background} style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("reserveBoat.title")}
          backButton
          notification
        />
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
        <ScrollView>
          <FastImage source={HeaderImage} style={styles.headerImage} />
          {/* NAME  */}
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="username"
                  placeholder={I18n.t("reserveBoat.namePlaceholder")}
                  value={this.state.name}
                  isArabic={true}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={name => this.setState({ name })}
                  style={styles.input}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.name")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.name")}
                </Text>
                <InputValidation
                  validator="username"
                  isArabic={false}
                  placeholder={I18n.t("reserveBoat.namePlaceholder")}
                  value={this.state.name}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={name => this.setState({ name })}
                  style={styles.input}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>
          {/* IDENTITY */}
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="username"
                  placeholder={I18n.t("reserveBoat.identityPlaceholder")}
                  value={this.state.identity}
                  isArabic={true}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={identity => this.setState({ identity })}
                  style={styles.input}
                  onValidatorExecuted={identityError =>
                    this.setState({ identityError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.identity")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.identity")}
                </Text>
                <InputValidation
                  validator="username"
                  placeholder={I18n.t("reserveBoat.identityPlaceholder")}
                  value={this.state.identity}
                  isArabic={false}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={identity => this.setState({ identity })}
                  style={styles.input}
                  onValidatorExecuted={identityError =>
                    this.setState({ identityError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>
                  
          {/* Birhdate */}

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <TouchableOpacity
                style={styles.inputContent}
                onPress={() => this.setState({ isbirthDate: true })}
              >
                <DateTimePicker
                  isVisible={this.state.isbirthDate}
                  onConfirm={this.setBirthDate.bind(this)}
                  onCancel={this.setBirthDate.bind(this)}
                />
                <Text
                  style={[
                    styles.date,
                    this.state.birthDateError.length && { color: "red" }
                  ]}
                >
                  {this.state.birthDateError.length
                    ? this.state.birthDateError
                    : this.state.birthDate}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.birthDate")}
                </Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.inputContent}
                onPress={() => this.setState({ isbirthDate: true })}
              >
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.birthDate")}
                </Text>
                <Text
                  style={[
                    styles.date,
                    this.state.birthDateError.length && { color: "red" }
                  ]}
                >
                  {this.state.birthDateError.length
                    ? this.state.birthDateError
                    : this.state.birthDate}
                </Text>
                <DateTimePicker
                  isVisible={this.state.isbirthDate}
                  onConfirm={this.setBirthDate.bind(this)}
                  onCancel={this.setBirthDate.bind(this)}
                />
              </TouchableOpacity>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          {/* phone */}
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="phone"
                  placeholder={I18n.t("reserveBoat.phonePlaceholder")}
                  value={this.state.phone}
                  isArabic={true}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={phone => this.setState({ phone })}
                  style={styles.input}
                  keyboardType={'phone-pad'}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.phone")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("reserveBoat.phone")}
                </Text>
                <InputValidation
                  validator="phone"
                  placeholder={I18n.t("reserveBoat.phonePlaceholder")}
                  value={this.state.phone}
                  isArabic={false}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={phone => this.setState({ phone })}
                  style={styles.input}
                  keyboardType={'phone-pad'}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>
        </ScrollView>

        </KeyboardAvoidingView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.reservShip.bind(this)}
          >
            {this.props.rootStore.isLoading ? (
              <ActivityIndicator color={"white"} />
            ) : (
              <Text style={styles.buttonText}>
                {I18n.t("reserveBoat.reserveNow")}
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  headerImage: {
    width: Vars.width,
    height: 200,
    marginBottom: 10,
    paddingVertical: 30,
    paddingHorizontal: 20
  },
  hr: {
    width: Vars.width - 50,
    height: 7
  },
  selectBoxContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
    marginBottom: 5,
    flexDirection: "row",
    paddingHorizontal: 25
  },
  inputContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  inputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: Vars.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25
  },
  containerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    flex: 1,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  date: {
    flex: 1,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    paddingVertical: 15
  },
  inputTitle: {},
  button: {
    height: 30,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 30
  },
  buttonContainer: {
    height: 80,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "white"
  },
  selectbox: {
    borderBottomColor: "#eee",
    borderBottomWidth: 0.5,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    minWidth: 100
  },
  errorInputContainerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  counterContainer: {
    marginHorizontal: 10,
    borderColor: Vars.mainColor,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30
  },
  itemsContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});
