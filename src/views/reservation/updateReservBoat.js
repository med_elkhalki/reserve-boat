import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  AlertIOS
} from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/reservation/banner2.png";
import Background from "./../../images/background.png";
import Line from "./../../images/reservation/line2.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "./../common/header";
import I18n from "./../../languages/";
import { inject, observer } from "mobx-react/native";

@inject(stores => ({
  rootStore: stores.rootStore,
  reservationsStore: stores.reservationsStore,
  authStore: stores.authStore
}))
@observer
export default class EditResevBoat extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.reservationsStore.getReservationDetailAction(this.props.id);
  }

  accept() {
    this.props.reservationsStore.confirmdivereserveAction(this.props.id).then(() => {
      this.refreshReservationList();
      this.props.rootStore.navigate(
        this.props.componentId,
        {},
        "pop"
      );
    }).catch((error) => {
      if (error && error.dataErrors.has("server_error")) {
        AlertIOS.alert(
          I18n.t("errors.errorTitle"),
          error.dataErrors.get("server_error"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
        return;
      }
    });
  }

  cancel() {
    this.props.reservationsStore.canceldivereserveAction(this.props.id).then(() => {
      this.refreshReservationList();
      this.props.rootStore.navigate(
        this.props.componentId,
        {},
        "pop"
      );
    }).catch((error) => {
      if (error && error.dataErrors.has("server_error")) {
        AlertIOS.alert(
          I18n.t("errors.errorTitle"),
          error.dataErrors.get("server_error"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
        return;
      }
    });
  }

  refreshReservationList() {
    if (this.props.authStore.user.isAdmin) {
      this.props.reservationsStore.getMyReservationsAction();
    } else {
      this.props.reservationsStore.getReservationsAction();
    }
  }

  render() {
    return (
      <FastImage source={Background} style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("updateReserveBoat.title")}
          backButton
          notification
        />
        <ScrollView style={styles.content}>
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.user.name}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.name")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.name")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.user.name}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.attendance}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.identity")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.identity")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.attendance}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.crewCount}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.numberOfPerson")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.numberOfPerson")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.crewCount}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.bankAccount}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.bankAccount")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.bankAccount")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.bankAccount}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.totalPrice}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.totalPricePlaceholder")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.totalPricePlaceholder")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.totalPrice}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.createdAt}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.reservDate")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.reservDate")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.createdAt}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.createdAt}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.reservTime")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.reservTime")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.createdAt}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {
                    this.props.reservationsStore.reservationsDetail.user
                      .birth_date
                  }
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.birthDate")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.birthDate")}
                </Text>
                <Text style={styles.input}>
                  {
                    this.props.reservationsStore.reservationsDetail.user
                      .birth_date
                  }
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.user.phone}
                </Text>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.phone")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("updateReserveBoat.phone")}
                </Text>
                <Text style={styles.input}>
                  {this.props.reservationsStore.reservationsDetail.user.phone}
                </Text>
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>
        </ScrollView>

        {this.props.authStore.user.isAdmin ? (
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.button} onPress={this.accept.bind(this)}>
              <Text style={styles.buttonText}>
                {I18n.t("updateReserveBoat.accept")}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={this.cancel.bind(this)}>
              <Text style={styles.buttonText}>
                {I18n.t("updateReserveBoat.reject")}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  content: {
    marginBottom: 10,
    paddingTop: 30
  },
  hr: {
    width: Vars.width - 50,
    height: 7
  },
  selectBoxContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
    marginBottom: 5,
    flexDirection: "row",
    paddingHorizontal: 25
  },
  inputContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  inputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: Vars.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25
  },
  input: {
    flex: 2,
    textAlign: "center",
    color: "#888"
  },
  inputTitle: {},
  button: {
    height: 30,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    marginHorizontal: 5
  },
  buttonContainer: {
    height: 80,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: 'row'
  },
  buttonText: {
    color: "white",
  },
  selectbox: {
    borderBottomColor: "#eee",
    borderBottomWidth: 0.5,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    minWidth: 100
  }
});
