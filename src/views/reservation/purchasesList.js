import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  FlatList
} from "react-native";
import React, { Component } from "react";
import Profile from "./../../images/notifications/profile_pic.png";
import Line from "./../../images/notifications/line.png";
import Arrow from "./../../images/notifications/arrow.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import I18n from "../../languages";
import { inject, observer } from "mobx-react/native";
import Screens from "../../navigation/screensName";
import Logo from "./../../images/sideBar/logobackground.png";
import LostConnection from "../settings/lostConnection";

@inject(stores => ({
  rootStore: stores.rootStore,
  reservationsStore: stores.reservationsStore,
  authStore: stores.authStore
}))
@observer
export default class PurchasesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inRefresh: false,
      loading: true
    };
  }

  componentDidMount() {
    if (this.props.authStore.user.isAdmin) {
      this.props.reservationsStore
        .getSellShipAction()
        .then(() => {
          this.setState({ loading: false });
        })
        .catch(() => {
          this.setState({ loading: false });
        });
    } else {
      this.props.reservationsStore
        .getMySellAction()
        .then(() => {
          this.setState({ loading: false });
        })
        .catch(() => {
          this.setState({ loading: false });
        });
    }
  }

  openReservation(id) {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.reservation.editResevBoat,
          options: { topBar: { visible: false } },
          passProps: {
            id
          }
        }
      },
      "push"
    );
  }

  keyExtractor = () => Math.random().toString();
  async resfresh() {
    if (this.props.authStore.user.isAdmin) {
      await this.setState({ inRefresh: true });
      await this.props.reservationsStore.getSellShipAction();
      await this.setState({ inRefresh: false });
    } else {
      await this.setState({ inRefresh: true });
      await this.props.reservationsStore.getMySellAction();
      await this.setState({ inRefresh: false });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("menu.purchasesList")}
        />
        {this.state.loading ? (
          <View style={styles.loaderContainer}>
            <ActivityIndicator size="large" color={Vars.mainColor} />
          </View>
        ) : (
          <FlatList
            data={this.props.reservationsStore.mySell}
            keyExtractor={this.keyExtractor}
            onRefresh={this.resfresh.bind(this)}
            refreshing={this.state.inRefresh}
            ListEmptyComponent={
              <LostConnection
                screen={"EMPTY_STATE"}
                onClick={this.resfresh.bind(this)}
              />
            }
            renderItem={({ item }) => (
              <TouchableOpacity
                key={item.id}
                onPress={this.openReservation.bind(this, item.id)}
              >
                <View style={styles.card}>
                  {this.props.rootStore.position === "right" ? (
                    <View style={styles.cardDetails}>
                      <View style={styles.timeContainer}>
                        <View style={styles.timeContent}>
                          <Text style={styles.time}>{item.status}</Text>
                        </View>
                      </View>
                      <View style={styles.cradMessage}>
                        <Text style={styles.title}>{item.ship.name}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={[styles.subTitle, styles.coloredText]}>
                            {item.ship.end}
                          </Text>
                          <FastImage source={Arrow} style={styles.arrow} />
                          <Text style={styles.subTitle}>
                            {I18n.t("reservationList.from")}
                            <Text style={styles.coloredText}>
                              {item.ship.start}
                            </Text>
                          </Text>
                          <Text style={styles.subTitle}>{item.day}</Text>
                        </View>
                        <Text style={styles.subTitle}>
                          {I18n.t("reservationList.numberOfPerson")}{" "}
                          <Text style={styles.coloredText}>
                            {item.crewCount}
                          </Text>
                        </Text>
                      </View>
                      <View>
                        <FastImage source={Logo} style={styles.imageProfile} />
                      </View>
                    </View>
                  ) : (
                    <View style={styles.cardDetails}>
                      <View>
                        <FastImage source={Logo} style={styles.imageProfile} />
                      </View>
                      <View style={styles.leftCradMessage}>
                        <Text style={styles.leftTitle}>{item.ship.name}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={styles.leftSubTitle}>{item.day}</Text>
                          <Text style={styles.leftSubTitle}>
                            {I18n.t("reservationList.from")}
                            <Text style={styles.coloredText}>
                              {item.ship.start}
                            </Text>
                          </Text>
                          <FastImage source={Arrow} style={styles.leftArrow} />
                          <Text
                            style={[styles.leftSubTitle, styles.coloredText]}
                          >
                            {item.ship.end}
                          </Text>
                        </View>
                        <Text style={styles.leftSubTitle}>
                          {I18n.t("reservationList.numberOfPerson")}{" "}
                          <Text style={styles.coloredText}>
                            {item.crewCount}
                          </Text>
                        </Text>
                      </View>
                      <View style={styles.timeContainer}>
                        <View style={styles.timeContent}>
                          <Text style={styles.time}>{item.status}</Text>
                        </View>
                      </View>
                    </View>
                  )}
                  <FastImage source={Line} style={styles.hr} />
                </View>
              </TouchableOpacity>
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 100
  },
  card: {
    width: Vars.width,
    flexDirection: "column"
  },
  cardDetails: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  cradMessage: {
    flexDirection: "column",
    alignItems: "flex-end",
    paddingRight: 10,
    flex: 4,
  },
  leftCradMessage: {
    flexDirection: "column",
    alignItems: "flex-start",
    flex: 4
  },
  cradSubMessage: {
    flexDirection: "row",
    alignItems: "center"
  },
  imageProfile: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  arrow: {
    width: 20,
    height: 10,
    marginLeft: 10
  },
  leftArrow: {
    width: 20,
    height: 10,
    marginLeft: 10,
    transform: [{ rotate: "180deg" }]
  },
  hr: {
    width: Vars.width,
    height: 5
  },
  time: {
    color: "white",
    fontSize: 10
  },
  timeContent: {
    paddingHorizontal: 5,
    borderRadius: 10,
    backgroundColor: Vars.mainColor
  },
  timeContainer: {
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 1
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  title: {
    fontSize: 13,
    marginBottom: 5
  },
  leftTitle: {
    fontSize: 13,
    marginBottom: 5,
    marginLeft: 15
  },
  subTitle: {
    color: "#909090",
    fontSize: 10,
    marginLeft: 15
  },
  leftSubTitle: {
    color: "#909090",
    fontSize: 10,
    marginLeft: 15
  },
  coloredText: {
    color: Vars.mainColor
  },
  loaderContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});
