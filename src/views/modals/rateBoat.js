import {
    View,
    Text,
    ImageBackground,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator
} from 'react-native';
import React, {Component} from 'react';
import {Navigation} from 'react-native-navigation';
import Image from './../../images/settings/authorization.png';
import Vars from '../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from '../common/header';
import Rating from 'react-native-easy-rating';
import Star from './../../images/modals/star.png';
import EmtytStar from './../../images/modals/emptyStar.png';
import UserStars from './../../images/modals/userStars.png';
import {inject, observer} from 'mobx-react/native';
import I18n from '../../languages';

@inject(stores => ({
    rootStore: stores.rootStore,
    boatStore: stores.boatStore
}))
@observer
export default class RateBoat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating_1: 0,
            rating_2: 3,
            rating_3: 5
        };
    }

    rate() {
        if(this.props.rootStore.isLoading) return;
        this.props.boatStore.sendRating({ship_id: 3, rate: this.state.rating_1, rate2: this.state.rating_2, rate3: this.state.rating_3});
        this.props.rootStore.navigate(this.props.componentId, {}, 'dismissOverlay');
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.imageContainer}>
                        <FastImage source={UserStars} style={styles.image}/>
                    </View>
                    <View>
                        <View style={styles.ratingSectionContainer}>
                            <Text style={styles.title}>{I18n.t('rateBoat.description_1')}</Text>
                            <Rating
                                rating={1}
                                max={5}
                                iconWidth={18}
                                iconHeight={18}
                                iconSelected={Star}
                                iconUnselected={EmtytStar}
                                onRate={rating => this.setState({rating_1: rating})}
                            />
                        </View>
                        <View style={styles.ratingSectionContainer}>
                            <Text style={styles.title}>{I18n.t('rateBoat.description_2')}</Text>
                            <Rating
                                rating={1}
                                max={5}
                                iconWidth={18}
                                iconHeight={18}
                                iconSelected={Star}
                                iconUnselected={EmtytStar}
                                onRate={rating => this.setState({rating_2: rating})}
                            />
                        </View>
                        <View style={styles.ratingSectionContainer}>
                            <Text style={styles.title}>{I18n.t('rateBoat.description_3')}</Text>
                            <Rating
                                rating={1}
                                max={5}
                                iconWidth={18}
                                iconHeight={18}
                                iconSelected={Star}
                                iconUnselected={EmtytStar}
                                onRate={rating => this.setState({rating_3: rating})}
                            />
                        </View>

                    </View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.rate.bind(this)}
                    >
                        {
                            this.props.rootStore.isLoading ?  
                            <ActivityIndicator color={'white'} /> :
                            <Text style={styles.buttonText}>{I18n.t('rateBoat.validate')}</Text>
                        }
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    content: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: Vars.width - 20,
        alignItems: 'center',
        paddingVertical: 20
    },
    imageContainer: {
        backgroundColor: 'white',
        height: 80,
        width: 80,
        borderRadius: 40,
        position: 'absolute',
        shadowOffset: {width: 0, height: 0},
        shadowColor: '#ccc',
        shadowOpacity: 1,
        top: -43,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 40,
        height: 40
    },
    button: {
        backgroundColor: Vars.mainColor,
        borderRadius: 20,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
        width: 140,
        marginTop: 20
    },
    title: {
        marginTop: 20,
        marginBottom: 10,
        textAlign: 'center'
    },
    subTitle: {
        color: '#ccc',
        fontSize: 11,
        marginBottom: 50,
        paddingHorizontal: 30,
        textAlign: 'center'
    },
    buttonText: {
        color: 'white'
    },
    ratingSectionContainer:{
        alignItems: 'center',
        justifyContent: 'center'
    }
});
