import {
  View,
  Text,
  ImageBackground,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import Image from './../../images/settings/authorization.png';
import Vars from '../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from '../common/header';
import Rating from 'react-native-easy-rating';
import Star from './../../images/modals/star.png';
import EmtytStar from './../../images/modals/emptyStar.png';
import UserStars from './../../images/modals/userStars.png';
import I18n from '../../languages';

export default class ReservationError extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0
    };
  }
  rate() {
    Navigation.dismissOverlay(this.props.componentId);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <FastImage source={UserStars} style={styles.image} />
          </View>
          <Text style={styles.title}>
            {I18n.t('reservationError.description')}
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={this.rate.bind(this)}
          >
            <Text style={styles.buttonText}>
              {I18n.t('reservationError.goToSettings')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)'
  },
  content: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: Vars.width - 20,
    alignItems: 'center',
    paddingVertical: 20
  },
  imageContainer: {
    backgroundColor: 'white',
    height: 80,
    width: 80,
    borderRadius: 40,
    position: 'absolute',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: '#ccc',
    shadowOpacity: 1,
    top: -43,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: 40,
    height: 40
  },
  button: {
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    width: 200,
    marginTop: 20
  },
  title: {
    marginTop: 20,
    marginBottom: 10
  },
  subTitle: {
    color: '#ccc',
    fontSize: 11,
    marginBottom: 50,
    paddingHorizontal: 30,
    textAlign: 'center'
  },
  buttonText: {
    color: 'white'
  }
});
