import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator
} from "react-native";
import React, { Component } from "react";
import Profile from "./../../images/notifications/profile_pic.png";
import Line from "./../../images/notifications/line.png";
import Arrow from "./../../images/notifications/arrow.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "./../common/header";
import { inject, observer } from "mobx-react/native";
import I18n from "./../../languages/";
import Logo from "./../../images/sideBar/logobackground.png";
import LostConnection from "./../../views/settings/lostConnection";
@inject(stores => ({
  rootStore: stores.rootStore,
  notificationsStore: stores.notificationsStore
}))
@observer
export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inRefresh: false,
      loading: true
    };
  }
  componentDidMount() {
    this.props.notificationsStore
      .getNotificationAction()
      .then(() => {
        this.setState({ loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  }
  keyExtractor = () => Math.random().toString();
  async resfresh() {
    await this.setState({ inRefresh: true });
    await this.props.notificationsStore.getNotificationAction();
    await this.setState({ inRefresh: false });
  }
  render() {
    return (
      <View style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("notifications.title")}
          modal
        />
        {this.state.loading ? (
          <View style={styles.loaderContainer}>
            <ActivityIndicator size="large" color={Vars.mainColor} />
          </View>
        ) : (
          <FlatList
            data={this.props.notificationsStore.notifications}
            keyExtractor={this.keyExtractor}
            onRefresh={this.resfresh.bind(this)}
            refreshing={this.state.inRefresh}
            ListEmptyComponent={
              <LostConnection
                screen={"EMPTY_STATE"}
                onClick={this.resfresh.bind(this)}
              />
            }
            renderItem={({ item }) => (
              <View>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.card}>
                    <View style={styles.cardDetails}>
                      <View style={styles.timeContainer}>
                        <Text style={styles.time}>{item.start}</Text>
                      </View>
                      <View style={styles.cradMessage}>
                        <Text style={styles.title}> {item.notiyBody}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={styles.subTitle}>
                            {I18n.t("notifications.from")} {item.fromCity}
                          </Text>
                          <FastImage source={Arrow} style={styles.arrow} />
                          <Text style={styles.subTitle}>
                            {I18n.t("notifications.to")} {item.toCity}
                          </Text>
                        </View>
                      </View>
                      <View>
                        <FastImage source={Logo} style={styles.imageProfile} />
                      </View>
                    </View>
                    <FastImage source={Line} style={styles.hr} />
                  </View>
                ) : (
                  <View style={styles.card}>
                    <View style={styles.cardDetails}>
                      <View>
                        <FastImage source={Logo} style={styles.imageProfile} />
                      </View>
                      <View style={styles.leftCradMessage}>
                        <Text style={styles.title}>{item.notiyBody}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={styles.subTitle}>
                            {I18n.t("notifications.from")} {item.fromCity}
                          </Text>
                          <FastImage source={Arrow} style={styles.leftArrow} />
                          <Text style={styles.subTitle}>
                            {I18n.t("notifications.to")} {item.toCity}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.timeContainer}>
                        <Text style={styles.time}>{item.start}</Text>
                      </View>
                    </View>
                    <FastImage source={Line} style={styles.hr} />
                  </View>
                )}
              </View>
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 100
  },
  card: {
    width: Vars.width,
    flexDirection: "column"
  },
  cardDetails: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  cradMessage: {
    flexDirection: "column",
    alignItems: "flex-end",
    paddingRight: 10,
    flex: 4
  },
  leftCradMessage: {
    flexDirection: "column",
    alignItems: "flex-start",
    paddingLeft: 10,
    flex: 4
  },
  cradSubMessage: {
    flexDirection: "row",
    alignItems: "center"
  },
  imageProfile: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  arrow: {
    width: 20,
    height: 10,
    marginHorizontal: 10
  },
  leftArrow: {
    width: 20,
    height: 10,
    marginLeft: 10,
    marginRight: 10,
    transform: [{ rotate: "180deg" }]
  },
  hr: {
    width: Vars.width,
    height: 5
  },
  time: {
    color: Vars.mainColor,
    fontSize: 10
  },
  timeContainer: {
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 1
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  title: {
    fontSize: 13,
    marginBottom: 5
  },
  subTitle: {
    color: "#909090",
    fontSize: 10
  },
  loaderContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});
