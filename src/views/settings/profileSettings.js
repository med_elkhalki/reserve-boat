import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  AlertIOS,
  ActivityIndicator
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/background.png";
import Line from "./../../images/reservation/line2.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import { inject, observer } from "mobx-react/native";
import I18n from "./../../languages/";
import InputValidation from "../common/inputValidation";

@inject(stores => ({
  rootStore: stores.rootStore,
  authStore: stores.authStore,
  settingsStore: stores.settingsStore
}))
@observer
export default class ProfileSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.authStore.user.name,
      nameError: false,
      identity: this.props.authStore.user.phone,
      identityError: false,
      phone: this.props.authStore.user.phone,
      phoneError: false,
      email: this.props.authStore.user.email,
      emailError: false,
      password: this.props.authStore.user.password,
      passwordError: false,
      confirmPassword: this.props.authStore.user.password_confirmation,
      confirmPasswordError: false,
      data: {}
    };
  }

  validator() {
    let data = {};
    if(!this.state.nameError || !this.state.phoneError || !this.state.passwordError ) {
      return false;
    } else {
      if(this.state.name !== this.props.authStore.user.name) {
        data.name = this.state.name;
      } 
      if(this.state.phone !== this.props.authStore.user.phone) {
        data.phone = this.state.phone;
      }
      if(this.state.identity !== this.props.authStore.user.identity) {
        data.identity = this.state.identity;
      }
      if(this.state.email !== this.props.authStore.user.email) {
        data.email = this.state.email;
      }
      if(this.state.password !== this.props.authStore.user.password) {
        data.password = this.state.password;
      }
      if(this.state.confirmPassword !== this.props.authStore.user.password_confirmation) {
        data.password_confirmation = this.state.confirmPassword;
      }
      data.group_id = this.props.authStore.user.group_id;

      this.setState({data})
      return true;

    }
  }

  async send() {
    if (this.props.rootStore.isLoading) return;
    let isValid = await this.validator();
    if(!isValid) return;
    this.props.settingsStore
      .editUserProfile(//{
        // name: this.state.name,
        // phone: this.state.phone,
        // email: this.state.email,
        // password: this.state.password,
        // password_confirmation: this.state.confirmPassword,
        // group_id: this.props.authStore.user.group_id
      //}
      this.state.data
      )
      .then(data => {
        console.log(data);
        AlertIOS.alert(
          I18n.t("success.contactUs.messageTitle"),
          I18n.t("success.contactUs.message"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
      })
      .catch(error => {
        console.log(error);
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("name"))
            messageError += error.dataErrors.get("name") + "\n";
          if (error && error.dataErrors.has("email"))
            messageError += error.dataErrors.get("email") + "\n";
          if (error && error.dataErrors.has("phone"))
            messageError += error.dataErrors.get("phone") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
      });
  }

  render() {
    return (
      <FastImage source={Background} style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("profileSettings.title")}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="username"
                  isArabic={true}
                  placeholder={I18n.t("profileSettings.namePlaceholder")}
                  value={this.state.name}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={name => this.setState({ name })}
                  style={styles.input}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.name")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.name")}
                </Text>
                <InputValidation
                  validator="username"
                  isArabic={false}
                  placeholder={I18n.t("profileSettings.namePlaceholder")}
                  value={this.state.name}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  onChangeText={name => this.setState({ name })}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="phone"
                  isArabic={true}
                  placeholder={I18n.t("profileSettings.identiyPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.identity}
                  onChangeText={identity => this.setState({ identity })}
                  onValidatorExecuted={identityError =>
                    this.setState({ identityError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.identiy")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.identiy")}
                </Text>
                <InputValidation
                  validator="phone"
                  isArabic={false}
                  placeholder={I18n.t("profileSettings.identiyPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.identity}
                  onChangeText={identity => this.setState({ identity })}
                  onValidatorExecuted={identityError =>
                    this.setState({ identityError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="phone"
                  isArabic={true}
                  placeholder={I18n.t("profileSettings.phonePlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.phone")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.phone")}
                </Text>
                <InputValidation
                  validator="phone"
                  isArabic={false}
                  placeholder={I18n.t("profileSettings.phonePlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="email"
                  isArabic={true}
                  placeholder={I18n.t("contactUs.emailPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                  onValidatorExecuted={emailError =>
                    this.setState({ emailError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.email")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("profileSettings.email")}
                </Text>
                <InputValidation
                  validator="email"
                  isArabic={false}
                  placeholder={I18n.t("contactUs.emailPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                  onValidatorExecuted={emailError =>
                    this.setState({ emailError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="password"
                  placeholder={"**********"}
                  isArabic={true}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.inputPassword}
                  value={this.state.password}
                  onChangeText={password => this.setState({ password })}
                  onValidatorExecuted={passwordError =>
                    this.setState({ passwordError })
                  }
                  validatorExecutionDelay={100}
                  secureTextEntry={true}
                />
                <Text style={styles.inputPasswordTitle}>
                  {I18n.t("profileSettings.password")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.leftInputPasswordTitle}>
                  {I18n.t("profileSettings.password")}
                </Text>
                <InputValidation
                  validator="password"
                  placeholder={"**********"}
                  isArabic={false}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.inputPassword}
                  value={this.state.password}
                  onChangeText={password => this.setState({ password })}
                  onValidatorExecuted={passwordError =>
                    this.setState({ passwordError })
                  }
                  validatorExecutionDelay={100}
                  secureTextEntry={true}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="password"
                  placeholder={"**********"}
                  isArabic={true}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.inputPassword}
                  value={this.state.confirmPassword}
                  onChangeText={confirmPassword =>
                    this.setState({ confirmPassword })
                  }
                  onValidatorExecuted={confirmPasswordError =>
                    this.setState({ confirmPasswordError })
                  }
                  validatorExecutionDelay={100}
                  secureTextEntry={true}
                />
                <Text style={styles.inputPasswordTitle}>
                  {I18n.t("profileSettings.confirmPassword")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.leftInputPasswordTitle}>
                  {I18n.t("profileSettings.confirmPassword")}
                </Text>
                <InputValidation
                  validator="password"
                  placeholder={"**********"}
                  isArabic={false}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.inputPassword}
                  value={this.state.confirmPassword}
                  onChangeText={confirmPassword =>
                    this.setState({ confirmPassword })
                  }
                  onValidatorExecuted={confirmPasswordError =>
                    this.setState({ confirmPasswordError })
                  }
                  validatorExecutionDelay={100}
                  secureTextEntry={true}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>
          {/*    {<View style={styles.inputContainer}>
                    {
                        this.props.rootStore.position === 'right' ?
                            <View style={styles.inputContent}>
                                <TextInput secureTextEntry={true} placeholder={'**********'}
                                           style={styles.inputPassword}/>
                                <Text style={styles.inputPasswordTitle}>
                                    {I18n.t('profileSettings.confirmPassword')}
                                </Text>
                            </View>
                            :
                            <View style={styles.inputContent}>
                                <Text style={styles.leftInputPasswordTitle}>
                                    {I18n.t('profileSettings.confirmPassword')}
                                </Text>
                                <TextInput secureTextEntry={true} placeholder={'**********'}
                                           style={styles.inputPassword}/>
                            </View>
                    }
                    <FastImage source={Line} style={styles.hr}/>
                </View>}*/}
        </ScrollView>

        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.button}
            onPress={this.send.bind(this)}
          >
            {this.props.rootStore.isLoading ? (
              <ActivityIndicator color={"white"} />
            ) : (
              <Text style={styles.buttonText}>
                {I18n.t("profileSettings.save")}
              </Text>
            )}
          </TouchableHighlight>
        </View>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  hr: {
    width: Vars.width - 50,
    height: 7
  },
  inputContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  inputContent: {
    flexDirection: "row",
    //justifyContent: 'space-between',
    width: Vars.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25
  },
  input: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  inputTitle: {},
  button: {
    height: 40,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 60
  },
  buttonContainer: {
    height: 90,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 40
  },
  buttonText: {
    color: "white"
  },
  scrollView: {
    marginTop: 60
  },
  inputPassword: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  inputPasswordTitle: {
    flex: 1,
    textAlign: "right"
  },
  leftInputPasswordTitle: {
    flex: 1,
    textAlign: "left"
  },
  containerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center"
  },
  errorInputContainerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "red",
    borderBottomWidth: 1
  }
});
