import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  Picker,
  AlertIOS,
  ActivityIndicator
} from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/reservation/banner2.png";
import Background from "./../../images/background.png";
import Line from "./../../images/reservation/line2.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import I18n from "./../../languages/";
import InputValidation from "./../common/inputValidation";
import { inject, observer } from "mobx-react/native";

@inject(stores => ({
  rootStore: stores.rootStore,
  settingsStore: stores.settingsStore
}))
@observer
export default class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      nameError: false,
      email: "",
      emailError: false,
      phone: "",
      phoneError: false,
      msg: "",
      msgError: false
    };
  }
  send() {
    if (this.props.rootStore.isLoading) return;
    let TESTIFINPUTISEMPTY =
      !this.state.name.length ||
      !this.state.email.length ||
      !this.state.phone.length ||
      !this.state.msg.length;
    let TESTIFERROR =
      this.state.nameError && this.state.emailError && this.state.phoneError; //this.state.msgError;
    if (TESTIFINPUTISEMPTY) {
      AlertIOS.alert(
        I18n.t("errors.contactUs.requiredTitle"),
        I18n.t("errors.contactUs.requiredMessage"),
        [
          {
            text: I18n.t("errors.cancel"),
            onPress: () => {},
            style: "cancel"
          }
        ]
      );
      return;
    }
    if (!TESTIFERROR) {
      return;
    }
    this.props.settingsStore
      .contactUsAction({
        name: this.state.name,
        email: this.state.email,
        phone: this.state.phone,
        msg: this.state.msg
      })
      .then(data => {
        this.setState({
          name: "",
          email: "",
          phone: "",
          msg: ""
        });
        AlertIOS.alert(
          I18n.t("success.contactUs.messageTitle"),
          I18n.t("success.contactUs.message"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
      })
      .catch(error => {
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("name"))
            messageError += error.dataErrors.get("name") + "\n";
          if (error && error.dataErrors.has("email"))
            messageError += error.dataErrors.get("email") + "\n";
          if (error && error.dataErrors.has("phone"))
            messageError += error.dataErrors.get("phone") + "\n";
          if (error && error.dataErrors.has("msg"))
            messageError += error.dataErrors.get("msg");
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () =>
                this.setState({
                  name: "",
                  email: "",
                  phone: "",
                  msg: ""
                }),
              style: "cancel"
            }
          ]);
        }
      });
  }

  render() {
    return (
      <FastImage source={Background} style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("contactUs.title")}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="username"
                  isArabic={true}
                  placeholder={I18n.t("contactUs.namePlaceholder")}
                  value={this.state.name}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  onChangeText={name => this.setState({ name })}
                  style={styles.input}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.name")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.name")}
                </Text>
                <InputValidation
                  validator="username"
                  isArabic={false}
                  placeholder={I18n.t("contactUs.namePlaceholder")}
                  value={this.state.name}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  onChangeText={name => this.setState({ name })}
                  onValidatorExecuted={nameError =>
                    this.setState({ nameError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="email"
                  isArabic={true}
                  placeholder={I18n.t("contactUs.emailPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                  onValidatorExecuted={emailError =>
                    this.setState({ emailError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.email")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.email")}
                </Text>
                <InputValidation
                  validator="email"
                  isArabic={false}
                  placeholder={I18n.t("contactUs.emailPlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                  onValidatorExecuted={emailError =>
                    this.setState({ emailError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          <View style={styles.inputContainer}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.inputContent}>
                <InputValidation
                  validator="phone"
                  isArabic={true}
                  placeholder={I18n.t("contactUs.phonePlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.phone")}
                </Text>
              </View>
            ) : (
              <View style={styles.inputContent}>
                <Text style={styles.inputTitle}>
                  {I18n.t("contactUs.phone")}
                </Text>
                <InputValidation
                  validator="phone"
                  isArabic={false}
                  placeholder={I18n.t("contactUs.phonePlaceholder")}
                  errorInputContainerStyle={styles.errorInputContainerStyle}
                  containerStyle={styles.containerStyle}
                  style={styles.input}
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                  onValidatorExecuted={phoneError =>
                    this.setState({ phoneError })
                  }
                  validatorExecutionDelay={100}
                />
              </View>
            )}
            <FastImage source={Line} style={styles.hr} />
          </View>

          {this.props.rootStore.position === "right" ? (
            <View style={styles.textAreaContainer}>
              <Text style={styles.textAreaTitle}>
                {I18n.t("contactUs.message")}
              </Text>
              <View style={styles.textAreaInputWrapper}>
                <TextInput
                  placeholder={I18n.t("contactUs.messagePlaceholder")}
                  style={styles.textArea}
                  value={this.state.msg}
                  onChangeText={msg => this.setState({ msg })}
                  numberOfLines={4}
                  multiline={true}
                />
              </View>
            </View>
          ) : (
            <View style={styles.leftTextAreaContainer}>
              <Text style={styles.leftTextAreaTitle}>
                {I18n.t("contactUs.message")}
              </Text>
              <View style={styles.leftTextAreaInputWrapper}>
                <TextInput
                  placeholder={I18n.t("contactUs.messagePlaceholder")}
                  style={styles.LeftTextArea}
                  onChangeText={msg => this.setState({ msg })}
                  numberOfLines={4}
                  multiline={true}
                />
              </View>
            </View>
          )}
        </ScrollView>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.button}
            onPress={this.send.bind(this)}
          >
            {this.props.rootStore.isLoading ? (
              <ActivityIndicator color={"white"} />
            ) : (
              <Text style={styles.buttonText}>{I18n.t("contactUs.send")}</Text>
            )}
          </TouchableHighlight>
        </View>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  hr: {
    width: Vars.width - 50,
    height: 7
  },
  selectBoxWrapper: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  selectBoxContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
    marginBottom: 5,
    flexDirection: "row",
    paddingHorizontal: 25
  },
  selectbox: {},
  selectboxText: {
    textAlign: "center"
  },
  inputContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  inputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: Vars.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25
  },
  input: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  containerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center"
  },
  errorInputContainerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  inputTitle: {},
  button: {
    height: 40,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 60
  },
  buttonContainer: {
    height: 90,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 40
  },
  buttonText: {
    color: "white"
  },
  textAreaContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    paddingHorizontal: 25,
    marginTop: 20
  },
  textAreaInputWrapper: {
    borderColor: "#F2F2F2",
    borderWidth: 0.5
  },
  textArea: {
    flex: 2,
    height: 55,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "right",
    paddingHorizontal: 5
  },
  textAreaTitle: {
    textAlign: "right",
    marginBottom: 5
  },
  scrollView: {
    marginTop: 60
  },
  leftTextAreaContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    paddingHorizontal: 25,
    marginTop: 20
  },
  leftTextAreaInputWrapper: {
    borderColor: "#F2F2F2",
    borderWidth: 0.5
  },
  LeftTextArea: {
    flex: 2,
    height: 55,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left",
    paddingHorizontal: 5
  },
  leftTextAreaTitle: {
    textAlign: "left",
    marginBottom: 5
  }
});
