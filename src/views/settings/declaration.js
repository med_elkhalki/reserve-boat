import {View, Text, ImageBackground, TextInput, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import React, {Component} from 'react';
import Image from './../../images/settings/authorization.png';
import Vars from '../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from '../common/header';
import I18n from './../../languages/';


export default class Declaration extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Header componentId={this.props.componentId} title={I18n.t('declaration.title')}/>
                <View style={styles.imageContainer}>
                    <FastImage  source={Image} style={styles.image}/>
                </View>
                <Text style={styles.title}>{I18n.t('declaration.messageTitle')}</Text>
                <Text style={styles.subTitle}>{I18n.t('declaration.message')}</Text>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText}>{I18n.t('declaration.agree')}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 150,
        alignItems: 'center',
    },
    imageContainer: {
        paddingHorizontal: 30
    },
    image: {
        width: Vars.width * .75,
        height: Vars.width * .4
    },
    button: {
        backgroundColor: Vars.mainColor,
        borderRadius: 20,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30
    },
    title: {
        marginBottom: 20,
        marginTop: 50,
    },
    subTitle: {
        color: '#ccc',
        fontSize: 11,
        marginBottom: 50,
        paddingHorizontal: 30,
        textAlign: 'center'
    },
    buttonText: {
        color: 'white',
    }
})