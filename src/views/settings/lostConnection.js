import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableHighlight
} from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/description/banner.png";
import Background from "./../../images/background.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "./../common/header";
import emptyWhiteBox from "./../../images/settings/empty-white-box.png";
import noInternetConnection from "./../../images/settings/no-internet-connection.png";
import error from "./../../images/settings/error.png";
import FolderEmpty from "./../../images/settings/folder-icon-md.png";
import I18n from "./../../languages/";

export default class LostConnection extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const screen = this.props.screen;

    switch (screen) {
      case "LOST-CONNECTION":
        return (
          <FastImage source={Background} style={styles.container}>
            <ScrollView>
              <View style={styles.cardContainer}>
                <FastImage
                  source={noInternetConnection}
                  style={styles.roadImage}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.textContainer}>
                  <Text style={styles.title}>
                    {"Oops! your connection seems off..."}
                  </Text>
                  <Text style={styles.description}>
                    {"Check your connection or try again"}
                  </Text>
                  <View style={styles.buttonContainer}>
                    <TouchableHighlight style={styles.button}>
                      <Text style={styles.buttonText}>{"TRY AGAIN"}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </ScrollView>
          </FastImage>
        );
        break;

      case "ERROR-500":
        return (
          <FastImage source={Background} style={styles.container}>
            <ScrollView>
              <View style={styles.cardContainer}>
                <FastImage
                  source={error}
                  style={styles.roadImage}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.textContainer}>
                  <Text style={styles.title}>{"Uh... Error 500 !"}</Text>
                  <Text style={styles.description}>
                    {"Something went wrong at our end."}
                  </Text>
                  <Text style={styles.description}>
                    {"Don't worry it's not you - it's us."}
                  </Text>
                  <Text style={styles.description}>{"Sorry about that."}</Text>
                  <View style={styles.buttonContainer}>
                    <TouchableHighlight style={styles.button}>
                      <Text style={styles.buttonText}>{"BACK"}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </ScrollView>
          </FastImage>
        );
        break;

      case "EMPTY_STATE":
        return (
          <FastImage source={Background} style={styles.container}>
            <ScrollView>
              <View style={styles.cardContainer}>
                <FastImage
                  source={FolderEmpty}
                  style={styles.roadImage}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.textContainer}>
                  <Text style={styles.title}>{I18n.t('emptyState.empty')}</Text>
                  {this.props.onClick ? (
                    <View style={styles.buttonContainer}>
                      <TouchableHighlight
                        style={styles.button}
                        onPress={this.props.onClick.bind(this)}
                      >
                        <Text style={styles.buttonText}>{I18n.t('emptyState.refresh')}</Text>
                      </TouchableHighlight>
                    </View>
                  ) : null}
                </View>
              </View>
            </ScrollView>
          </FastImage>
        );
        break;

      default:
        return (
          <FastImage source={Background} style={styles.container}>
            <ScrollView>
              <View style={styles.cardContainer}>
                <FastImage
                  source={emptyWhiteBox}
                  style={styles.roadImage}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.textContainer}>
                  <Text style={styles.title}>{I18n.t('emptyState.empty')}</Text>
                  <Text style={styles.description}>
                    {"Click on button to add items."}
                  </Text>
                  <View style={styles.buttonContainer}>
                    <TouchableHighlight style={styles.button}>
                      <Text style={styles.buttonText}>{I18n.t('emptyState.refresh')}</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </ScrollView>
          </FastImage>
        );
        break;
    }
  }
}

LostConnection.defaultProps = {
  screen: "LOST-CONNECTION"
  //screen: 'ERROR-500'
  //screen: 'EMPTY-STATE'
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 80,
    justifyContent: "center",
    alignItems: "center"
  },
  roadImage: {
    width: 128,
    height: 128,
    marginBottom: 30
    //paddingVertical: 30,
    //paddingHorizontal: 20
  },
  cardContainer: {
    flexDirection: "column",
    width: Vars.width,
    height: Vars.height - 50,
    justifyContent: "center",
    alignItems: "center"
  },
  textContainer: {
    width: Vars.width
  },
  title: {
    fontSize: 18,
    textAlign: "center",
    color: "#3986f0"
  },
  description: {
    fontSize: 12,
    textAlign: "center",
    paddingHorizontal: 70
  },
  button: {
    height: 40,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 60
  },
  buttonContainer: {
    height: 90,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 40
  },
  buttonText: {
    color: "white"
  }
});
