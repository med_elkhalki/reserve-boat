import { View, Text, StyleSheet, ScrollView } from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/description/banner.png";
import Background from "./../../images/background.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "./../common/header";
import CardHeaderTitleEllipse from "./../../images/appInfo/ellipse.png";
import CardBackground from "./../../images/appInfo/hook.png";
import { inject, observer } from "mobx-react/native";
import I18n from "../../languages";


@inject(stores => ({
  rootStore: stores.rootStore,
  authStore: stores.authStore
}))
@observer
export default class AppInfo extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }
  render() {
    return (
        <FastImage source={Background} style={styles.container}>
          <Header componentId={this.props.componentId} />
          <ScrollView>
            <FastImage source={HeaderImage} style={styles.headerImage} />
            <View style={styles.cardContainer}>
              <View style={styles.cardHeader}>
                <View style={styles.cardHeaderTitleWrapper}>
                  <Text style={styles.cardHeaderTitle}>
                    {I18n.t("policy.aboutUs.title")}
                  </Text>
                  <View style={styles.cardHeaderTitleEllipseWrapper}>
                    <FastImage
                        source={CardHeaderTitleEllipse}
                        style={styles.cardHeaderTitleEllipse}
                    />
                  </View>
                </View>
                <Text style={styles.cardHeaderText}>
                  {I18n.t("policy.aboutUs.body")}
                </Text>
              </View>
              <View style={styles.cardBody}>
                <Text style={styles.cardBodyTitle}>
                  {I18n.t("policy.humanIsAllforUs.title")}
                </Text>
                <Text style={styles.cardBodyText}>
                  {I18n.t("policy.humanIsAllforUs.body")}
                </Text>
              </View>
              <View style={styles.cardBody}>
                <Text style={styles.cardBodyTitle}>
                  {I18n.t("policy.ourVision.title")}
                </Text>
                <Text style={styles.cardBodyText}>
                  {I18n.t("policy.ourVision.body")}
                </Text>
              </View>
              <View style={styles.cardBody}>
                <Text style={styles.cardBodyTitle}>
                  {I18n.t("policy.goals.title")}
                </Text>
                <Text style={styles.cardBodyText}>
                  {I18n.t("policy.goals.body")}
                </Text>
              </View>
              <FastImage
                  source={CardBackground}
                  style={styles.cardImage}
                  resizeMode={FastImage.resizeMode.contain}
              >
                <View style={styles.cardBody}>
                  <Text style={styles.cardBodyTitle}>
                    {I18n.t("policy.conditions.title")}
                  </Text>
                  <Text style={styles.cardBodyText}>
                    {I18n.t("policy.conditions.body")}
                  </Text>
                </View>
              </FastImage>
            </View>
          </ScrollView>
        </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  headerImage: {
    width: Vars.width,
    height: 200,
    marginBottom: 10,
    paddingVertical: 30,
    paddingHorizontal: 20
  },
  cardContainer: {
    flexDirection: "column",
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
    //backgroundColor: 'yellow',
    backgroundColor: "#ffffff",
    shadowColor: "#696969",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderBottomRightRadius: 80,
    borderTopLeftRadius: 80
  },
  cardImage: {
    //height: 30
  },
  cardHeader: {
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    marginBottom: 25
  },
  cardHeaderTitleWrapper: {
    flexDirection: "row"
  },
  cardHeaderTitleEllipse: {
    width: 10,
    height: 10
  },
  cardHeaderTitleEllipseWrapper: {
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: 5
  },
  cardHeaderTitle: {
    textAlign: "right",
    fontSize: 20,
    //lineHeight: 25,
    color: "#3986f0"
    //fontFamily: 'Din Next Regular'
  },
  cardHeaderText: {
    textAlign: "right",
    fontSize: 15,
    //lineHeight: 25px;
    color: "#484848"
    //fontFamily: 'Din Next Regular'
  },
  cardBody: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 15
  },
  cardBodyTitle: {
    textAlign: "center",
    fontSize: 15,
    //lineHeight: 25,
    color: "#3986f0"
    //fontFamily: 'Din Next Regular'
  },
  cardBodyText: {
    textAlign: "center",
    fontSize: 10,
    //lineHeight: 25px;
    color: "#484848"
    //fontFamily: 'Din Next Regular'
  }
});
