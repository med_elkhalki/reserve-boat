import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  FlatList
} from "react-native";
import React, { Component } from "react";
import Arrow from "./../../images/notifications/arrow.png";
import Search from "./../../images/description/search.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import I18n from "../../languages";
import { inject, observer } from "mobx-react/native";
import Screens from "../../navigation/screensName";
import LostConnection from "./../../views/settings/lostConnection";
import Logo from "./../../images/sideBar/logobackground.png";

@inject(stores => ({
  rootStore: stores.rootStore,
  authStore: stores.authStore,
  boatStore: stores.boatStore
}))
@observer
export default class MyBoats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inRefresh: false,
      loading: true
    };
  }

  componentDidMount() {
    this.props.boatStore
      .getMyShipsAction()
      .then(() => {
        this.setState({ loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  }

  openDetails(id) {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.boat.boatDetails,
          options: {
            topBar: {
              visible: false
            }
          },
          passProps: {
            id
          }
        }
      },
      "push"
    );
  }

  addBoat() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.boat.addBoat,
          options: {
            topBar: {
              visible: false
            }
          }
        }
      },
      "push"
    );
  }

  keyExtractor = () => Math.random().toString();
  async resfresh() {
    await this.setState({ inRefresh: true });
    await this.props.boatStore.getMyShipsAction();
    await this.setState({ inRefresh: false });
  }

  searchInMyBoats(text) {
    if(text && text.length) {
      this.props.boatStore.searchInMyBoats(text)
    } else {
      this.props.boatStore.getMyShipsAction();
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("searchBoats.title")}
        />

        {this.props.rootStore.position === "right" ? (
          <View style={styles.searchContainer}>
            <TextInput
              placeholder={I18n.t("searchBoats.inputSearchPlaceholder")}
              onChangeText={this.searchInMyBoats.bind(this)}
              autoCapitalize={'none'}
              style={styles.searchInput}
            />
            <FastImage
              source={Search}
              style={styles.searchIcon}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
        ) : (
          <View style={styles.leftSearchContainer}>
            <FastImage
              source={Search}
              style={styles.leftSearchIcon}
              resizeMode={FastImage.resizeMode.contain}
            />
            <TextInput
              placeholder={I18n.t("searchBoats.inputSearchPlaceholder")}
              onChangeText={this.searchInMyBoats.bind(this)}
              autoCapitalize={'none'}
              style={styles.leftSearchInput}
            />
          </View>
        )}
        {this.state.loading ? (
          <View style={styles.loaderContainer}>
            <ActivityIndicator size="large" color={Vars.mainColor} />
          </View>
        ) : (
          <FlatList
            data={this.props.boatStore.myShips}
            keyExtractor={this.keyExtractor}
            onRefresh={this.resfresh.bind(this)}
            refreshing={this.state.inRefresh}
            ListEmptyComponent={
              <LostConnection
                screen={"EMPTY_STATE"}
                onClick={this.resfresh.bind(this)}
              />
            }
            renderItem={({ item }) => (
              <TouchableOpacity
                key={item.id}
                onPress={this.openDetails.bind(this, item.id)}
              >
                <View style={styles.card}>
                  {this.props.rootStore.position === "right" ? (
                    <View style={styles.cardDetails}>
                      <View style={styles.timeContainer}>
                        <View style={styles.timeContent}>
                          <Text style={styles.time}>
                            {I18n.t("searchBoats.more")}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.cradMessage}>
                        <Text style={styles.title}>{item.name}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={styles.subTitle}>
                            {I18n.t("searchBoats.to")} {item.toCity}
                          </Text>
                          <FastImage source={Arrow} style={styles.arrow} />
                          <Text style={styles.subTitle}>
                            {I18n.t("searchBoats.from")} {item.fromCity}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.boatImageWrapper}>
                        <FastImage
                          source={item.images.length ? {uri: item.images[0].img} : Logo}
                          style={styles.boatImage}
                          resizeMode={FastImage.resizeMode.cover}
                        />
                      </View>
                    </View>
                  ) : (
                    <View style={styles.cardDetails}>
                      <View style={styles.boatImageWrapper}>
                        <FastImage
                          source={item.images.length ? {uri: item.images[0].img} : Logo}
                          style={styles.boatImage}
                          resizeMode={FastImage.resizeMode.cover}
                        />
                      </View>

                      <View style={styles.leftCradMessage}>
                        <Text style={styles.title}>{item.name}</Text>
                        <View style={styles.cradSubMessage}>
                          <Text style={styles.subTitle}>
                            {I18n.t("searchBoats.from")} {item.fromCity}
                          </Text>
                          <FastImage source={Arrow} style={styles.leftArrow} />
                          <Text style={styles.subTitle}>
                            {I18n.t("searchBoats.to")} {item.toCity}
                          </Text>
                        </View>
                      </View>

                      <View style={styles.timeContainer}>
                        <View style={styles.timeContent}>
                          <Text style={styles.time}>
                            {I18n.t("searchBoats.more")}
                          </Text>
                        </View>
                      </View>
                    </View>
                  )}
                </View>
              </TouchableOpacity>
            )}
          />
        )}
        {this.props.authStore.user.isAdmin ? (
          <TouchableOpacity
            style={styles.fab}
            onPress={this.addBoat.bind(this)}
          >
            <Text style={styles.fabText}>+</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 100
  },
  card: {
    width: Vars.width,
    flexDirection: "column",
    marginBottom: 15,
    paddingHorizontal: 15
  },
  cardDetails: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  cradMessage: {
    flexDirection: "column",
    alignItems: "flex-end",
    paddingRight: 10,
    flex: 4
  },
  leftCradMessage: {
    flexDirection: "column",
    alignItems: "flex-start",
    paddingLeft: 10,
    flex: 4
  },
  cradSubMessage: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 5
  },
  boatImage: {
    width: 80,
    height: 60,
    borderRadius: 5,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    shadowColor: "#696969",
    backgroundColor: "#ffffff"
  },
  boatImageWrapper: {
    borderRadius: 5,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    shadowColor: "#696969",
    backgroundColor: "#ffffff"
  },
  arrow: {
    width: 20,
    height: 10,
    marginHorizontal: 10
  },
  leftArrow: {
    width: 20,
    height: 10,
    marginHorizontal: 10,
    transform: [{ rotate: "180deg" }]
  },
  time: {
    color: "white",
    fontSize: 10
  },
  timeContent: {
    paddingHorizontal: 5,
    borderRadius: 10,
    backgroundColor: Vars.mainColor
  },
  timeContainer: {
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1,
    height: 50
  },
  imageContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  title: {
    fontSize: 13,
    marginBottom: 5
  },
  subTitle: {
    color: "#909090",
    fontSize: 8
  },
  coloredText: {
    color: Vars.mainColor
  },
  searchContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#ffffff",
    height: 40,
    width: Vars - 30,
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 5,
    shadowColor: "#696969"
  },
  searchInput: {},
  searchIcon: {
    width: 20,
    height: 20,
    marginLeft: 20
  },
  leftSearchContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#ffffff",
    height: 40,
    width: Vars - 30,
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 5,
    shadowColor: "#696969"
  },
  leftSearchInput: {},
  leftSearchIcon: {
    width: 20,
    height: 20,
    marginRight: 20
  },
  fab: {
    width: 40,
    height: 40,
    backgroundColor: "#ff8400",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 10,
    left: 10
  },
  fabText: {
    color: "#fff",
    fontSize: 20
  },
  loaderContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});
