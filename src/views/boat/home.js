import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/splash.png";
import HeaderImage from "./../../images/description/banner.png";
import ButtonBackground from "./../../images/description/buttonBackground.png";
import Boat from "./../../images/description/boat.png";
import Boat1 from "./../../images/description/boat3.png";
import Boats from "./../../images/description/boats.png";
import divingIcon from "./../../images/home/diving.png";
import maintenanceIcon from "./../../images/home/maintenance.png";
import Vehicles from "./../../images/description/vehicles.png";
import Hr from "./../../images/description/hr.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import Screens from "./../../navigation/screensName";
import { inject, observer } from "mobx-react/native";
import I18n from "./../../languages/";
import Config from "./../../../config";

@inject(stores => ({
  rootStore: stores.rootStore,
  boatStore: stores.boatStore
}))
@observer
export default class Home extends Component {
  constructor(props) {
    super(props);
  }

  openDetails(name) {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.boat.searchBoats,
          options: { topBar: { visible: false } },
          passProps: {
            name
          }
        }
      },
      "push"
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("home.title")}
        />

        <ScrollView>
          <FastImage source={HeaderImage} style={styles.headerImage} />
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.trip)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage source={Boats} style={styles.icon} />
                <Text>{I18n.t("home.picnicBoats")}</Text>
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.dive)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage source={Vehicles} style={styles.icon} />
                <Text>{I18n.t("home.boats")}</Text>
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.transport)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage source={Boat1} style={styles.icon} />
                <Text>{I18n.t("home.transportBoats")}</Text>
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.fish)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage source={Boat} style={styles.icon} />
                <Text>{I18n.t("home.fishingBoats")}</Text>
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.coachdive)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage source={divingIcon} style={styles.icon} />
                {this.props.rootStore.position === "right" ? (
                  <Text style={styles.divingTextRight}>
                    {I18n.t("home.boatsDiving")}
                  </Text>
                ) : (
                  <Text style={styles.divingTextLeft}>
                    {I18n.t("home.boatsDiving")}
                  </Text>
                )}
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={this.openDetails.bind(this, Config.sell)}
            >
              <FastImage source={ButtonBackground} style={styles.buttonImage}>
                <FastImage
                  source={maintenanceIcon}
                  style={styles.maintenanceIcon}
                />
                {this.props.rootStore.position === "right" ? (
                  <Text style={styles.divingTextRight}>
                    {I18n.t("home.boatsMaintenance")}
                  </Text>
                ) : (
                  <Text style={styles.divingTextLeft}>
                    {I18n.t("home.boatsMaintenance")}
                  </Text>
                )}
                <FastImage source={Hr} style={styles.hr} />
              </FastImage>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  headerImage: {
    width: Vars.width,
    height: 200,
    marginBottom: 10
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 80
  },
  inputContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20
  },
  input: {
    backgroundColor: "#ffffff3b",
    height: 45,
    borderRadius: 25,
    textAlign: "center"
  },
  circle: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0
  },
  link: {
    color: "white",
    fontSize: 12,
    marginBottom: 20
  },
  button: {
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "#ccc",
    shadowOpacity: 0.5
  },
  buttonText: {
    color: Vars.mainColor
  },
  linkContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-end"
  },
  buttonImage: {
    width: Vars.width / 2.2,
    height: Vars.width / 2.3,
    margin: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "center"
  },
  icon: {
    width: 60,
    height: 60
  },
  maintenanceIcon: {
    width: 60,
    height: 60,
    marginTop: 30
  },
  hr: {
    width: 70,
    height: 30
  },
  divingTextRight: {
    paddingHorizontal: 20,
    textAlign: "right"
  },
  divingTextLeft: {
    paddingHorizontal: 20,
    textAlign: "left"
  }
});
