import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicator,
  KeyboardAvoidingView
} from "react-native";
import React, { Component } from "react";
import HeaderImage from "./../../images/reservation/banner2.png";
import Background from "./../../images/background.png";
import Line from "./../../images/reservation/line2.png";
import Vars from "../../libs/vars";
import FastImage from "react-native-fast-image";
import Header from "../common/header";
import Search from "./../../images/description/search.png";
import Add from "./../../images/boatDetails/plus.png";
import I18n from "./../../languages/";
import InputValidation from "./../common/inputValidation";
import moment from "moment";
import DateTimePicker from "react-native-modal-datetime-picker";
import RNPickerSelect from "./../../libs/picker";
import { inject, observer } from "mobx-react/native";
import config from "../../../config";
import ImagePicker from "react-native-image-picker";

const options = {
  title: "Select Avatar",
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

@inject(stores => ({
  rootStore: stores.rootStore,
  boatStore: stores.boatStore,
  authStore: stores.authStore
}))
@observer
export default class AddBoat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shipeName: "",
      shipeNameError: false,
      activity: "",
      activityError: false,
      load: 0,
      loadError: false,
      price: 0,
      priceError: false,
      detail: "",
      detailError: false,
      to: "",
      toError: false,
      from: "",
      fromError: false,
      repairDateFrom: "",
      repairDateFromError: false,
      repairDateTo: "",
      repairDateToError: false,
      isRepairDateFrom: false,
      marque: "",
      marqueError: false,
      width: 0,
      widthError: false,
      height: 0,
      heightError: false,
      carburant: "",
      carburantError: false,
      isYear: false,
      year: "",
      yearError: false,
      Motorisation: "",
      MotorisationError: false,
      images: [],
      imagesData: [],
      activityData: [
        {
          label: config.trip,
          value: config.trip,
          key: config.trip
        },
        {
          label: config.transport,
          value: config.transport,
          key: config.transport
        },
        {
          label: config.fish,
          value: config.fish,
          key: config.fish
        },
        {
          label: config.dive,
          value: config.dive,
          key: config.dive
        },
        {
          label: config.coachdive,
          value: config.coachdive,
          key: config.coachdive
        },
        {
          label: config.sell,
          value: config.sell,
          key: config.sell
        }
      ]
    };
  }

  clear() {
    this.setState({
      shipeName: "",
      shipeNameError: false,
      activity: "",
      activityError: false,
      load: 0,
      loadError: false,
      price: 0,
      priceError: false,
      detail: "",
      detailError: false,
      to: "",
      toError: false,
      from: "",
      fromError: false,
      repairDateFrom: "",
      repairDateFromError: false,
      repairDateTo: "",
      repairDateToError: false,
      isRepairDateFrom: false,
      images: [],
      imagesData: []
    });
  }

  validation() {
    let TESTIFINPUTISEMPTY = false;
    let TESTIFERROR = false;
    if(this.state.activity === config.sell) {
      TESTIFINPUTISEMPTY =
      !this.state.shipeName.length ||
      !this.state.activity.length ||
      !this.state.load ||
      !this.state.price ||
      !this.state.detail.length ||
      !this.state.marque.length ||
      !this.state.carburant.length ||
      !this.state.year.length ||
      !this.state.width.length ||
      !this.state.Motorisation.length ||
      !this.state.height.length;

      TESTIFERROR =
      this.state.shipeNameError &&
      this.state.loadError &&
      this.state.priceError &&
      this.state.detailError &&
      this.state.marqueError &&
      this.state.carburantError &&
      this.state.yearError &&
      this.state.widthError &&
      this.state.heightError &&
      this.state.MotorisationError;

    } else {

      TESTIFINPUTISEMPTY =
      !this.state.shipeName.length ||
      !this.state.activity.length ||
      !this.state.load ||
      !this.state.price ||
      !this.state.detail.length ||
      !this.state.to.length ||
      !this.state.from.length ||
      !this.state.repairDateFrom.length ||
      !this.state.repairDateTo.length ||
      !this.state.images.length;

     TESTIFERROR =
      this.state.shipeNameError &&
      this.state.loadError &&
      this.state.priceError &&
      this.state.detailError &&
      this.state.toError &&
      this.state.fromError &&
      this.state.repairDateFromError &&
      this.state.repairDateToError;
    }


    if (TESTIFINPUTISEMPTY) {
      AlertIOS.alert(
        I18n.t("errors.reservBoat.requiredTitle"),
        I18n.t("errors.reservBoat.requiredMessage"),
        [
          {
            text: I18n.t("errors.cancel"),
            onPress: () => {},
            style: "cancel"
          }
        ]
      );
      return false;
    }

    if (!TESTIFERROR) {
      return false;
    }
    return true;
  }
  add() {
    if (this.props.rootStore.isLoading) return;
    let isValid = this.validation();
    if(!isValid) return;
    this.props.boatStore
      .addShip({
        type: this.state.activity,
        crew_count: this.state.load,
        maintenance_start: this.state.repairDateFrom,
        maintenance_end: this.state.repairDateTo,
        start: this.state.from,
        end: this.state.to,
        price: this.state.price,
        details: this.state.detail,
        from_city: this.state.from,
        to_city: this.state.to,
        user_id: this.props.authStore.user.id,
        images: this.state.imagesData,
        name: this.state.shipeName,
        marque :this.state.marque,
        carburant :this.state.carburant,
        year :this.state.year,
        width :this.state.width,
        Motorisation :this.state.Motorisation,
        height :this.state.height
      })
      .then(() => {
        this.clear();
        AlertIOS.alert(
          I18n.t("success.reservBoat.messageTitle"),
          I18n.t("success.reservBoat.message"),
          [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]
        );
      })
      .catch(error => {
        if (error && error.dataErrors.has("server_error")) {
          AlertIOS.alert(
            I18n.t("errors.errorTitle"),
            error.dataErrors.get("server_error"),
            [
              {
                text: I18n.t("errors.cancel"),
                onPress: () => {},
                style: "cancel"
              }
            ]
          );
          return;
        }
        if (error && error.status === 400) {
          let messageError = "";
          if (error && error.dataErrors.has("permission"))
            messageError += error.dataErrors.get("permission") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
        if (error && error.status === 422) {
          let messageError = "";
          if (error && error.dataErrors.has("name"))
            messageError += error.dataErrors.get("name") + "\n";
          if (error && error.dataErrors.has("crew_count"))
            messageError += error.dataErrors.get("crew_count") + "\n";
          if (error && error.dataErrors.has("image"))
            messageError += error.dataErrors.get("image") + "\n";
          if (error && error.dataErrors.has("details"))
            messageError += error.dataErrors.get("details") + "\n";
          if (error && error.dataErrors.has("maintenance_start"))
            messageError += error.dataErrors.get("maintenance_start") + "\n";
          if (error && error.dataErrors.has("maintenance_end"))
            messageError += error.dataErrors.get("maintenance_end") + "\n";
          if (error && error.dataErrors.has("group_id"))
            messageError += error.dataErrors.get("group_id") + "\n";
          if (error && error.dataErrors.has("start"))
            messageError += error.dataErrors.get("start") + "\n";
          if (error && error.dataErrors.has("end"))
            messageError += error.dataErrors.get("end") + "\n";
          if (error && error.dataErrors.has("price"))
            messageError += error.dataErrors.get("price") + "\n";
          if (error && error.dataErrors.has("from_city"))
            messageError += error.dataErrors.get("from_city") + "\n";
          if (error && error.dataErrors.has("to_city"))
            messageError += error.dataErrors.get("to_city") + "\n";
          if (error && error.dataErrors.has("type"))
            messageError += error.dataErrors.get("type") + "\n";
          if (error && error.dataErrors.has("year"))
            messageError += error.dataErrors.get("year") + "\n";
          if (error && error.dataErrors.has("marque"))
            messageError += error.dataErrors.get("marque") + "\n";
          if (error && error.dataErrors.has("width"))
            messageError += error.dataErrors.get("width") + "\n";
          if (error && error.dataErrors.has("height"))
            messageError += error.dataErrors.get("height") + "\n";
          if (error && error.dataErrors.has("Carburant"))
            messageError += error.dataErrors.get("Carburant") + "\n";
          if (error && error.dataErrors.has("Motorisation"))
            messageError += error.dataErrors.get("Motorisation") + "\n";
          AlertIOS.alert(I18n.t("errors.errorTitle"), messageError, [
            {
              text: I18n.t("errors.cancel"),
              onPress: () => {},
              style: "cancel"
            }
          ]);
        }
      });
  }

  setRepairDateTo(date) {
    this.setState({
      repairDateTo: date ? moment(date).format("MMM Do YY") : "",
      isRepairDateTo: false,
      repairDateToError: date ? true : I18n.t("errors.InvalidEntry")
    });
  }

  setRepairDateFrom(date) {
    this.setState({
      repairDateFrom: date ? moment(date).format("MMM Do YY") : "",
      isRepairDateFrom: false,
      repairDateFromError: date ? true : I18n.t("errors.InvalidEntry")
    });
  }

  setYear(date) {
    this.setState({
      year: date ? moment(date).format("MMM Do YY") : "",
      isYear: false,
      yearError: date ? true : I18n.t("errors.InvalidEntry")
    });
  }

  setActivity(activity) {
    this.setState({ activity });
  }

  shoosePic() {
    ImagePicker.showImagePicker(options, async response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        alert(I18n.t("errors.errorTitle"));
      } else {
        const source = { uri: response.uri };
        let images = this.state.images;
        let imagesData = this.state.imagesData;
        imagesData.push("data:image/jpeg;base64," + response.data);
        images.push(source);
        this.setState({
          images,
          imagesData
        });
      }
    });
  }

  render() {
    return (
      <FastImage source={Background} style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("addBoat.title")}
          backButton
          notification
        />
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
          <ScrollView style={styles.scrollView}>
            <View style={styles.inputContainer}>
              {this.props.rootStore.position === "right" ? (
                <View style={styles.inputContent}>
                  <InputValidation
                    validator="username"
                    placeholder={I18n.t("addBoat.namePlaceholder")}
                    value={this.state.shipeName}
                    isArabic={true}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={shipeName => this.setState({ shipeName })}
                    style={styles.input}
                    onValidatorExecuted={shipeNameError =>
                      this.setState({ shipeNameError })
                    }
                    validatorExecutionDelay={100}
                  />
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.name")}
                  </Text>
                </View>
              ) : (
                <View style={styles.inputContent}>
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.name")}
                  </Text>
                  <InputValidation
                    validator="username"
                    placeholder={I18n.t("addBoat.namePlaceholder")}
                    value={this.state.shipeName}
                    isArabic={false}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={shipeName => this.setState({ shipeName })}
                    style={styles.input}
                    onValidatorExecuted={shipeNameError =>
                      this.setState({ shipeNameError })
                    }
                    validatorExecutionDelay={100}
                  />
                </View>
              )}
              <FastImage source={Line} style={styles.hr} />
            </View>

            <View style={styles.inputContainer}>
              {this.props.rootStore.position === "right" ? (
                <View style={styles.selectBox}>
                  <RNPickerSelect
                    hideIcon
                    onValueChange={this.setActivity.bind(this)}
                    placeholder={{
                      label: I18n.t("signin.selectTitle"),
                      value: null
                    }}
                    doneText={I18n.t("signin.selectTitle")}
                    items={this.state.activityData}
                  />
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.activity")}
                  </Text>
                </View>
              ) : (
                <View style={styles.selectBox}>
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.activity")}
                  </Text>
                  <RNPickerSelect
                    hideIcon
                    onValueChange={this.setActivity.bind(this)}
                    placeholder={{
                      label: I18n.t("signin.selectTitle"),
                      value: null
                    }}
                    doneText={I18n.t("signin.selectTitle")}
                    items={this.state.activityData}
                  />
                </View>
              )}
              <FastImage source={Line} style={styles.hr} />
            </View>

            <View style={styles.inputContainer}>
              {this.props.rootStore.position === "right" ? (
                <View style={styles.inputContent}>
                  <InputValidation
                    validator="number"
                    placeholder={I18n.t("addBoat.capacityPlaceholder")}
                    value={this.state.load}
                    isArabic={true}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={load => this.setState({ load })}
                    style={styles.input}
                    keyboardType={"numeric"}
                    onValidatorExecuted={loadError =>
                      this.setState({ loadError })
                    }
                    validatorExecutionDelay={100}
                  />
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.capacity")}
                  </Text>
                </View>
              ) : (
                <View style={styles.inputContent}>
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.capacity")}
                  </Text>
                  <InputValidation
                    validator="number"
                    placeholder={I18n.t("addBoat.capacityPlaceholder")}
                    value={this.state.load}
                    keyboardType={"numeric"}
                    isArabic={false}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={load => this.setState({ load })}
                    style={styles.input}
                    onValidatorExecuted={loadError =>
                      this.setState({ loadError })
                    }
                    validatorExecutionDelay={100}
                  />
                </View>
              )}
              <FastImage source={Line} style={styles.hr} />
            </View>

            {/*  marque */}
            {this.state.activity === config.sell ? (
              <View style={styles.inputContainer}>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.inputContent}>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.marque")}
                      value={this.state.marque}
                      isArabic={true}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={marque => this.setState({ marque })}
                      style={styles.input}
                      onValidatorExecuted={marqueError =>
                        this.setState({ marqueError })
                      }
                      validatorExecutionDelay={100}
                    />
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.marque")}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.inputContent}>
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.marque")}
                    </Text>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.marque")}
                      value={this.state.marque}
                      isArabic={false}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={marque => this.setState({ marque })}
                      style={styles.input}
                      onValidatorExecuted={marqueError =>
                        this.setState({ marqueError })
                      }
                      validatorExecutionDelay={100}
                    />
                  </View>
                )}
                <FastImage source={Line} style={styles.hr} />
              </View>
            ) : null}

            {/*  width */}
            {this.state.activity === config.sell ? (
              <View style={styles.inputContainer}>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.inputContent}>
                    <InputValidation
                      validator="number"
                      placeholder={I18n.t("addBoat.width")}
                      value={this.state.width}
                      keyboardType={"numeric"}
                      isArabic={true}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={width => this.setState({ width })}
                      style={styles.input}
                      onValidatorExecuted={widthError =>
                        this.setState({ widthError })
                      }
                      validatorExecutionDelay={100}
                    />
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.width")}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.inputContent}>
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.width")}
                    </Text>
                    <InputValidation
                      validator="number"
                      placeholder={I18n.t("addBoat.width")}
                      keyboardType={"numeric"}
                      isArabic={false}
                      value={this.state.width}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={width => this.setState({ width })}
                      style={styles.input}
                      onValidatorExecuted={widthError =>
                        this.setState({ widthError })
                      }
                      validatorExecutionDelay={100}
                    />
                  </View>
                )}
                <FastImage source={Line} style={styles.hr} />
              </View>
            ) : null}

            {/*  height */}
            {this.state.activity === config.sell ? (
              <View style={styles.inputContainer}>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.inputContent}>
                    <InputValidation
                      validator="number"
                      placeholder={I18n.t("addBoat.height")}
                      value={this.state.height}
                      keyboardType={"numeric"}
                      isArabic={true}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={height => this.setState({ height })}
                      style={styles.input}
                      onValidatorExecuted={heightError =>
                        this.setState({ heightError })
                      }
                      validatorExecutionDelay={100}
                    />
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.height")}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.inputContent}>
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.height")}
                    </Text>
                    <InputValidation
                      validator="number"
                      placeholder={I18n.t("addBoat.height")}
                      value={this.state.height}
                      keyboardType={"numeric"}
                      isArabic={false}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={height => this.setState({ height })}
                      style={styles.input}
                      onValidatorExecuted={heightError =>
                        this.setState({ heightError })
                      }
                      validatorExecutionDelay={100}
                    />
                  </View>
                )}
                <FastImage source={Line} style={styles.hr} />
              </View>
            ) : null}

            {/*  year */}

            {this.state.activity === config.sell ? (
              <View>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.selectBoxContainerInline}>
                    <TouchableOpacity
                      style={styles.selectboxInline}
                      onPress={() => this.setState({ isYear: true })}
                    >
                      <Text
                        style={[
                          styles.date,
                          this.state.yearError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.yearError.length
                          ? this.state.yearError
                          : this.state.year}
                      </Text>
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.year")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isYear}
                        onConfirm={this.setYear.bind(this)}
                        onCancel={this.setYear.bind(this)}
                      />
                      
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View style={styles.leftSelectBoxContainerInline}>
                    <TouchableOpacity
                      style={styles.leftSelectboxInline}
                      onPress={() => this.setState({ isYear: true })}
                    >
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.year")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isYear}
                        onConfirm={this.setYear.bind(this)}
                        onCancel={this.setYear.bind(this)}
                      />
                      <Text
                        style={[
                          styles.date,
                          this.state.yearError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.yearError.length
                          ? this.state.yearError
                          : this.state.year}
                      </Text>
                      
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            ) : null}

            {/*  Carburant */}
            {this.state.activity === config.sell ? (
              <View style={styles.inputContainer}>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.inputContent}>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.Carburant")}
                      value={this.state.carburant}
                      isArabic={true}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={carburant => this.setState({ carburant })}
                      style={styles.input}
                      onValidatorExecuted={carburantError =>
                        this.setState({ carburantError })
                      }
                      validatorExecutionDelay={100}
                    />
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.Carburant")}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.inputContent}>
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.Carburant")}
                    </Text>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.Carburant")}
                      value={this.state.carburant}
                      isArabic={false}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={carburant => this.setState({ carburant })}
                      style={styles.input}
                      onValidatorExecuted={carburantError =>
                        this.setState({ carburantError })
                      }
                      validatorExecutionDelay={100}
                    />
                  </View>
                )}
                <FastImage source={Line} style={styles.hr} />
              </View>
            ) : null}


            {/*  Motorisation */}
            {this.state.activity === config.sell ? (
              <View style={styles.inputContainer}>
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.inputContent}>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.Motorisation")}
                      value={this.state.Motorisation}
                      isArabic={true}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={Motorisation => this.setState({ Motorisation })}
                      style={styles.input}
                      onValidatorExecuted={MotorisationError =>
                        this.setState({ MotorisationError })
                      }
                      validatorExecutionDelay={100}
                    />
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.Motorisation")}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.inputContent}>
                    <Text style={styles.inputTitle}>
                      {I18n.t("addBoat.Motorisation")}
                    </Text>
                    <InputValidation
                      validator="username"
                      placeholder={I18n.t("addBoat.Motorisation")}
                      value={this.state.Motorisation}
                      isArabic={false}
                      errorInputContainerStyle={styles.errorInputContainerStyle}
                      containerStyle={styles.containerStyle}
                      onChangeText={Motorisation => this.setState({ Motorisation })}
                      style={styles.input}
                      onValidatorExecuted={MotorisationError =>
                        this.setState({ MotorisationError })
                      }
                      validatorExecutionDelay={100}
                    />
                  </View>
                )}
                <FastImage source={Line} style={styles.hr} />
              </View>
            ) : null}

            <View style={styles.inputContainer}>
              {this.props.rootStore.position === "right" ? (
                <View style={styles.inputContent}>
                  <InputValidation
                    validator="number"
                    placeholder={I18n.t("addBoat.pricePlaceHolder")}
                    value={this.state.price}
                    isArabic={true}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={price => this.setState({ price })}
                    style={styles.input}
                    keyboardType={"numeric"}
                    onValidatorExecuted={priceError =>
                      this.setState({ priceError })
                    }
                    validatorExecutionDelay={100}
                  />
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.price")}
                  </Text>
                </View>
              ) : (
                <View style={styles.inputContent}>
                  <Text style={styles.inputTitle}>
                    {I18n.t("addBoat.price")}
                  </Text>
                  <InputValidation
                    validator="number"
                    isArabic={false}
                    placeholder={I18n.t("addBoat.pricePlaceHolder")}
                    value={this.state.price}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={price => this.setState({ price })}
                    style={styles.input}
                    keyboardType={"numeric"}
                    onValidatorExecuted={priceError =>
                      this.setState({ priceError })
                    }
                    validatorExecutionDelay={100}
                  />
                </View>
              )}
              <FastImage source={Line} style={styles.hr} />
            </View>

            {this.props.rootStore.position === "right" ? (
              <View style={styles.textAreaContainer}>
                <Text style={styles.textAreaTitle}>
                  {I18n.t("addBoat.details")}
                </Text>
                <View style={styles.textAreaInputWrapper}>
                  <InputValidation
                    validator="message"
                    isArabic={true}
                    placeholder={I18n.t("addBoat.detailsPlaceholder")}
                    value={this.state.detail}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={detail => this.setState({ detail })}
                    style={styles.textArea}
                    numberOfLines={4}
                    multiline={true}
                    onValidatorExecuted={detailError =>
                      this.setState({ detailError })
                    }
                    validatorExecutionDelay={100}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.leftTextAreaContainer}>
                <Text style={styles.leftTextAreaTitle}>
                  {I18n.t("addBoat.details")}
                </Text>
                <View style={styles.leftTextAreaInputWrapper}>
                  <InputValidation
                    validator="message"
                    isArabic={false}
                    placeholder={I18n.t("addBoat.detailsPlaceholder")}
                    value={this.state.detail}
                    errorInputContainerStyle={styles.errorInputContainerStyle}
                    containerStyle={styles.containerStyle}
                    onChangeText={detail => this.setState({ detail })}
                    style={styles.leftTextArea}
                    numberOfLines={4}
                    multiline={true}
                    onValidatorExecuted={detailError =>
                      this.setState({ detailError })
                    }
                    validatorExecutionDelay={100}
                  />
                </View>
              </View>
            )}

            {this.state.activity !== config.sell ? (
              <View style={styles.selectBoxWrapper}>
                {this.props.rootStore.position === "right" ? (
                  <Text style={styles.selectboxTitle}>
                    {I18n.t("addBoat.direction")}
                  </Text>
                ) : (
                  <Text style={styles.leftSelectboxTitle}>
                    {I18n.t("addBoat.direction")}
                  </Text>
                )}
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.selectBoxContainer}>
                    <View style={styles.selectbox}>
                      <Text style={styles.selectboxText}>
                        {I18n.t("addBoat.directionTo")}
                      </Text>
                      <InputValidation
                        validator="username"
                        isArabic={true}
                        placeholder={I18n.t("addBoat.directionTo")}
                        value={this.state.to}
                        errorInputContainerStyle={
                          styles.errorInputContainerStyle
                        }
                        containerStyle={styles.containerStyle}
                        onChangeText={to => this.setState({ to })}
                        style={styles.input}
                        onValidatorExecuted={toError =>
                          this.setState({ toError })
                        }
                        validatorExecutionDelay={100}
                      />
                      <FastImage
                        source={Line}
                        style={styles.hr_1}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                    <View style={styles.selectbox}>
                      <Text style={styles.selectboxText}>
                        {I18n.t("addBoat.directionFrom")}
                      </Text>
                      <InputValidation
                        validator="username"
                        isArabic={true}
                        placeholder={I18n.t("addBoat.directionFrom")}
                        value={this.state.from}
                        errorInputContainerStyle={
                          styles.errorInputContainerStyle
                        }
                        containerStyle={styles.containerStyle}
                        onChangeText={from => this.setState({ from })}
                        style={styles.input}
                        onValidatorExecuted={fromError =>
                          this.setState({ fromError })
                        }
                        validatorExecutionDelay={100}
                      />
                      <FastImage
                        source={Line}
                        style={styles.hr_1}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </View>
                ) : (
                  <View style={styles.leftSelectBoxContainer}>
                    <View style={styles.leftSelectbox}>
                      <Text style={styles.leftSelectboxText}>
                        {I18n.t("addBoat.directionTo")}
                      </Text>
                      <InputValidation
                        validator="username"
                        isArabic={false}
                        placeholder={I18n.t("addBoat.directionTo")}
                        value={this.state.to}
                        errorInputContainerStyle={
                          styles.errorInputContainerStyle
                        }
                        containerStyle={styles.containerStyle}
                        onChangeText={to => this.setState({ to })}
                        style={styles.input}
                        onValidatorExecuted={toError =>
                          this.setState({ toError })
                        }
                        validatorExecutionDelay={100}
                      />
                      <FastImage
                        source={Line}
                        style={styles.hr_1}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                    <View style={styles.leftSelectbox}>
                      <Text style={styles.leftSelectboxText}>
                        {I18n.t("addBoat.directionFrom")}
                      </Text>
                      <InputValidation
                        validator="username"
                        placeholder={I18n.t("addBoat.directionFrom")}
                        value={this.state.from}
                        isArabic={false}
                        errorInputContainerStyle={
                          styles.errorInputContainerStyle
                        }
                        containerStyle={styles.containerStyle}
                        onChangeText={from => this.setState({ from })}
                        style={styles.input}
                        onValidatorExecuted={fromError =>
                          this.setState({ fromError })
                        }
                        validatorExecutionDelay={100}
                      />
                      <FastImage
                        source={Line}
                        style={styles.hr_1}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </View>
                )}
              </View>
            ) : null}

            {this.state.activity !== config.sell ? (
              <View style={styles.selectBoxWrapper}>
                {this.props.rootStore.position === "right" ? (
                  <Text style={styles.selectboxTitle}>
                    {I18n.t("addBoat.fix")}
                  </Text>
                ) : (
                  <Text style={styles.leftSelectboxTitle}>
                    {I18n.t("addBoat.fix")}
                  </Text>
                )}
                {this.props.rootStore.position === "right" ? (
                  <View style={styles.selectBoxContainer}>
                    <TouchableOpacity
                      style={styles.selectbox}
                      onPress={() => this.setState({ isRepairDateTo: true })}
                    >
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.fixTo")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isRepairDateTo}
                        onConfirm={this.setRepairDateTo.bind(this)}
                        onCancel={this.setRepairDateTo.bind(this)}
                      />
                      <Text
                        style={[
                          styles.date,
                          this.state.repairDateToError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.repairDateToError.length
                          ? this.state.repairDateToError
                          : this.state.repairDateTo}
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.selectbox}
                      onPress={() => this.setState({ isRepairDateFrom: true })}
                    >
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.fixFrom")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isRepairDateFrom}
                        onConfirm={this.setRepairDateFrom.bind(this)}
                        onCancel={this.setRepairDateFrom.bind(this)}
                      />
                      <Text
                        style={[
                          styles.date,
                          this.state.repairDateFromError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.repairDateFromError.length
                          ? this.state.repairDateFromError
                          : this.state.repairDateFrom}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View style={styles.leftSelectBoxContainer}>
                    <TouchableOpacity
                      style={styles.leftSelectbox}
                      onPress={() => this.setState({ isRepairDateFrom: true })}
                    >
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.fixFrom")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isRepairDateFrom}
                        onConfirm={this.setRepairDateFrom.bind(this)}
                        onCancel={this.setRepairDateFrom.bind(this)}
                      />
                      <Text
                        style={[
                          styles.date,
                          this.state.repairDateFromError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.repairDateFromError.length
                          ? this.state.repairDateFromError
                          : this.state.repairDateFrom}
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.leftSelectbox}
                      onPress={() => this.setState({ isRepairDateTo: true })}
                    >
                      <Text style={styles.inputTitle}>
                        {I18n.t("addBoat.fixTo")}
                      </Text>
                      <DateTimePicker
                        isVisible={this.state.isRepairDateTo}
                        onConfirm={this.setRepairDateTo.bind(this)}
                        onCancel={this.setRepairDateTo.bind(this)}
                      />
                      <Text
                        style={[
                          styles.date,
                          this.state.repairDateToError.length && {
                            color: "red"
                          }
                        ]}
                      >
                        {this.state.repairDateToError.length
                          ? this.state.repairDateToError
                          : this.state.repairDateTo}
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            ) : null}

            <ScrollView horizontal={true} style={styles.boatImageScrollView}>
              <TouchableHighlight
                style={styles.boatImageWrapper}
                onPress={this.shoosePic.bind(this)}
              >
                <FastImage
                  source={Add}
                  style={styles.boatImage}
                  resizeMode={FastImage.resizeMode.center}
                />
              </TouchableHighlight>
              {this.state.images.map((element, key) => (
                <View style={styles.boatImageWrapper} key={key}>
                  <FastImage
                    source={{ uri: element.uri }}
                    style={styles.boatImage}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </View>
              ))}
            </ScrollView>
          </ScrollView>
          <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
        </KeyboardAvoidingView>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.button}
            onPress={this.add.bind(this)}
          >
            {this.props.rootStore.isLoading ? (
              <ActivityIndicator color={"white"} />
            ) : (
              <Text style={styles.buttonText}>{I18n.t("addBoat.save")}</Text>
            )}
          </TouchableHighlight>
        </View>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50
  },
  hr: {
    width: Vars.width - 50,
    height: 7
  },
  selectBoxWrapper: {
    flexDirection: "column",
    justifyContent: "space-between",
    width: Vars.width - 25,
    marginTop: 20
  },
  selectBoxContainer: {
    justifyContent: "space-between",
    alignItems: "flex-end",
    flexDirection: "row",
    paddingHorizontal: 25,
    flex: 1,
    paddingTop: 10
  },
  selectbox: {
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    minWidth: 100
  },
  selectboxInline: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 10
  },
  selectboxText: {
    textAlign: "right"
  },
  leftSelectBoxContainer: {
    justifyContent: "space-between",
    alignItems: "flex-start",
    flexDirection: "row",
    paddingHorizontal: 25,
    flex: 1,
    paddingTop: 10
  },
  leftSelectBoxContainerInline: {
    justifyContent: "space-between",
    alignItems: "flex-start",
    flexDirection: "row",
    paddingHorizontal: 25,
    flex: 1,
    paddingTop: 10
  },
  leftSelectbox: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    minWidth: 100
  },
  leftSelectboxInline: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    flex: 1,
    paddingTop: 10
  },
  selectBoxContainerInline: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  leftSelectboxText: {
    textAlign: "left"
  },
  hr_1: {
    width: Vars.width / 2 - 50,
    height: 10
  },
  selectboxTitle: {
    textAlign: "right"
  },
  leftSelectboxTitle: {
    textAlign: "left",
    paddingHorizontal: 25
  },
  inputContainer: {
    width: Vars.width,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 5
  },
  inputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: Vars.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 25
  },
  selectBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: Vars.width,
    alignItems: "center",
    paddingHorizontal: 25
  },
  input: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  inputTitle: {},
  button: {
    height: 40,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 60
  },
  buttonContainer: {
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical:10
  },
  buttonText: {
    color: "white"
  },
  textAreaContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    paddingHorizontal: 25,
    marginTop: 20
  },
  textAreaInputWrapper: {
    borderColor: "#F2F2F2",
    borderWidth: 0.5
  },
  textArea: {
    flex: 2,
    height: 55,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "right",
    paddingHorizontal: 5
  },
  textAreaTitle: {
    textAlign: "right",
    marginBottom: 5
  },
  leftTextAreaContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    paddingHorizontal: 25,
    marginTop: 20
  },
  leftTextAreaInputWrapper: {
    borderColor: "#F2F2F2",
    borderWidth: 0.5
  },
  leftTextArea: {
    flex: 2,
    height: 55,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left",
    paddingHorizontal: 5
  },
  leftTextAreaTitle: {
    textAlign: "left",
    marginBottom: 5
  },
  scrollView: {
    marginTop: 60
  },
  boatImage: {
    width: 80,
    height: 60,
    borderRadius: 5,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    shadowColor: "#696969",
    backgroundColor: "#ffffff"
  },
  boatImageWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
    shadowColor: "#696969",
    backgroundColor: "#ffffff",
    margin: 10
  },
  boatImageScrollView: {
    flexDirection: "row",
    minHeight: 80
  },
  containerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center"
  },
  errorInputContainerStyle: {
    flex: 2,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "red",
    borderBottomWidth: 1
  }
});
