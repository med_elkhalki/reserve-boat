import {
    View,
    Text,
    ImageBackground,
    Image,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import React, {Component} from 'react';
import {Navigation} from 'react-native-navigation';
import Background from './../../images/splash.png';
import HeaderImage from './../../images/boatDetails/boatDesc.png';
import ButtonBackground from './../../images/description/buttonBackground.png';
import Weight from './../../images/boatDetails/weight.png';
import Phone from './../../images/boatDetails/mobile2.png';
import Starts from './../../images/boatDetails/starts.png';
import RatingIcon from './../../images/boatDetails/rating-icon.png';
import User from './../../images/boatDetails/name2.png';
import Arrow from './../../images/boatDetails/arrow1.png';
import Vars from './../../libs/vars';
import FastImage from 'react-native-fast-image';
import Header from './../common/header';
import Screens from './../../navigation/screensName';
import {inject, observer} from 'mobx-react/native';
import I18n from '../../languages';
import boat_1 from './../../images/description/boat_1.png';
import boat_2 from './../../images/description/boat_2.png';
import boat_3 from './../../images/description/boat_3.png';
import Config from "./../../../config";
import config from './../../../config';

@inject(stores => ({
  rootStore: stores.rootStore,
  boatStore: stores.boatStore,
  authStore: stores.authStore
}))
@observer
export default class BoatDetails extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("name", this.props.id)
    this.props.boatStore.getBoatsDetailsAction(this.props.id);
  }
  openModal() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.modals.rateBoat,
          options: {
            overlay: {
              interceptTouchOutside: true
            }
          }
        }
      },
      "showOverlay"
    );
  }

  reserveBoat() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.reservation.reservBoat,
          options: {
            topBar: {
              visible: false
            }
          },
          passProps: {
            id: this.props.id
          }
        }
      },
      "push"
    );
  }

  add() {}

  update() {}

  render() {
    return (
      <View style={styles.container}>
        <Header
          componentId={this.props.componentId}
          title={I18n.t("boatDetails.title")}
          backButton
          notification
        />
        <ScrollView>
          <FastImage source={HeaderImage} style={styles.headerImage}>
            <View style={styles.pin}>
              <Text style={styles.pinText}>
                {this.props.boatStore.boatDetail.price}
              </Text>
            </View>
            <View style={styles.descContainer}>
              <Text style={styles.title}>
                {this.props.boatStore.boatDetail.name}
              </Text>
              <Text style={styles.subTitle}>
                {I18n.t("boatDetails.boatSubTitle")}
              </Text>
              {this.props.authStore.user.isAdmin ? null
              // (
              //   <View style={styles.buttonsContainer}>
              //     <TouchableOpacity
              //       style={styles.buttonReserv}
              //       onPress={this.add.bind(this)}
              //     >
              //       <Text style={styles.buttonReservText}>
              //         {I18n.t("boatDetails.reserveNow")}
              //       </Text>
              //     </TouchableOpacity>
              //     <TouchableOpacity
              //       style={styles.buttonReserv}
              //       onPress={this.update.bind(this)}
              //     >
              //       <Text style={styles.buttonReservText}>
              //         {I18n.t("boatDetails.reserveNow")}
              //       </Text>
              //     </TouchableOpacity>
              //   </View>
              // ) 
              : (
                <TouchableOpacity
                  style={styles.buttonReserv}
                  onPress={this.reserveBoat.bind(this)}
                >
                  <Text style={styles.buttonReservText}>
                    {I18n.t("boatDetails.reserveNow")}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </FastImage>

          <View style={styles.buttonsContainer}>
            <View style={styles.button}>
              <View style={styles.buttonImage}>
                <FastImage source={Weight} style={styles.icon} />
                <Text>{I18n.t("boatDetails.boatWeight")}</Text>
                <Text>{this.props.boatStore.boatDetail.crewCount}</Text>
              </View>
            </View>

            {this.props.authStore.user.isAdmin ? (
              <View style={styles.button}>
                <View style={styles.buttonImage}>
                  <FastImage source={Phone} style={styles.icon} />
                  <Text>{I18n.t("boatDetails.boatPhone")}</Text>
                  <Text>{this.props.authStore.user.phone}</Text>
                </View>
              </View>
            ) : (
              <TouchableOpacity
                style={styles.button}
                onPress={this.openModal.bind(this)}
              >
                <View style={styles.buttonImage}>
                  <FastImage source={RatingIcon} style={styles.icon} />
                  <Text>{this.props.type === config.coachdive ? I18n.t("boatDetails.tripRating") : I18n.t("boatDetails.boateRating")}</Text>
                  <Text>{""}</Text>
                </View>
              </TouchableOpacity>
            )}
            <View style={styles.button}>
              <View style={styles.buttonImage}>
                <FastImage source={User} style={styles.icon} />
                <Text>{I18n.t("boatDetails.boatOwner")}</Text>
                <Text>{this.props.boatStore.boatDetail.user.name}</Text>
              </View>
            </View>
          </View>

          {this.props.type === Config.sell ? (
            <View>
              <View style={styles.buttonsContainer}>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.year")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Annee}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.marque")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Marque}
                  </Text>
                </View>
              </View>
              <View style={styles.buttonsContainer}>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.width")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Largeur}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.height")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Longueur}
                  </Text>
                </View>
              </View>

              <View style={styles.buttonsContainer}>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.Carburant")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Carburant}
                  </Text>
                </View>
                <View style={styles.item}>
                  <Text>{I18n.t("boatDetails.Motorisation")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Motorisation}
                  </Text>
                </View>
              </View>

              <View style={styles.buttonsContainer}>
                <View style={styles.lastItem}>
                  <Text>{I18n.t("boatDetails.Description")}</Text>
                  <Text>
                    {this.props.boatStore.boatDetail.Description}
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.buttonsContainer}>
              <View style={styles.item}>
                <Text>{I18n.t("boatDetails.leavedCountry")}</Text>
                <Text>
                  {I18n.t("boatDetails.city")}{" "}
                  {this.props.boatStore.boatDetail.fromCity}
                </Text>
              </View>
              <FastImage source={Arrow} style={styles.arrow} />
              <View style={styles.item}>
                <Text>{I18n.t("boatDetails.arrivedCountry")}</Text>
                <Text>
                  {I18n.t("boatDetails.city")}{" "}
                  {this.props.boatStore.boatDetail.toCity}
                </Text>
              </View>
            </View>
          )}

          <ScrollView
            horizontal
            contentContainerStyle={styles.buttonsContainer}
          >
            {this.props.boatStore.boatDetail.images.map((item, key) => (
              <View key={key} style={styles.section}>
                <FastImage
                  source={{ uri: item.img }}
                  resizeMode={FastImage.resizeMode.cover}
                  style={styles.boatImages}
                />
              </View>
            ))}
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80
  },
  headerImage: {
    width: Vars.width,
    height: 200,
    marginBottom: 10,
    paddingVertical: 30,
    paddingHorizontal: 20
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 80
  },
  inputContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20
  },
  input: {
    backgroundColor: "#ffffff3b",
    height: 45,
    borderRadius: 25,
    textAlign: "center"
  },
  circle: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0
  },
  link: {
    color: "white",
    fontSize: 12,
    marginBottom: 20
  },
  button: {
    borderWidth: 0.2,
    borderColor: "#eee"
  },
  buttonText: {
    color: Vars.mainColor
  },
  linkContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-end"
  },
  buttonImage: {
    width: Vars.width / 3.2,
    height: Vars.width / 3.3,
    alignItems: "center",
    justifyContent: "center"
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "center",
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "#ccc",
    shadowOpacity: 1,
    marginBottom: 10
  },
  icon: {
    width: 40,
    height: 40,
    marginBottom: 10
  },
  starsIcon: {
    width: 40,
    height: 40
  },
  boatImages: {
    width: 80,
    height: 60,
    marginBottom: 10
  },
  arrow: {
    width: 40,
    height: 40,
    position: "absolute",
    top: Vars.width / 3.3 / 3
  },
  hr: {
    width: 70,
    height: 30
  },
  pin: {
    height: 20,
    backgroundColor: "#eee",
    opacity: 0.5,
    maxWidth: 100,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  pinText: {
    fontSize: 10
  },
  descContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 20
  },
  buttonReserv: {
    height: 20,
    backgroundColor: Vars.mainColor,
    borderRadius: 20,
    paddingHorizontal: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  buttonReservText: {
    color: "white",
    fontSize: 10
  },
  title: {
    color: "white",
    fontSize: 15,
    marginBottom: 10
  },
  subTitle: {
    color: "white",
    fontSize: 13,
    marginBottom: 10
  },
  item: {
    width: Vars.width / 2 - 14,
    height: Vars.width / 3.3,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: "#eee"
  },
  lastItem: {
    width: Vars.width - 28,
    height: Vars.width / 3.3,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: "#eee"
  },
  section: {
    width: Vars.width / 3.5,
    height: Vars.width / 3.5,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: "#eee",
    marginHorizontal: 15
  },
  sectionMed: {
    marginHorizontal: 15
  }
});
