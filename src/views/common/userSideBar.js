import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/sideBar/background2.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import Logo from "./../../images/sideBar/logobackground.png";
import Home from "./../../images/sideBar/home.png";
import Logout from "./../../images/sideBar/logout.png";
import ContactUs from "./../../images/sideBar/contactus.png";
import Policy from "./../../images/sideBar/policy.png";
import Settings from "./../../images/sideBar/setting.png";
import Reserve from "./../../images/sideBar/reserve.png";
import { inject, observer } from "mobx-react/native";
import Screens from "./../../navigation/screensName";
import { Navigation } from "react-native-navigation";
import Header from "./header";
import I18n from "./../../languages/";
import Api from "./../../libs/api";
import {
  LoginStack,
  sideMenuStack,
  userStack
} from "./../../navigation/stacks";

@inject(stores => ({
  rootStore: stores.rootStore,
  authStore: stores.authStore
}))
@observer
export default class UserSideBar extends Component {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  openSceen(screenName) {
    this.props.rootStore.navigate(
      `${Screens.stacks.userStack}_${this.props.rootStore.position}_${
        this.props.rootStore.lang
      }`,
      sideMenuStack(screenName),
      "setStackRoot"
    );
    this.props.rootStore.navigate(
        this.props.componentId,
        {
          sideMenu: {
            [this.props.rootStore.position]: {
              visible: false
            }
          }
        },
        "mergeOptions"
    );
  }

  async logout() {
    await this.props.authStore.logoutAction();
    await this.props.rootStore.navigate(
      this.props.componentId,
      LoginStack(),
      "setRoot"
    );
  }

  async changeLang(lang) {
    await this.props.rootStore.changeLang(lang);
    Api.setLocaleLanguage(lang);
    await this.props.rootStore.navigate(
      this.props.componentId,
      userStack(this.props.rootStore.lang),
      "setRoot"
    );
  }

  activeTab(screenName) {
    if(this.props.rootStore.position === "right") {
      return this.props.rootStore.currentScreen === screenName ? styles.sectionActive : styles.section;
    }
    return this.props.rootStore.currentScreen === screenName ? styles.sectionActiveLeft : styles.sectionLeft;
  }
  activeTabText(screenName) {
    return this.props.rootStore.currentScreen === screenName ? styles.sectionTitleActive : {};
  }
  render() {
    return (
      <FastImage
        source={Background}
        style={styles.backgroundImage}
        resizeMode={FastImage.resizeMode.stretch}
      >
        <Header
          componentId={this.props.componentId}
          sideMenu
          title={I18n.t("menu.title")}
        />
        {this.props.rootStore.position === "right" ? (
          <View style={styles.headerSection}>
            <View style={styles.titleContainer}>
              <Text>{this.props.authStore.user.name}</Text>
              <View>
                <Text style={styles.countryText}>
                  {this.props.authStore.user.phone}
                </Text>
              </View>
            </View>
            <FastImage source={Logo} style={styles.logo} />
          </View>
        ) : (
          <View style={styles.headerSectionLeft}>
            <FastImage source={Logo} style={styles.logo} />
            <View style={styles.titleContainerLeft}>
              <Text>{this.props.authStore.user.name}</Text>
              <View>
                <Text style={styles.countryText}>
                  {this.props.authStore.user.phone}
                </Text>
              </View>
            </View>
          </View>
        )}
        <ScrollView>
          <TouchableOpacity
            onPress={this.openSceen.bind(this, Screens.boat.home)}
          >
            {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.boat.home)}>
                <Text style={this.activeTabText(Screens.boat.home)}>
                  {I18n.t("menu.home")}
                </Text>
                <FastImage source={Home} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.boat.home)}>
                <FastImage source={Home} style={styles.icon} />
                <Text style={this.activeTabText(Screens.boat.home)}>
                  {I18n.t("menu.home")}
                </Text>
              </View>
            )}
          </TouchableOpacity>
          {this.props.authStore.user.isAdmin ? (
            <TouchableOpacity
              onPress={this.openSceen.bind(
                this,
                Screens.boat.myBoats
              )}
            >
              {this.props.rootStore.position === "right" ? (
                <View style={this.activeTab(Screens.boat.myBoats)}>
                  <Text style={this.activeTabText(Screens.boat.myBoats)}>{I18n.t("menu.myBoats")}</Text>
                  <FastImage source={Reserve} style={styles.icon} />
                </View>
              ) : (
                <View style={this.activeTab(Screens.boat.myBoats)}>
                  <FastImage source={Reserve} style={styles.icon} />
                  <Text style={this.activeTabText(Screens.boat.myBoats)}>{I18n.t("menu.myBoats")}</Text>
                </View>
              )}
            </TouchableOpacity>
          ) : null}
          <TouchableOpacity
            onPress={this.openSceen.bind(
              this,
              Screens.reservation.reservationList
            )}
          >
            {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.reservation.reservationList)}>
                <Text style={this.activeTabText(Screens.reservation.reservationList)}>{I18n.t("menu.myReservation")}</Text>
                <FastImage source={Reserve} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.reservation.reservationList)}>
                <FastImage source={Reserve} style={styles.icon} />
                <Text style={this.activeTabText(Screens.reservation.reservationList)}>{I18n.t("menu.myReservation")}</Text>
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openSceen.bind(
              this,
              Screens.reservation.PurchasesList
            )}
          >
          {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.reservation.PurchasesList)}>
                <Text style={this.activeTabText(Screens.reservation.PurchasesList)}>{I18n.t("menu.purchasesList")}</Text>
                <FastImage source={Reserve} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.reservation.PurchasesList)}>
                <FastImage source={Reserve} style={styles.icon} />
                <Text style={this.activeTabText(Screens.reservation.PurchasesList)}>{I18n.t("menu.purchasesList")}</Text>
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openSceen.bind(this, Screens.settings.appInfo)}
          >
            {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.settings.appInfo)}>
                <Text style={this.activeTabText(Screens.settings.appInfo)}>{I18n.t("menu.infos")}</Text>
                <FastImage source={Policy} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.settings.appInfo)}>
                <FastImage source={Policy} style={styles.icon} />
                <Text style={this.activeTabText(Screens.settings.appInfo)}>{I18n.t("menu.infos")}</Text>
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openSceen.bind(
              this,
              Screens.settings.profileSettings
            )}
          >
            {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.settings.profileSettings)}>
                <Text style={this.activeTabText(Screens.settings.profileSettings)}>{I18n.t("menu.settings")}</Text>
                <FastImage source={Settings} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.settings.profileSettings)}>
                <FastImage source={Settings} style={styles.icon} />
                <Text style={this.activeTabText(Screens.settings.profileSettings)}>{I18n.t("menu.settings")}</Text>
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.openSceen.bind(this, Screens.settings.contactUs)}
          >
            {this.props.rootStore.position === "right" ? (
              <View style={this.activeTab(Screens.settings.contactUs)}>
                <Text style={this.activeTabText(Screens.settings.contactUs)}>{I18n.t("menu.contactUs")}</Text>
                <FastImage source={ContactUs} style={styles.icon} />
              </View>
            ) : (
              <View style={this.activeTab(Screens.settings.contactUs)}>
                <FastImage source={ContactUs} style={styles.icon} />
                <Text style={this.activeTabText(Screens.settings.contactUs)}>{I18n.t("menu.contactUs")}</Text>
              </View>
            )}
          </TouchableOpacity>
          <TouchableOpacity onPress={this.logout.bind(this)}>
            {this.props.rootStore.position === "right" ? (
              <View style={styles.section}>
                <Text>{I18n.t("menu.logout")}</Text>
                <FastImage source={Logout} style={styles.icon} />
              </View>
            ) : (
              <View style={styles.sectionLeft}>
                <FastImage source={Logout} style={styles.icon} />
                <Text>{I18n.t("menu.logout")}</Text>
              </View>
            )}
          </TouchableOpacity>
        </ScrollView>
        <View style={styles.langSection}>
          <TouchableOpacity
            style={styles.langSectionBtn}
            onPress={this.changeLang.bind(this, "ar")}
          >
            <Text
              style={
                this.props.rootStore.lang === "ar"
                  ? styles.langTextActive
                  : styles.langText
              }
            >
              {"العربية"}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.langSectionBtn}
            onPress={this.changeLang.bind(this, "fr")}
          >
            <Text
              style={
                this.props.rootStore.lang === "fr"
                  ? styles.langTextActive
                  : styles.langText
              }
            >
              {"Français"}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.langSectionBtn}
            onPress={this.changeLang.bind(this, "en")}
          >
            <Text
              style={
                this.props.rootStore.lang === "en"
                  ? styles.langTextActive
                  : styles.langText
              }
            >
              {"English"}
            </Text>
          </TouchableOpacity>
        </View>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    paddingTop: 100
  },
  logo: {
    width: 60,
    height: 60,
    marginHorizontal: 20
  },
  headerSection: {
    width: Vars.width,
    minHeight: 100,
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    borderBottomWidth: 0.5,
    borderBottomColor: "#eee",
    marginBottom: 10
  },
  headerSectionLeft: {
    width: Vars.width,
    minHeight: 100,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    borderBottomWidth: 0.5,
    borderBottomColor: "#eee",
    marginBottom: 10
  },
  section: {
    width: Vars.width,
    height: 40,
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    marginVertical: 10
  },
  sectionLeft: {
    width: Vars.width,
    height: 40,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginVertical: 10
  },
  langSection: {
    width: Vars.width,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginVertical: 10
  },
  langSectionBtn: {
    width: Vars.width / 3,
    alignItems: "center",
    justifyContent: "center"
  },
  sectionActive: {
    width: Vars.width,
    height: 40,
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    marginVertical: 10,
    borderRightColor: Vars.mainColor,
    borderRightWidth: 2
  },
  sectionActiveLeft: {
    width: Vars.width,
    height: 40,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    marginVertical: 10,
    borderLeftColor: Vars.mainColor,
    borderLeftWidth: 2
  },
  icon: {
    width: 30,
    height: 30,
    marginHorizontal: 20
  },
  titleContainer: {
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "flex-start"
  },
  titleContainerLeft: {
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  countryText: {
    fontSize: 11,
    color: "#999"
  },
  sectionTitleActive: {
    color: Vars.mainColor
  },
  langText: {},
  langTextActive: {
    color: Vars.mainColor
  }
});
