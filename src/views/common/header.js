import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import React, { Component } from "react";
import Background from "./../../images/header/header.png";
import Menu from "./../../images/header/menu.png";
import Close from "./../../images/header/close.png";
import Notifications from "./../../images/header/notifications.png";
import Back from "./../../images/header/arrow.png";
import Vars from "./../../libs/vars";
import FastImage from "react-native-fast-image";
import { Navigation } from "react-native-navigation";
import Screens from "./../../navigation/screensName";
import { inject, observer } from "mobx-react/native";

const resiseMode = FastImage.resizeMode.stretch

@inject(stores => ({
  rootStore: stores.rootStore
}))
@observer
export default class Header extends Component {
  constructor(props) {
    super(props);
  }

  toggleSideMenu(status) {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        sideMenu: {
          [this.props.rootStore.position]: {
            visible: status
          }
        }
      },
      "mergeOptions"
    );
  }
  goBack() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {},
      "pop"
    );
  }
  openNotificationList() {
    this.props.rootStore.navigate(
      this.props.componentId,
      {
        component: {
          name: Screens.notifications.boatNotifications,
          options: {
            topBar: {
              visible: false
            }
          }
        }
      },
      "showModal"
    );
  }

  closeModal() {
    this.props.rootStore.navigate(this.props.componentId, {}, "dismissModal");
  }

  render() {
    if (this.props.sideMenu) {
      return (
        <FastImage source={Background} style={styles.backgroundImage} resizeMode={resiseMode}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.toggleSideMenu.bind(this, false)}
          >
            <FastImage source={Close} style={styles.icon} />
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title || ""}</Text>
          </View>
          <View style={styles.button} />
        </FastImage>
      );
    }
    if (this.props.modal) {
      return (
        <FastImage source={Background} style={styles.backgroundImage} resizeMode={resiseMode}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.closeModal.bind(this, false)}
          >
            <FastImage source={Close} style={styles.icon} />
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title || ""}</Text>
          </View>
          <View style={styles.button} />
        </FastImage>
      );
    }
    if(this.props.backButton && this.props.notification) {
      return (
        <FastImage source={Background} style={styles.backgroundImage} resizeMode={resiseMode}>
          <TouchableOpacity
            style={styles.button}
            onPress={
              this.props.rootStore.position === "right"
                ? this.openNotificationList.bind(this)
                : this.goBack.bind(this)
            }
          >
            <FastImage
              source={
                this.props.rootStore.position === "right" ? Notifications : Back
              }
              style={styles.icon}
            />
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title || ""}</Text>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={
              this.props.rootStore.position === "right"
                ? this.goBack.bind(this)
                : this.openNotificationList.bind(this)
            }
          >
            <FastImage
              source={
                this.props.rootStore.position === "right" ? Back : Notifications
              }
              style={styles.icon}
            />
          </TouchableOpacity>
        </FastImage>
      );
    }
    if(this.props.backButton) {
      return (
        <FastImage source={Background} style={styles.backgroundImage} resizeMode={resiseMode}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.goBack.bind(this)}
          >
            <FastImage source={Back} style={styles.icon} />
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title || ""}</Text>
          </View>
          <View style={styles.button} />
        </FastImage>
      );

    }

    if(this.props.login) {
      return (
        <View style={styles.backgroundImage}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.goBack.bind(this)}
          >
            <FastImage source={Back} style={styles.icon} />
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title || ""}</Text>
          </View>
          <View style={styles.button} />
        </View>
      );

    }

    return (
      <FastImage source={Background} style={styles.backgroundImage} resizeMode={resiseMode}>
        <TouchableOpacity
          style={styles.button}
          onPress={
            this.props.rootStore.position === "right"
              ? this.openNotificationList.bind(this)
              : this.toggleSideMenu.bind(this, true)
          }
        >
          <FastImage
            source={
              this.props.rootStore.position === "right" ? Notifications : Menu
            }
            style={styles.icon}
          />
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.title || ""}</Text>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={
            this.props.rootStore.position === "right"
              ? this.toggleSideMenu.bind(this, true)
              : this.openNotificationList.bind(this)
          }
        >
          <FastImage
            source={
              this.props.rootStore.position === "right" ? Menu : Notifications
            }
            style={styles.icon}
          />
        </TouchableOpacity>
      </FastImage>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    width: Vars.width,
    position: "absolute",
    top: 0,
    flexDirection: "row",
    zIndex: 1000
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 80
  },
  inputContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    marginBottom: 20
  },
  input: {
    backgroundColor: "#ffffff3b",
    height: 45,
    borderRadius: 25,
    textAlign: "center"
  },
  circle: {
    width: 45,
    height: 45,
    backgroundColor: "white",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    position: "absolute",
    right: 0
  },
  link: {
    color: "white",
    fontSize: 12,
    marginBottom: 20
  },
  button: {
    flex: 1,
    height: 50,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: Vars.mainColor
  },
  linkContainer: {
    width: Vars.width - Vars.signinPaddingHorizontal,
    alignItems: "flex-end"
  },
  icon: {
    width: 20,
    height: 20
  },
  titleContainer: {
    flex: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "#fff"
  }
});
