import I18n from "./../languages/";

export default (countries = [
  {
    label: I18n.t("countries.morocco.label"),
    value: I18n.t("countries.morocco.value"),
    key: I18n.t("countries.morocco.key")
  },
  {
    label: I18n.t("countries.Algeria.label"),
    value: I18n.t("countries.Algeria.value"),
    key: I18n.t("countries.Algeria.key")
  },
  {
    label: I18n.t("countries.Somalia.label"),
    value: I18n.t("countries.Somalia.value"),
    key: I18n.t("countries.Somalia.key")
  },
  {
    label: I18n.t("countries.Comoros.label"),
    value: I18n.t("countries.Comoros.value"),
    key: I18n.t("countries.Comoros.key")
  },
  {
    label: I18n.t("countries.Djibouti.label"),
    value: I18n.t("countries.Djibouti.value"),
    key: I18n.t("countries.Djibouti.key")
  },
  {
    label: I18n.t("countries.Bahrain.label"),
    value: I18n.t("countries.Bahrain.value"),
    key: I18n.t("countries.Bahrain.key")
  },
  {
    label: I18n.t("countries.Egypt.label"),
    value: I18n.t("countries.Egypt.value"),
    key: I18n.t("countries.Egypt.key")
  },
  {
    label: I18n.t("countries.Yemen.label"),
    value: I18n.t("countries.Yemen.value"),
    key: I18n.t("countries.Yemen.key")
  },
  {
    label: I18n.t("countries.Iraq.label"),
    value: I18n.t("countries.Iraq.value"),
    key: I18n.t("countries.Iraq.key")
  },
  {
    label: I18n.t("countries.Jordan.label"),
    value: I18n.t("countries.Jordan.value"),
    key: I18n.t("countries.Jordan.key")
  },
  {
    label: I18n.t("countries.Kuwait.label"),
    value: I18n.t("countries.Kuwait.value"),
    key: I18n.t("countries.Kuwait.key")
  },
  {
    label: I18n.t("countries.Lebanon.label"),
    value: I18n.t("countries.Lebanon.value"),
    key: I18n.t("countries.Lebanon.key")
  },
  {
    label: I18n.t("countries.Libya.label"),
    value: I18n.t("countries.Libya.value"),
    key: I18n.t("countries.Libya.key")
  },
  {
    label: I18n.t("countries.Mauritania.label"),
    value: I18n.t("countries.Mauritania.value"),
    key: I18n.t("countries.Mauritania.key")
  },
  {
    label: I18n.t("countries.Oman.label"),
    value: I18n.t("countries.Oman.value"),
    key: I18n.t("countries.Oman.key")
  },
  {
    label: I18n.t("countries.Palestine.label"),
    value: I18n.t("countries.Palestine.value"),
    key: I18n.t("countries.Palestine.key")
  },
  {
    label: I18n.t("countries.Qatar.label"),
    value: I18n.t("countries.Qatar.value"),
    key: I18n.t("countries.Qatar.key")
  },
  {
    label: I18n.t("countries.SaudiArabia.label"),
    value: I18n.t("countries.SaudiArabia.value"),
    key: I18n.t("countries.SaudiArabia.key")
  },
  {
    label: I18n.t("countries.Sudan.label"),
    value: I18n.t("countries.Sudan.value"),
    key: I18n.t("countries.Sudan.key")
  },
  {
    label: I18n.t("countries.Syria.label"),
    value: I18n.t("countries.Syria.value"),
    key: I18n.t("countries.Syria.key")
  },
  {
    label: I18n.t("countries.Tunisia.label"),
    value: I18n.t("countries.Tunisia.value"),
    key: I18n.t("countries.Tunisia.key")
  },
  {
    label: I18n.t("countries.uae.label"),
    value: I18n.t("countries.uae.value"),
    key: I18n.t("countries.uae.key")
  },
]);
