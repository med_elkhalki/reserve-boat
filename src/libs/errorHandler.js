//import RootStore from 'stores/root.store';
import I18n from "./../languages";
import { Navigation } from "react-native-navigation";
import { LoginStack } from "./../navigation/stacks";
import { AlertIOS } from "react-native";
export default class ErrorsHandler {
  static async handleGenericErrors(error) {
    let dataErrors = new Map();
    if (error === "errors.auth.no_token_in_storage" || error === 'errors.auth.token_expired') {
      Navigation.setRoot(LoginStack());
      AlertIOS.alert(
        I18n.t("errors.errorTitle"),
        I18n.t('errors.tokenExpired'),
        [
          {
            text: I18n.t("errors.cancel"),
            onPress: () => {},
            style: 'cancel',
          },
        ]
      );
      return;
    } else if (error === "errors.network") {
      AlertIOS.alert(
        I18n.t("errors.errorTitle"),
        I18n.t('errors.network'),
        [
          {
            text: I18n.t("errors.cancel"),
            onPress: () => {},
            style: 'cancel',
          },
        ]
      );
      return {
        status: 0,
        dataErrors: dataErrors.set("network", error)
      };
    }
    switch (error.status) {
      case 422: {
        if (error["_bodyInit"]) {
          await error.json().then(data => {
            for (let key in data["errors"]) {
              dataErrors.set(key, data["errors"][key]["fieldErrors"][0]);
            }
          });
        }
        return { dataErrors, status: 422 };
      }
      // Too Many Attempts
      case 429: {
        return {
          status: 429,
          dataErrors: dataErrors.set("manyAttempt", "errors.manyAttempt")
        };
      }
      // Unauthorized
      case 401: {
        if (error["_bodyInit"]) {
          await error.json().then(data => {
            if(data && data.errors && data.errors.length && ['errors.auth.token_invalid', 'errors.auth.token_expired'].indexOf(data.errors[0].code) > -1) {
              Navigation.setRoot(LoginStack());
              AlertIOS.alert(
                I18n.t("errors.errorTitle"),
                I18n.t('errors.tokenExpired'),
                [
                  {
                    text: I18n.t("errors.cancel"),
                    onPress: () => {},
                    style: 'cancel',
                  },
                ]
              );
              return;
            }

            if (data["errors"] && data["errors"][0]) {
              if (
                data["errors"] &&
                data["errors"][0]["code"] === "errors.auth.token_expired"
              ) {
                //RootStore.setAppModeAndRootScreen('singleScreen', 'Login');
              }
              dataErrors.set("unauthorized", data["errors"][0]);
            }
          });
        }
        return { dataErrors, status: 401 };
      }
      // Forbidden
      case 403: {
        if (error["_bodyInit"]) {
          await error.json().then(data => {
            if (data["errors"] && data["errors"][0]) {
              dataErrors.set("forbidden", data["errors"][0]);
            }
          });
        }
        return { dataErrors, status: 403 };
      }
      // Unauthorized
      case 400: {
        if (error["_bodyInit"]) {
          await error.json().then(data => {
            if (data["errors"] && data["errors"][0]) {
              dataErrors.set("permission", data["errors"][0]);
            }
            if(data["result"]) {
              dataErrors.set("permission", data["result"]);
            }
          });
        }
        return { dataErrors, status: 400 };
      }
      // stripe errors
      case 501: {
        dataErrors.set("server_error", I18n.t("errors.error500"));
        return { dataErrors, status: 501 };
      }
      // Server Error
      case 500: {
        dataErrors.set("server_error", I18n.t("errors.error500"));
        return { dataErrors, status: 500 };
      }
      default:
        return false;
    }
  }
}
