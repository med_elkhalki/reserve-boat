//import I18n from 'myMatelas/i18n/';
import config from './../../config';
//import Upload from 'react-native-background-upload'
import vars from './vars';

const pathServer = config.serverUrl;
const apiPrefix = config.apiPrefix;
const storeKey = config.storeKey;
const host = pathServer + apiPrefix;
const deviceLocale = 'en';//I18n.locale;

export default class Api {
    static token;
    static locale = "fr";
    static headers() {
        return {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            dataType: 'json',
            mode: 'no-cors',
            locale: this.locale//'en'//I18n.locale
        };
    }
    static testStatus(data) {
        if(data.status === 0) {
            return {error: true, status: 501}
        } 
        return {error: false}
    }
    static setLocaleLanguage(lang) {
        this.locale = lang;
    }
    static get(route, params, requiresAuth) {
        return this.xhr(route, null, 'GET', requiresAuth);
    }
    static put(route, params, requiresAuth) {
        return this.xhr(route, params, 'PUT', requiresAuth);
    }
    static post(route, params, requiresAuth, blob) {
        return this.xhr(route, params, 'POST', requiresAuth, blob);
    }
    static delete(route, params, requiresAuth) {
        return this.xhr(route, params, 'DELETE', requiresAuth);
    }
    static xhr(route, params, verb, requiresAuth = false) {
        return new Promise((resolve, reject) => {
            const url = `${host}${route}`;
            const options = Object.assign(
                { method: verb }, params ? { body: JSON.stringify(params) } : null
            );
            options.headers = Api.headers();
            if (requiresAuth) {
                if (this.token) {
                  options.headers.Authorization = `Bearer ${this.token}`;
                    this.fetch(url, options).then((resp) => {
                      resolve(resp);
                    }).catch((err) => {
                        reject(err);
                    });
                } else {
                    reject('errors.auth.no_token_in_storage');
                }
            } else {
                this.fetch(url, options).then((resp) => {
                    resolve(resp);
                }).catch((err) => {
                    reject(err);
                });
            }
        });
    }
    static fetch(url, options) {
        return new Promise((resolve, reject) => {
            console.log('ASYNCSTORAGETESTTOKEN3',url)
            fetch(url, options).then((response) => {
                if (response.ok) {
                    const json = response.json();
                    json.then((data) => {
                        if (response.ok) {
                            resolve(data);
                        } else {
                            reject(data);
                        }
                    }).catch(() => {
                        reject(response);
                    });
                } else {
                    // response.json().then((data) => {
                    //     reject(data)
                    // })
                    reject(response);
                }
            }).catch(() => {
                reject('errors.network');
            });
        });
    }

    static upload(route, pathToFile, requiresAuth) {
        // return new Promise((resolve, reject) => {
        //     let finalPath = pathToFile;
        //     if (vars.isAndroid) {
        //         finalPath = pathToFile.replace(/^(file:\/\/)/, '')
        //     } else {
        //         finalPath = pathToFile.indexOf('file://') === -1 ? ('file://' + pathToFile) : pathToFile;
        //     }
        //     let options = {
        //         url: host + route,
        //         path: finalPath,
        //         method: 'POST',
        //         headers: {
        //             'Locale': deviceLocale
        //         },
        //         notification: {
        //             enabled: false
        //         }
        //     };
        //     if (requiresAuth) {
        //         if (Api.token) {
        //             options.headers['Authorization'] = "Bearer " + Api.token;
        //             this.performUpload(options, resolve, reject);
        //         } else {
        //             reject('errors.auth.no_token_in_storage');
        //         }
        //     } else {
        //         this.performUpload(options, resolve, reject);
        //     }
        // });
    }

    static uploadPromise(uri, path) {
        // return new Promise((resolve, reject) => {
        //     Api.upload(uri, path, true).then((uploadId) => {
        //         Upload.addListener('progress', uploadId, (data) => {
        //             console.log(`Progress: ${data.progress}%`);
        //         });
        //         Upload.addListener('error', uploadId, (data) => {
        //             console.log(`Error: ${data.error}%`)
        //         });
        //         Upload.addListener('cancelled', uploadId, (data) => {
        //             console.log(`Cancelled!`)
        //         });
        //         Upload.addListener('completed', uploadId, (data) => {
        //             resolve(data);
        //         });
        //     }).catch((err) => {
        //         reject(err);
        //     })
        // })
    }

    static performUpload(options, resolve, reject) {
        // console.log(options);
        // Upload.startUpload(options).then((uploadId) => {
        //     console.log('Upload started');
        //     // resolve with the upload ID to be used to subscribe to upload events
        //     resolve(uploadId);
        // }).catch((err) => {
        //     console.log('Upload error!', err);
        //     reject(err);
        // })
    }

}