import { Dimensions, Platform, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
  screenPrefix: 'boats.',
  width,
  height,
  signinPaddingHorizontal: 50,
  mainColor: '#43a0d9'
}