export default {
  signin: {
    phone: "Telephone",
    password: "Mot de passe",
    forgotPassword: "vous avez déjà un compte ?",
    go: "S'identifier",
    signup: "Créer un compte",
    select: "Pays +",
    email: "E-mail",
    selectTitle: "Selectionner"
  },
  signup: {
    username: "Nom",
    phone: "Téléphone",
    password: "Mot de passe",
    confirm: "Confirmer votre mot de passe",
    save: "INSCRIPTION",
    user: "Client",
    admin: "Fournisseur de service",
    email: "E-mail",
  },
  home: {
    title: "Accueil",
    picnicBoats: "Bateaux pique-nique",
    transportBoats: "bateau de transport",
    fishingBoats: "Bateaux de pêche",
    boats: "Croisière",
    myBoats: "Mes Bateaux",
    boatsMaintenance: "Vente et maintenance de bateaux",
    boatsDiving: "Stage de plongée et voyage de plongée"
  },
  menu: {
    title: "Menu",
    home: "Accueil",
    myReservation: "Mes réservations",
    infos: "confidentialité légale",
    settings: "Les paramètres",
    contactUs: "Contacter Nous",
    logout: "Déconnexion",
    purchasesList: "Mes achats"
  },
  profileSettings: {
    title: "Modifier des données personnelles",
    name: "Nom",
    namePlaceholder: "Nom d'utilisateur",
    identiy: "Identité / Résidence",
    identiyPlaceholder: "identité / résidence",
    phone: "Téléphone",
    phonePlaceholder: "0500000000",
    email: "Email",
    emailPlaceholder: "user@gmail.com",
    password: "Mot de passe",
    confirmPassword: "Confirmez votre mot de passe",
    save: "Enregister"
  },
  contactUs: {
    title: "Contactez-nous",
    name: "Nom",
    namePlaceholder: "Nom d'utilisateur",
    email: "Email",
    emailPlaceholder: "user@gmail.com",
    phone: "Téléphone",
    phonePlaceholder: "0500000000",
    boatType: "Type de véhicule",
    message: "Laissez votre message",
    messagePlaceholder: "message",
    send: "Envoyer"
  },
  addBoat: {
    title: "Ajouter un bateau",
    name: "Nom",
    namePlaceholder: "bateau esallam",
    activity: "activité",
    activityPlaceholder: "transport de passagers",
    capacity: "Charge",
    capacityPlaceholder: "150 passagers",
    price: "Prix par passager",
    pricePlaceHolder: "50 euros",
    details: "Détails",
    detailsPlaceholder: "message",
    direction: "Destination",
    directionFrom: "de",
    directionTo: "a",
    fix: "Dates de maintenance",
    fixFrom: "de",
    fixTo: "a",
    save: "Enregister",
    marque: "Marque",
    year: "Annee",
    width: "Largeur",
    height: "Longueur",
    Carburant: "Carburant",
    Motorisation: "Motorisation",
  },
  boatDetails: {
    title: "Détails du bateau",
    price: "50 pour personne",
    boatTitle: "bateau xxx",
    boatSubTitle:
      "Transport de passagers pour le divertissement de Jizan aux Chevaliers",
    reserveNow: "Reserver",
    inMaintenance: "En maintenance",
    editBoat: "Modifier",
    addBoat: "Ajouter",
    boatWeight: "Places dans bateau",
    boatPhone: "Téléphone",
    boatOwner: "Propriétaire",
    boateRating: "Évaluer le bateau",
    tripRating: "Évaluer la sortie",
    leavedCountry: "départ",
    arrivedCountry: "l'arrivée",
    city: "Ville",
    marque: "Marque",
    year: "Annee",
    width: "Largeur",
    height: "Longueur",
    Carburant: "Carburant",
    Motorisation: "Motorisation",
    Description: "Description"
  },
  changePassword: {
    title: "Récupérer le numéro secret",
    description: "Veuillez entrer votre nouveau mot de passe",
    newPassword: "Nouveau mot de passe",
    confirmPassword: "Confirmez votre mot de passe",
    confirmation: "Confirmation"
  },
  searchBoats: {
    title: "Rechercher un bateau",
    inputSearchPlaceholder: "Rechercher",
    more: "Plus",
    from: "De",
    to: "À",
    boatName: "Nom du Bateau",
    pointA: "xxx",
    pointB: "xxx"
  },
  resetPassword: {
    title: "Récupérer le numéro secret",
    description: "Veuillez envoyer le code via",
    or: "Ou",
    phone: "Numéro de portable",
    email: "Email",
    send: "Envoyer"
  },
  reservation: {
    title: "Reservation",
    username: "Username",
    usernamePlaceHolder: "Username",
    cin: "Cin",
    cinplaceHolder: "Cin",
    trip: "Tip",
    from: "From",
    to: "To",
    bankAccount: "Bank Account",
    bankAccountPlaceHolder: "4242424242424242",
    price: "Price",
    pricePlaceHolder: "Price",
    date: "Date Reservation",
    datePlaceHolder: "##/##/####",
    time: "Time",
    timePlaceHolder: "##:##:##",
    birthday: "BirthDay",
    birthdayPlaceHolder: "##/##/####",
    phone: "Phone",
    phonePlaceHolder: "#############"
  },
  reservationList: {
    title: "Mes réservations",
    waiting: "en attente",
    fromHour: "de",
    from: "de",
    boatName: "bateau xxx",
    numberOfPerson: "Nombre d'individus"
  },
  reserveBoat: {
    title: "Réserver un bateau",
    name: "Nom d'utilisateur",
    namePlaceholder: "###########",
    identity: "Numéro d'identification / de résidence",
    identityPlaceholder: "###########",
    journey: "Voyage",
    journeyA: "de xxx",
    journeyB: "a yyy",
    numberOfPerson: "Nombre d'individus",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "Compte bancaire",
    bankAccountPlaceholder: "###########",
    totalPrice: "Somme totale",
    totalPricePlaceholder: "###########",
    reservDate: "Date de réservation",
    reservDatePlaceholder: "###########",
    reservTime: "Temps de réservation",
    reservTimePlaceholder: "###########",
    birthDate: "Date de naissance",
    birthDatePlaceholder: "###########",
    phone: "Numéro de portable",
    phonePlaceholder: "###########",
    reserveNow: "Réservez maintenant"
  },
  updateReserveBoat: {
    title: "Réserver un bateau",
    name: "Nom d'utilisateur",
    namePlaceholder: "###########",
    identity: "Numéro d'identification / de résidence",
    identityPlaceholder: "###########",
    numberOfPerson: "Nombre d'individus",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "Compte bancaire",
    bankAccountPlaceholder: "###########",
    totalPrice: "Somme totale",
    totalPricePlaceholder: "###########",
    reservDate: "Date de réservation",
    reservDatePlaceholder: "###########",
    reservTime: "Temps de réservation",
    reservTimePlaceholder: "###########",
    birthDate: "Date de naissance",
    birthDatePlaceholder: "###########",
    phone: "Numéro de portable",
    phonePlaceholder: "###########",
    reserveNow: "Réservez maintenant",
    accept: "Accepter",
    reject: "Refuser"
  },
  rateBoat: {
    description_1: "S'il vous plaît évaluer la propreté du bateau\n",
    description_2: "Exactitude des rendez-vous",
    description_3: "Crédibilité du propriétaire du bateau",
    validate: "Envoyer"
  },
  reservationError: {
    description: "Sorry. Please complete registration information",
    goToSettings: "Aller dans les paramètres"
  },
  validateCode: {
    title: "Code de confirmation",
    description:
      "Veuillez entrer votre code de confirmation pour vérifier votre compte",
      code: "Code",
    btnSend: "Envoyer"
  },
  notifications: {
    title: "Notifications",
    description: "Nous avons réservé avec succès. Attendez la permission",
    from: "De",
    to: "À",
    pointA: "xxxx",
    pointB: "yyyy"
  },
  declaration: {
    title: "Déclaration",
    agree: "OK",
    messageTitle: "Autorisation du propriétaire du bateau",
    message:
      "Ce texte est un exemple de texte qui peut être remplacé dans le même espace, a été donné que ce texte est un exemple de texte peut être remplacé dans le même espace, a été donné ce texte est un exemple de texte peut être remplacé dans le même espace"
  },
  profile: {
    title: "",
    username: "",
    usernamePlaceHolder: "",
    cin: "",
    cinplaceHolder: "",
    trip: "",
    from: "",
    to: "",
    bankAccount: "",
    bankAccountPlaceHolder: "",
    price: "",
    pricePlaceHolder: "",
    birthday: "",
    birthdayPlaceHolder: "",
    phone: "",
    phonePlaceHolder: "",
    email: "",
    emailPlaceHolder: ""
  },
  countries: {
    morocco: {
      label: "+212 Morocco",
      value: "+212",
      key: "+212"
    },
    Iraq: {
      label: "+964 Iraq",
      value: "+964",
      key: "+964"
    },
    uae: {
      label: "+971 United Arab Emirates",
      value: "+971",
      key: "+971"
    },
    Somalia: {
      label: "+252 Somalia",
      value: "+252",
      key: "+252"
    },
    SaudiArabia: {
      label: "+966 Saudi Arabia",
      value: "+966",
      key: "+966"
    },
    Sudan: {
      label: "+249 Sudan",
      value: "+249",
      key: "+249"
    },
    Algeria: {
      label: "+213 Algeria",
      value: "+213",
      key: "+213"
    },
    Bahrain: {
      label: "+973 Bahrain",
      value: "+973",
      key: "+973"
    },
    Jordan: {
      label: "+962 Jordan",
      value: "+962",
      key: "+962"
    },
    Yemen: {
      label: "+967 Yemen",
      value: "+967",
      key: "+967"
    },
    Kuwait: {
      label: "+965 Kuwait",
      value: "+965",
      key: "+965"
    },
    Mauritania: {
      label: "+222 Mauritania",
      value: "+222",
      key: "+222"
    },
    Egypt: {
      label: "+20 Egypt",
      value: "+20",
      key: "+20"
    },
    Qatar: {
      label: "+971 Qatar",
      value: "+971",
      key: "+971"
    },
    Libya: {
      label: "+218 Libya",
      value: "+218",
      key: "+218"
    },
    Palestine: {
      label: "+970 Palestine",
      value: "+970",
      key: "+970"
    },
    Lebanon: {
      label: "+961 Lebanon",
      value: "+961",
      key: "+961"
    },
    Oman: {
      label: "+968 Oman",
      value: "+968",
      key: "+968"
    },
    Syria: {
      label: "+963 Syria",
      value: "+963",
      key: "+963"
    },
    Djibouti: {
      label: "+253 Djibouti",
      value: "+253",
      key: "+253"
    },
    Comoros: {
      label: "+269 Comoros",
      value: "+269",
      key: "+269"
    },
    Tunisia: {
      label: "+216 Tunisia",
      value: "+216",
      key: "+216"
    }
  },
  success: {
    contactUs: {
      messageTitle: "l'envoie",
      message: "votre message a bien été envoyé "
    },
    reservBoat: {
      messageTitle: "l'envoie",
      message: "votre message a bien été envoyé "
    }
  },
  errors: {
    login: {
      requiredTitle: "Ereur",
      requiredMessage:
        "Numero de telephone, code et le mode de pass sont obligatoire",
      cancel: "Annuler"
    },
    contactUs: {
      requiredTitle: "Ereur",
      requiredMessage:
        "Numero de telephone, nom, email et le message sont obligatoire",
      cancel: "Annuler"
    },
    reservBoat: {
      requiredTitle: "Erreur",
      requiredMessage:
        "tous les informatios sont obligatoire",
      cancel: "Annuler"
    },
      changePassword: {
          errorTitle: "Erreur",
          message: "Votre mot de passe et votre mot de passe de confirmation ne correspondent pas",
          cancel: "Annuler"
      },
    error500: "Nous avons rencontré un problème",
    errorTitle: "Ereur",
    cancel: "Annuler",
    tokenExpired: "Votre Token va bientôt expirer",
    InvalidEntry: "Champ obligatoire",
    network: "le réseau ne fonctionne pas"
  },
  emptyState: {
    empty: "données vides",
    refresh: "rafraîchir"
  },
  policy: {
    aboutUs: {
      title: "À propos de l'application des bateaux Qwereb",
      body: "L'application des premiers bateaux d'application en Arabie Saoudite, qui intègre les technologies modernes aux services de transport maritime. L'application fournit des informations intégrées pour des centaines de modes d'expédition, ferries, bateaux de pêche, bateaux de pique-nique, bateaux de transport. Plus besoin de temps et d’arrangement pour obtenir le service souhaité: recherchez le bateau ou le ferry où vous souhaitez sélectionner la destination et la destination et laissez le reste s’appliquer aux bateaux.Quel que soit votre plan, transporter des marchandises, Boats est l'application idéale et la plus simple pour organiser vos réservations."
    },
    humanIsAllforUs: {
      title: "L'homme est au centre de tout",
      body: ""
    },
    ourVision: {
      title: "Vision et mission",
      body: "À travers notre travail, nous attendons avec impatience les valeurs que nous recherchons: partenariat - honnêteté - perfection - transparence - service humain - développement - et plaçons le client au cœur des événements pour tout ce que nous faisons. Nous attirons, développons et conservons les meilleurs ingrédients et promouvons une culture globale. Travailler conformément aux normes professionnelles et éthiques, développer et promouvoir un tourisme maritime qui réponde aux besoins du client, créer des avantages concurrentiels et marketing et sensibiliser davantage le public aux aspects du tourisme et du tourisme maritime."
    },
    goals: {
      title: "Notre but",
      body: "Notre objectif est de devenir l'une des premières et principales applications dans le domaine de la fourniture de services de tourisme maritime en Arabie saoudite et à l'étranger en proposant des offres qui répondent aux besoins de nos clients - et de fournir des avantages supplémentaires à nos clients dans l'ensemble du Royaume d'Arabie saoudite, ainsi que d'établir et de maintenir cette relation de manière continue et durable."
    },
    conditions: {
      title: "",
      body: ""
    }
  }
};
