export default {
  signin: {
    phone: "الهاتف",
    password: "كلمة المرور",
    forgotPassword: "هل لديك حساب بالفعل ؟",
    go: "دخول",
    signup: "تسجيل جديد",
    select: "الدولة +",
    email: "الإيميل",
    selectTitle: "إختر"
  },
  signup: {
    username: "الإسم",
    phone: "رقم الجوال",
    password: "كلمة المرور",
    confirm: "تأكيد كلمة المرور",
    save: "تسجيل",
    user: "طالب خدمه",
    admin: "مقدم خدمه",
    email: "الإيميل"
  },
  home: {
    title: "الرئيسية",
    picnicBoats: "قوارب نزهة",
    transportBoats: "قوارب نقل",
    fishingBoats: "قوارب صيد",
    boats: "عبارات",
    boatsMaintenance: "بيع و صيانة قوارب",
    boatsDiving: "تدريب غوص ورحلة غوص"
  },
  menu: {
    title: "القائمة",
    home: "الرئيسية",
    myReservation: "حجوزاتي",
    infos: "سياسة التطبيق",
    settings: "اعدادت الحساب",
    contactUs: "اتصل بنا",
    logout: "خروج",
    myBoats: "القوارب الخاصة بى",
    purchasesList: "مشترياتي"
  },
  profileSettings: {
    title: "تعديل البيانات الشخصية",
    name: "الإسم",
    namePlaceholder: "اسم المستخدم",
    identiy: "الهوية / الإقامة",
    identiyPlaceholder: "الهوية / الإقامة",
    phone: "الهاتف",
    phonePlaceholder: "0500000000",
    email: "البريد",
    emailPlaceholder: "user@gmail.com",
    password: "كلمة المرور",
    confirmPassword: "تأكيد كلمة المرور",
    save: "حفظ"
  },
  contactUs: {
    title: "أتصل بنا",
    name: "الإسم",
    namePlaceholder: "اسم المستخدم",
    email: "الإيميل",
    emailPlaceholder: "user@gmail.com",
    phone: "الجوال",
    phonePlaceholder: "0500000000",
    boatType: "نوع المركبة",
    message: "أترك رسالتك",
    messagePlaceholder: "رسالة",
    send: "إرسال"
  },
  addBoat: {
    title: "اضف قارب",
    name: "الإسم",
    namePlaceholder: "قارب السلام",
    activity: "النشاط",
    activityPlaceholder: "نقل ركاب",
    capacity: "الحمولة",
    capacityPlaceholder: "150 راكب",
    price: "سعر الفرد",
    pricePlaceHolder: "ر.س 50",
    details: "التفاصيل",
    detailsPlaceholder: "رسالة",
    direction: "الوجهة",
    directionFrom: "من",
    directionTo: "إلى",
    fix: "مواعيد الصيانة",
    fixFrom: "من",
    fixTo: "الى",
    save: "حفظ",
    marque: "علامة",
    year: "العام",
    width: "العرض",
    height: "الطول",
    Carburant: "نوع الوقود",
    Motorisation: "نوع المحرك"
  },
  boatDetails: {
    title: "صفحة القارب",
    price: "50 ر.س للفرد",
    boatTitle: "قارب فارس السلام",
    boatSubTitle: "نقل ركاب بغرض الترفية من جازان الى فرسان copy",
    reserveNow: "أحجز الأن",
    inMaintenance: "في الصيانة",
    editBoat: "تعديل",
    addBoat: "أضف قارب",
    boatWeight: "حمولة القارب",
    boatPhone: "رقم الجوال",
    boatOwner: "المالك",
    boateRating: "تقييم القارب",
    tripRating: "تقييم الرحلة",
    leavedCountry: "مغادرة",
    arrivedCountry: "وصول",
    city: "مدينة",
    marque: "علامة",
    year: "العام",
    width: "العرض",
    height: "الطول",
    Carburant: "نوع الوقود",
    Motorisation: "نوع المحرك",
    Description: "تفاصيل"
  },
  changePassword: {
    title: "إسترجاع الرقم السرى",
    description: "برجاء إدخال كلمة المرور الجديدة",
    newPassword: "كلمة المرور الجديدة",
    confirmPassword: "تأكيد كلمة المرور",
    confirmation: "تأكيد"
  },
  searchBoats: {
    title: "بحث عن قارب",
    inputSearchPlaceholder: "بحث",
    more: "المزيد",
    from: "من",
    to: "الى",
    boatName: "إسم القارب",
    pointA: "فرسان",
    pointB: "جازان"
  },
  resetPassword: {
    title: "إسترجاع الرقم السرى",
    description: "برجاء ارسال الكود عن طريق",
    or: "او",
    phone: "رقم الجوال",
    email: "البريد الإلكترونى",
    send: "إرسال"
  },
  reservation: {
    title: "",
    username: "",
    usernamePlaceHolder: "",
    cin: "",
    cinplaceHolder: "",
    trip: "",
    from: "",
    to: "",
    bankAccount: "",
    bankAccountPlaceHolder: "",
    price: "",
    pricePlaceHolder: "",
    date: "",
    datePlaceHolder: "",
    time: "",
    timePlaceHolder: "",
    birthday: "",
    birthdayPlaceHolder: "",
    phone: "",
    phonePlaceHolder: ""
  },
  reservationList: {
    title: "حجوزاتى",
    waiting: "قيد الإنتظار",
    fromHour: "من الساعة ",
    from: "من",
    boatName: "قارب فارس السلام",
    numberOfPerson: "عدد الأفراد"
  },
  reserveBoat: {
    title: "حجز قارب",
    name: "اسم المستخدم",
    namePlaceholder: "الإسم رباعى",
    identity: "رقم الهوية / الاقامة",
    identityPlaceholder: "###########",
    journey: "الرحلة",
    journeyA: "من جازان",
    journeyB: "إلى فرسان",
    numberOfPerson: "عدد الافراد",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "الحساب البنكى",
    bankAccountPlaceholder: "###########",
    totalPrice: "المبلغ الكلى",
    totalPricePlaceholder: "###########",
    reservDate: "تاريخ الحجز",
    reservDatePlaceholder: "###########",
    reservTime: "وقت الحجز",
    reservTimePlaceholder: "###########",
    birthDate: "تاريخ الميلاد",
    birthDatePlaceholder: "###########",
    phone: "رقم الجوال",
    phonePlaceholder: "###########",
    reserveNow: "احجز الأن"
  },
  updateReserveBoat: {
    title: "حجز قارب",
    name: "الإسم رباعى",
    namePlaceholder: "###########",
    identity: "رقم الهوية / الاقامة",
    identityPlaceholder: "###########",
    numberOfPerson: "عدد الافراد",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "الحساب البنكى",
    bankAccountPlaceholder: "###########",
    totalPrice: "المبلغ الكلى",
    totalPricePlaceholder: "###########",
    reservDate: "تاريخ الحجز",
    reservDatePlaceholder: "###########",
    reservTime: "وقت الحجز",
    reservTimePlaceholder: "###########",
    birthDate: "تاريخ الميلاد",
    birthDatePlaceholder: "###########",
    phone: "رقم الجوال",
    phonePlaceholder: "###########",
    reserveNow: "احجز الأن",
    accept: "قبول",
    reject: "رفض"
  },
  rateBoat: {
    description_1: "من فضلك قم بتقييم نضافة القارب",
    description_2: "دقة المواعيد",
    description_3: "مصداقية صاحب القارب",
    validate: "إرسال"
  },
  reservationError: {
    description: "نأسف . برجاء إكمال بيانات التسجيل",
    goToSettings: "الذهاب إلى الإعدادات"
  },
  validateCode: {
    title: "كود التأكيد",
    description: "برجاء إدخال كود التأكيد للتحقق من حسابك",
    btnSend: "إرسال"
  },
  notifications: {
    title: "الإشعارات",
    description: "لقد تم الحجز بنجاح . انتظر التصريح",
    from: "من",
    to: "الى",
    pointA: "فرسان",
    pointB: "جازان"
  },
  declaration: {
    title: "بيان",
    agree: "موافق",
    messageTitle: "التصريح الخاص بصاحب القارب",
    message:
      "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم تولي  هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم تولي هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم تولي"
  },
  profile: {
    title: "",
    username: "",
    usernamePlaceHolder: "",
    cin: "",
    cinplaceHolder: "",
    trip: "",
    from: "",
    to: "",
    bankAccount: "",
    bankAccountPlaceHolder: "",
    price: "",
    pricePlaceHolder: "",
    birthday: "",
    birthdayPlaceHolder: "",
    phone: "",
    phonePlaceHolder: "",
    email: "",
    emailPlaceHolder: ""
  },
  countries: {
    morocco: {
      label: "+212 المغرب",
      value: "+212",
      key: "+212"
    },

    Iraq: {
      label: "+964 العراق",
      value: "+964",
      key: "+964"
    },
    uae: {
      label: "+971 الإمارات",
      value: "+971",
      key: "+971"
    },
    Somalia: {
      label: "+252 الصومال",
      value: "+252",
      key: "+252"
    },
    SaudiArabia: {
      label: "+966 السعودية",
      value: "+966",
      key: "+966"
    },
    Sudan: {
      label: "+249 السودان",
      value: "+249",
      key: "+249"
    },
    Algeria: {
      label: "+213 الجزائر",
      value: "+213",
      key: "+213"
    },
    Bahrain: {
      label: "+973 البحرين",
      value: "+973",
      key: "+973"
    },
    Jordan: {
      label: "+962 الأردن",
      value: "+962",
      key: "+962"
    },
    Yemen: {
      label: "+967 اليمن",
      value: "+967",
      key: "+967"
    },
    Kuwait: {
      label: "+965 الكويت",
      value: "+965",
      key: "+965"
    },
    Mauritania: {
      label: "+222 موريتانيا",
      value: "+222",
      key: "+222"
    },
    Egypt: {
      label: "+20 مصر",
      value: "+20",
      key: "+20"
    },
    Qatar: {
      label: "+971 قطر",
      value: "+971",
      key: "+971"
    },
    Libya: {
      label: "+218 ليبيا",
      value: "+218",
      key: "+218"
    },
    Palestine: {
      label: "+970 فلسطين",
      value: "+970",
      key: "+970"
    },
    Lebanon: {
      label: "+961 لبنان",
      value: "+961",
      key: "+961"
    },
    Oman: {
      label: "+968 عمان",
      value: "+968",
      key: "+968"
    },
    Syria: {
      label: "+963 سوريا",
      value: "+963",
      key: "+963"
    },
    Djibouti: {
      label: "+253 جيبوتي",
      value: "+253",
      key: "+253"
    },
    Comoros: {
      label: "+269 جزر القمر",
      value: "+269",
      key: "+269"
    },
    Tunisia: {
      label: "+216 تونس",
      value: "+216",
      key: "+216"
    }
  },
  success: {
    contactUs: {
      messageTitle: "إرسال",
      message: "لقد تم إرسال طلبكم بنجاح"
    },
    reservBoat: {
      messageTitle: "إرسال",
      message: "لقد تم إرسال طلبكم بنجاح"
    }
  },
  errors: {
    login: {
      requiredTitle: "خطأ",
      requiredMessage:
        " يتوجب عليك الإدلاء بكل من الهاتف ، كلمة المرور و رمز البلد من أجل الولوج ",
      cancel: "إلغاء"
    },
    contactUs: {
      requiredTitle: "خطأ",
      requiredMessage:
        " يتوجب عليك الإدلاء بكل من الهاتف ،الإسم ، الإميل و الرسالة من أجل إكمال الطلب ",
      cancel: "إلغاء"
    },
    reservBoat: {
      requiredTitle: "خطأ",
      requiredMessage: " يتوجب عليك الإدلاء بكل المعلومات من أجل إكمال الطلب ",
      cancel: "إلغاء"
    },
    changePassword: {
      errorTitle: "خطأ",
      message: "كلمة المرور وتأكيد كلمة المرور غير متطابقتين",
      cancel: "إلغاء"
    },
    error500: "عذرا، نحن نواجه خطأ في التطبيق حاول لاحقا",
    errorTitle: "خطأ",
    cancel: "إلغاء",
    tokenExpired: "لقد انتهت مدة صلاحية تسجيلكم في تطبيق يرجى إعادة التسجيل",
    InvalidEntry: "مطلوب",
    network: "أنت لا تتوفر على شبكة نت"
  },
  emptyState: {
    empty: "بيانات فارغة",
    refresh: "تحديث"
  },
  policy: {
    aboutUs: {
      title: " Qwareb نبذه عن تطبيق قوارب",
      body: "يعد تطبيق قوارب التطبيق الأول بالمملكة العربية السعودية الذي يدمج التقنيات الحديثة مع خدمات النقل البحري . يقدم التطبيق معلومات متكاملة لمئات من وسائل النقل البحري، العبارات ، قوارب صيد ، قوارب نزهة ، قوارب نقل. لا حاجة لمزيد من الوقت والترتيب للحصول علي الخدمة التي تريد فقط قم بالبحث عن القارب او العبارة التي تريد حدد الجهة التي ستنطلق منها ، والجهة المراد الوصول اليها ، ودع الباقي لتطبيق قوارب .ايا كانت خطتك نقل بضائع ، تنزه ، صيد ، تطبيق قوارب هو التطبيق الأمثل والأسهل لترتيب حجوزاتك"
    },
    humanIsAllforUs: {
      title: "الانسان هو محور كل شىء",
      body: "ونتطلع من من خلال عملنا للقيم التي نسعى لها :الشراكة - الأمانة - الإتقان - الشفافية - خدمة الإنسان - التنمية - وأن نضع العميل في قلب الأحداث لكل شئ نقوم به وان نجذب ونطور ونحتفظ بأفضل المقومات وان نعزز لثقافة شاملة بالرحلات البحرية التي تترتقي بالتنوع وأن ندير العمل وفقا للمعايير المهنية والأخلاقية وان نطور ونسوق للسياحة البحرية التي تلبي احتياجات العميل وخلق الميزات التنافسية والتسوقية وان نعزز وعي الجمهور بجوانب الرحلات والسياحة البحريةكعنصر إجتماعي وإقتصادي واعد"
    },
    ourVision: {
      title: "الرؤيا والرساله",
      body: "وتحقيقا لرؤية المملكة العربية السعودية 2030 في التطوير والتنمية وخدمة الوطن وابناء الوطن والالتفات للسياحية البحرية وتنشيطها وتنظيمها وجدولتها والاستفادة من الرحلات البحرية كعنصر اساسي والوقوف علي جمال شواطئنا واطلالاتنا البحرية علي مستوي المملكه العربية السعودية والشرق الاوسط"
    },
    goals: {
      title: "هدفنا",
      body: "هدفنا ان نصبح أحد أول وأهم التطبيقات الرائدة في مجال تقديم الخدمات السياحة البحرية في المملكه العربية السعودية والخارج من خلال توفيرالعروض التي تلبي إحتياجات عملاءنا--وأن نوفر المميزات الإضافية لعملاءنا في جميع أنحاء المملكة العربية السعودية وكذلك التأسيس والإحتفاظ بتلك العلاقة المستمرة والمستدامة"
    },
    conditions: {
      title: "الشروط والأحكام",
      body: "ونتطلع من من خلال عملنا للقيم التي نسعى لها :الشراكة - الأمانة - الإتقان - الشفافية - خدمة الإنسان - التنمية - وأن نضع العميل في قلب الأحداث لكل شئ نقوم به وان نجذب ونطور ونحتفظ بأفضل المقومات وان نعزز لثقافة شاملة بالرحلات البحرية التي تترتقي بالتنوع وأن ندير العمل وفقا للمعايير المهنية والأخلاقية وان نطور ونسوق للسياحة البحرية التي تلبي احتياجات العميل وخلق الميزات التنافسية والتسوقية وان نعزز وعي الجمهور بجوانب الرحلات والسياحة البحريةكعنصر إجتماعي وإقتصادي واعد"
    }
  }
};
