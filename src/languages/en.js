export default {
  signin: {
    phone: "Phone",
    password: "Password",
    forgotPassword: "Already have an account ?",
    go: "Login",
    signup: "Create account",
    select: "Country +",
    email: "E-mail",
    selectTitle: "choose"
  },
  signup: {
    username: "Name",
    phone: "Phone number",
    password: "Password",
    confirm: "Confirm your password",
    save: "REGISTRATION",
    user: "Customer",
    admin: "Service Provider",
    email: "E-mail"
  },
  home: {
    title: "Home",
    picnicBoats: "Picnic boats",
    transportBoats: "Transport boats",
    fishingBoats: "Fishing boats",
    boats: "Boats",
    boatsMaintenance: "Sale and maintenance of boats",
    boatsDiving: "Diving training and diving trip"
  },
  menu: {
    title: "Menu",
    home: "Home",
    myReservation: "My Reservation",
    infos: "Legal privacy",
    settings: "Settings",
    contactUs: "Contact Us",
    logout: "Logout",
    myBoats: "My Boats",
    purchasesList: "My purchases"
  },
  profileSettings: {
    title: "Edit personal data",
    name: "Name",
    namePlaceholder: "Username",
    identiy: "Identity / Residence",
    identiyPlaceholder: "identity / residence",
    phone: "Phone",
    phonePlaceholder: "0500000000",
    email: "Email",
    emailPlaceholder: "user@gmail.com",
    password: "Password",
    confirmPassword: "Confirm password",
    save: "Save"
  },
  contactUs: {
    title: "contact us",
    name: "Name",
    namePlaceholder: "username",
    email: "Email",
    emailPlaceholder: "user@gmail.com",
    phone: "phone",
    phonePlaceholder: "0500000000",
    boatType: "Type of the vehicle",
    message: "Leave your message",
    messagePlaceholder: "message",
    send: "Send"
  },
  addBoat: {
    title: "Add a boat",
    name: "Name",
    namePlaceholder: "boat",
    activity: "Activity",
    activityPlaceholder: "Transportation of passengers",
    capacity: "Load",
    capacityPlaceholder: "150 passengers",
    price: "Price per capita",
    pricePlaceHolder: "50 euros",
    details: "Details",
    detailsPlaceholder: "message",
    direction: "Destination",
    directionFrom: "from",
    directionTo: "to",
    fix: "Maintenance dates",
    fixFrom: "from",
    fixTo: "from",
    save: "save",
    marque: "Brand",
    year: "year",
    width: "width",
    height: "height",
    Carburant: "Fuel",
    Motorisation: "Motorization"
  },
  boatDetails: {
    title: "Boat details",
    price: "50 for person",
    boatTitle: "boat xxx",
    boatSubTitle:
      "Transport passengers for entertainment from Jizan to Knights",
    reserveNow: "Book now",
    inMaintenance: "In maintenance",
    editBoat: "Modification",
    addBoat: "Add a boat",
    boatWeight: "The boat load",
    boatPhone: "Mobile number",
    boatOwner: "Owner",
    boateRating: "Evaluate the boat",
    tripRating: "Evaluate the trip",
    leavedCountry: "leaving",
    arrivedCountry: "arrival",
    city: "City",
    marque: "Brand",
    year: "year",
    width: "width",
    height: "height",
    Carburant: "Fuel",
    Motorisation: "Motorization",
    Description: "Description"
  },
  changePassword: {
    title: "Retrieving the secret number",
    description: "Please enter your new password",
    newPassword: "new password",
    confirmPassword: "confirm password",
    confirmation: "Confirmation"
  },
  searchBoats: {
    title: "Search for a boat",
    inputSearchPlaceholder: "Search",
    more: "More",
    from: "From",
    to: "To",
    boatName: "Boat name",
    pointA: "xxx",
    pointB: "xxx"
  },
  resetPassword: {
    title: "Retrieving the secret number",
    description: "Please send the code via",
    or: "Or",
    phone: "Mobile number",
    email: "E-mail",
    send: "Send"
  },
  reservation: {
    title: "Reservation",
    username: "Username",
    usernamePlaceHolder: "Username",
    cin: "Cin",
    cinplaceHolder: "Cin",
    trip: "Tip",
    from: "From",
    to: "To",
    bankAccount: "Bank Account",
    bankAccountPlaceHolder: "4242424242424242",
    price: "Price",
    pricePlaceHolder: "Price",
    date: "Date Reservation",
    datePlaceHolder: "##/##/####",
    time: "Time",
    timePlaceHolder: "##:##:##",
    birthday: "BirthDay",
    birthdayPlaceHolder: "##/##/####",
    phone: "Phone",
    phonePlaceHolder: "#############"
  },
  reservationList: {
    title: "My Reservations",
    waiting: "pending",
    fromHour: "from",
    from: "from",
    boatName: "boat xxx",
    numberOfPerson: "Number of individuals"
  },
  reserveBoat: {
    title: "Book a boat",
    name: "username",
    namePlaceholder: "###########",
    identity: "Identification number / Location",
    identityPlaceholder: "###########",
    journey: "Journey",
    journeyA: "from xxx",
    journeyB: "to yyy",
    numberOfPerson: "Number of individuals",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "Bank account",
    bankAccountPlaceholder: "###########",
    totalPrice: "Total amount",
    totalPricePlaceholder: "###########",
    reservDate: "booking date",
    reservDatePlaceholder: "###########",
    reservTime: "Booking time",
    reservTimePlaceholder: "###########",
    birthDate: "Date of Birth",
    birthDatePlaceholder: "###########",
    phone: "Phone",
    phonePlaceholder: "###########",
    reserveNow: "Book now"
  },
  updateReserveBoat: {
    title: "Book a boat",
    name: "username",
    namePlaceholder: "###########",
    identity: "Identification number / Location",
    identityPlaceholder: "###########",
    numberOfPerson: "Number of individuals",
    numberOfPersonPlaceholder: "###########",
    bankAccount: "Bank account",
    bankAccountPlaceholder: "###########",
    totalPrice: "Total amount",
    totalPricePlaceholder: "###########",
    reservDate: "booking date",
    reservDatePlaceholder: "###########",
    reservTime: "Booking time",
    reservTimePlaceholder: "###########",
    birthDate: "Date of Birth",
    birthDatePlaceholder: "###########",
    phone: "Phone",
    phonePlaceholder: "###########",
    reserveNow: "Book now",
    accept: "Accept",
    reject: "Reject"
  },
  rateBoat: {
    description_1: "Please evaluate the cleanliness of the boat\n",
    description_2: "Accuracy of Appointments",
    description_3: "Credibility of boat owner",
    validate: "Send"
  },
  reservationError: {
    description: "Sorry. Please complete registration information",
    goToSettings: "Go to Settings"
  },
  validateCode: {
    title: "Confirmation code",
    description: "Please enter your confirmation code to verify your account",
    btnSend: "Send"
  },
  notifications: {
    title: "Notifications",
    description: "We have successfully booked. Wait for permission",
    from: "From",
    to: "To",
    pointA: "xxxx",
    pointB: "yyyy"
  },
  declaration: {
    title: "Declaration",
    agree: "OK",
    messageTitle: "Authorization of boat owner",
    message:
      "This text is an example of text that can be replaced in the same space, has been given this text is an example of text can be replaced in the same space, has been given this text is an example of text can be replaced in the same space"
  },
  profile: {
    title: "",
    username: "",
    usernamePlaceHolder: "",
    cin: "",
    cinplaceHolder: "",
    trip: "",
    from: "",
    to: "",
    bankAccount: "",
    bankAccountPlaceHolder: "",
    price: "",
    pricePlaceHolder: "",
    birthday: "",
    birthdayPlaceHolder: "",
    phone: "",
    phonePlaceHolder: "",
    email: "",
    emailPlaceHolder: ""
  },
  countries: {
    morocco: {
      label: "+212 Morocco",
      value: "+212",
      key: "+212"
    },
    Iraq: {
      label: "+964 Iraq",
      value: "+964",
      key: "+964"
    },
    uae: {
      label: "+971 United Arab Emirates",
      value: "+971",
      key: "+971"
    },
    Somalia: {
      label: "+252 Somalia",
      value: "+252",
      key: "+252"
    },
    SaudiArabia: {
      label: "+966 Saudi Arabia",
      value: "+966",
      key: "+966"
    },
    Sudan: {
      label: "+249 Sudan",
      value: "+249",
      key: "+249"
    },
    Algeria: {
      label: "+213 Algeria",
      value: "+213",
      key: "+213"
    },
    Bahrain: {
      label: "+973 Bahrain",
      value: "+973",
      key: "+973"
    },
    Jordan: {
      label: "+962 Jordan",
      value: "+962",
      key: "+962"
    },
    Yemen: {
      label: "+967 Yemen",
      value: "+967",
      key: "+967"
    },
    Kuwait: {
      label: "+965 Kuwait",
      value: "+965",
      key: "+965"
    },
    Mauritania: {
      label: "+222 Mauritania",
      value: "+222",
      key: "+222"
    },
    Egypt: {
      label: "+20 Egypt",
      value: "+20",
      key: "+20"
    },
    Qatar: {
      label: "+971 Qatar",
      value: "+971",
      key: "+971"
    },
    Libya: {
      label: "+218 Libya",
      value: "+218",
      key: "+218"
    },
    Palestine: {
      label: "+970 Palestine",
      value: "+970",
      key: "+970"
    },
    Lebanon: {
      label: "+961 Lebanon",
      value: "+961",
      key: "+961"
    },
    Oman: {
      label: "+968 Oman",
      value: "+968",
      key: "+968"
    },
    Syria: {
      label: "+963 Syria",
      value: "+963",
      key: "+963"
    },
    Djibouti: {
      label: "+253 Djibouti",
      value: "+253",
      key: "+253"
    },
    Comoros: {
      label: "+269 Comoros",
      value: "+269",
      key: "+269"
    },
    Tunisia: {
      label: "+216 Tunisia",
      value: "+216",
      key: "+216"
    }
  },
  success: {
    contactUs: {
      messageTitle: "Sending",
      message: "Your message sent successfuly"
    },
    reservBoat: {
      messageTitle: "Sending",
      message: "Your message sent successfuly"
    }
  },
  errors: {
    login: {
      requiredTitle: "Error",
      requiredMessage: "Phone number, code and password are required",
      cancel: "cancel"
    },
    contactUs: {
      requiredTitle: "Error",
      requiredMessage: "Phone number, name, email and message are required",
      cancel: "cancel"
    },
    reservBoat: {
      requiredTitle: "Error",
      requiredMessage: "All Fields are required",
      cancel: "cancel"
    },
    changePassword: {
      errorTitle: "Error",
      message: "Your password and confirmation password do not match",
      cancel: "cancel"
    },
    error500: "Oops! something went wrong and we couldn't process your request",
    errorTitle: "Error",
    cancel: "cancel",
    tokenExpired: "Sorry, your token expired",
    InvalidEntry: "Required field",
    network: "network not working"
  },
  emptyState: {
    empty: "empty data",
    refresh: "Refresh"
  },
  policy: {
    aboutUs: {
      title: "About the Qwereb boat application",
      body: "The application of the first application boats in Saudi Arabia, which integrates modern technologies with maritime transport services. The app provides integrated information for hundreds of modes of shipping, ferries, fishing boats, picnic boats, transport boats. No more need for time and arrangement to get the service you want: Find the boat or ferry where you want to select the destination and destination and let the rest apply to the boats. Whatever your plan, carry goods, Boats is the ideal and easiest application to organize your reservations."
    },
    humanIsAllforUs: {
      title: "The man is at the center of everything",
      body: "Through our work, we look forward to the values ​​we seek: partnership - honesty - perfection - transparency - human service - development - and put the customer at the heart of events for everything we do. We attract, develop and preserve the best ingredients and promote a global culture. Work in accordance with professional and ethical standards, develop and promote marine tourism that meets customer needs, create competitive and marketing advantages, and increase public awareness of tourism and marine tourism."
    },
    ourVision: {
      title: "Vision and Mission",
      body: "IzyDev will help you implement the idea of ​​application using the latest methods."
    },
    goals: {
      title: "Our goal",
      body: "Our goal is to become one of the first and foremost applications in the provision of maritime tourism services in Saudi Arabia and abroad by offering offers that meet the needs of our customers - and provide additional benefits to our customers throughout the Kingdom of Saudi Arabia, as well as to establish and maintain this relationship in a continuous and sustainable manner."
    },
    conditions: {
      title: "",
      body: ""
    }
  }
};
