import I18n from 'react-native-i18n';
import en from './en';
import fr from './fr';
import ar from './ar';

I18n.fallbacks = true;

I18n.translations = {
  en,
  ar,
  fr
};

export default I18n;
