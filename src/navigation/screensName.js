export default (screens = {
  stacks: {
    userStack: 'USER_STACK',
    adminStack: 'USER_STACK',
    loginStack: 'USER_STACK'
  },
  auth: {
    signin: 'LOGIN_SCREEN',
    signup: 'SIGNUP_SCREEN',
    forgotPassword: 'FORGOT_PASSWORD_SCREEN',
    resetPassword: 'RESET_PASSWORD_SCREEN',
    validateCode: 'VALIDATE_CODE_SCREEN'
  },
  boat: {
    home: 'HOME_SCREEN',
    boatDetails: 'BOAT_DETAILS_SCREEN',
    searchBoats: 'SEARCH_BOATS_SCREEN',
    addBoat: 'ADD_BOAT_SCREEN',
    myBoats: 'MY_BOATS_SCREEN'
  },
  reservation: {
    reservBoat: 'RESERV_BOAT_SCREEN',
    editResevBoat: 'EDIT_RESERVATION_SCREEN',
    reservationList: 'RESERVATION_LIST_SCREEN',
    PurchasesList: 'PURCHASES_LIST_SCREEN'
  },
  notifications: {
    boatNotifications: 'BOAT_NOTIFICATION_SCREEN'
  },
  settings: {
    declaration: 'DECLARATION_SCREEN',
    resetPassword: 'RESET_PASSWORD_SCREEN',
    changePassword: 'CHANGE_PASSWORD_SCREEN',
    validateCode: 'VALIDATE_CODE_SCREEN',
    contactUs: 'CONTACT_US_SCREEN',
    appInfo: 'APP_INFO_SCREEN',
    appInfo: 'APP_INFO_SCREEN',
    profileSettings: 'PROFILE_SETTINGS_SCREEN',
    lostConnection: 'LOST_CONNECTION'
  },
  modals: {
    rateBoat: 'RATE_BOAT_MODAL',
    reservationError: 'RESERVATION_ERROR_MODAL'
  },
  sideBar: {
    userSideBar: 'USER_SIDE_BAR'
  }
});
