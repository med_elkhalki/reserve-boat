import { Navigation } from 'react-native-navigation';
import Signin from '../views/auth/signin';
import Singup from '../views/auth/signup';
import Home from '../views/boat/home';
import BoatDetails from '../views/boat/boatDetails';
import ReservBoat from '../views/reservation/reservBoat';
import EditReservBoat from '../views/reservation/updateReservBoat';
import BoatNotifications from '../views/notifications/boatNotifications';
import ReservationList from '../views/reservation/reservationList';
import PurchasesList from '../views/reservation/purchasesList';
import Declaration from '../views/settings/declaration';
import RateBoat from '../views/modals/rateBoat';
import ReservationError from '../views/modals/reservationError';
import ResetPassword from '../views/auth/resetPassword';
import ChangePassword from '../views/auth/changePassword';
import ValidateCode from '../views/auth/validateCode';
import UserSideBar from '../views/common/userSideBar';
import AppInfo from '../views/settings/appinfo';
import ProfileSettings from '../views/settings/profileSettings';
import ContactUs from '../views/settings/contactUs';
import SearchBoats from '../views/boat/searchBoats';
import AddBoat from '../views/boat/addBoat';
import MyBoats from '../views/boat/myBoats';
import Screens from './screensName';
import LostConnection from '../views/settings/lostConnection';


export default function registerScreens(store: {}, Provider: {}) {
  Navigation.registerComponentWithRedux(
    Screens.auth.signin,
    () => Signin,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.auth.signup,
    () => Singup,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.home,
    () => Home,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.boatDetails,
    () => BoatDetails,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.reservation.reservBoat,
    () => ReservBoat,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.reservation.editResevBoat,
    () => EditReservBoat,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.notifications.boatNotifications,
    () => BoatNotifications,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.reservation.reservationList,
    () => ReservationList,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.boatDetails,
    () => BoatDetails,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.settings.declaration,
    () => Declaration,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.modals.rateBoat,
    () => RateBoat,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.modals.reservationError,
    () => ReservationError,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.auth.forgotPassword,
    () => ResetPassword,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.auth.validateCode,
    () => ValidateCode,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.auth.resetPassword,
    () => ChangePassword,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.reservation.PurchasesList,
    () => PurchasesList,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.settings.contactUs,
    () => ContactUs,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.sideBar.userSideBar,
    () => UserSideBar,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.settings.appInfo,
    () => AppInfo,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.settings.profileSettings,
    () => ProfileSettings,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.searchBoats,
    () => SearchBoats,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.addBoat,
    () => AddBoat,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(
    Screens.boat.myBoats,
    () => MyBoats,
    Provider,
    store
  );
  Navigation.registerComponentWithRedux(Screens.settings.lostConnection, () => LostConnection, Provider, store);

}
