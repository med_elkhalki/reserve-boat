import Screens from "./screensName";
import rootStore from "./../controllers/";
import Vars from "./../libs/vars";

export const LoginStack = () => {
  return {
    root: {
      stack: {
        children: [
          {
            component: {
              name: Screens.auth.signin,
              options: {
                topBar: {
                  visible: false
                }
              }
            }
          }
        ]
      }
    }
  };
};

export const userStack = (lang) => {
  console.log(`${Screens.stacks.userStack}_${rootStore.position}_${lang}`)
  return {
    root: {
      sideMenu: {
        [rootStore.position]: {
          component: {
            name: Screens.sideBar.userSideBar
          }
        },
        center: {
          stack: {
            id: `${Screens.stacks.userStack}_${rootStore.position}_${lang}`,
            children: [
              {
                component: {
                  name: Screens.boat.home,
                  options: {
                    topBar: {
                      visible: false
                    }
                  }
                }
              }
            ]
          }
        },
        options: {
          topBar: {
            visible: false
          },
          sideMenu: {
            [rootStore.position]: {
              visible: false,
              enabled: true,
              width: Vars.width,
              height: Vars.height
            }
          }
        }
      }
    }
  };
};

export const sideMenuStack = (screenName) => {
  return {
    component: {
      name: screenName,
      options: {
        topBar: {
          visible: false
        },
        sideMenu: {
          [rootStore.position]: {
            visible: false
          }
        }
      }
    }
  }
}
 