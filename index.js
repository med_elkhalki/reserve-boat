/** @format */
import App from './src';

if (__DEV__) {
    global.XMLHttpRequest = global.originalXMLHttpRequest ?
      global.originalXMLHttpRequest :
      global.XMLHttpRequest;
    global.FormData = global.originalFormData ?
      global.originalFormData :
      global.FormData;
  } else {
    let consoleBACK = global.console;
    global.console = {
      log: (...args) => {
      },
      error: (...args) => {
      },
      warn: (...args) => {
      }
    }
  }

new App();
